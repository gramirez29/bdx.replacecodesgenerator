﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Xsl;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Core.Entities;

namespace BDX.ReplaceCodesGenerator.Core.Core
{
    public static class HtmlGenerator
    {
        private static Stopwatch _generateHtmlStopwatch;

        public static void GenerateHtmlAsync(XDocument xml, string bhiContentPath, string function, string section, Partner partner, string fileName = "", string view = "", bool useSpecialFolderLocation = false)
        {
            string viewPath;
            string htmlPath;
            var file = string.IsNullOrEmpty(fileName) ? section : fileName;

            if (useSpecialFolderLocation)
            {
                viewPath = $@"{bhiContentPath}\GeneratedSeoContent\Views\{function}\{view}.xslt";
                htmlPath = $@"{bhiContentPath}\GeneratedSeoContent\HTML\{function}\{section}\";
            }
            else if (string.IsNullOrEmpty(function))
            { 
                viewPath = $@"{bhiContentPath}\GeneratedSeoContent\Views\{section}\{(string.IsNullOrEmpty(view) ? file : view)}.xslt";
                htmlPath = $@"{bhiContentPath}\GeneratedSeoContent\HTML\{section}\";
            }
            else if (!string.IsNullOrEmpty(section) && !string.IsNullOrEmpty(function) && !string.IsNullOrEmpty(fileName))
            {
                viewPath = $@"{bhiContentPath}\GeneratedSeoContent\Views\{function}\{section}\{file}.xslt";
                htmlPath = $@"{bhiContentPath}\GeneratedSeoContent\HTML\{function}\{section}\";
            }
            else
            {
                viewPath = $@"{bhiContentPath}\GeneratedSeoContent\Views\{function}\{section}\{function}.xslt";
                htmlPath = $@"{bhiContentPath}\GeneratedSeoContent\HTML\{function}\{section}\";
            }

            if (xml.Root != null)
            {
                var fileNameForKey = useSpecialFolderLocation
                    ? fileName?.Substring(0, fileName.Length - 2)
                    : file;

                GenerateHtml(xml, viewPath, htmlPath, xml.Root.Elements("FileName").FirstOrDefault(), partner,
                    $"View_{function}_{(!string.IsNullOrEmpty(fileNameForKey) ? fileNameForKey : section)}_{partner.PartnerId}");
                return;
            }

            Messages.XDocumentIsNull.LogInfo(new object[] { viewPath }, partner.PartnerId);
        }

        #region Private Methods

        private static void GenerateHtml(XNode xml, string viewPath, string outputPath, XElement fileNameNode, Partner partner, string cacheKey)
        {
            var fileName = fileNameNode != null ? fileNameNode.Value : string.Empty;
            var path = $@"{outputPath}\{fileName}.html";
            var htmlResult = string.Empty;

            if (viewPath.EndsWith(".xslt"))
            {
                var htmlGenerated = new XDocument();

                using (var writer = htmlGenerated.CreateWriter())
                {
                    var xslt = GetViewFromCache(cacheKey, viewPath);
                    xslt.Transform(xml.CreateReader(), writer);
                }

                htmlResult = htmlGenerated.ToString();
            }

            try
            {
                _generateHtmlStopwatch = new Stopwatch();
                _generateHtmlStopwatch.Start();

                File.WriteAllText(path, htmlResult);
                // Messages.SavedHtmlFile.LogInfo(new object[] { fileName, view},partner.PartnerId);

                _generateHtmlStopwatch.Stop();
                BdxWatcher.LogGenerateHtmlWatcher("WriteAllText in GenerateHtml Method finished ", partner.PartnerId, _generateHtmlStopwatch.LogTime());
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }

        private static XslCompiledTransform GetViewFromCache(string cacheKey, string viewPath)
        {
            return BdxStaticContentCache.GetOrAddExisting(cacheKey, () => GetViewFromServer(viewPath));
        }

        private static XslCompiledTransform GetViewFromServer(string route)
        {
            var xslt = new XslCompiledTransform();
            xslt.Load(route);
            
            return xslt;
        }
        #endregion
    }
}