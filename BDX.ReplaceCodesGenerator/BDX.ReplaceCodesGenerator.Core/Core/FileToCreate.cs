﻿using System.Xml.Linq;

namespace BDX.ReplaceCodesGenerator.Core.Core
{
    public class FileToCreate
    {
        public FileToCreate(XDocument xml, string function, string section, bool isXml = false, string filename = "", string view = "")
        {
            Xml = xml;
            Function = function;
            Section = section;
            FileName = filename;
            IsXml = isXml;
            View = view;
        }

        public XDocument Xml { get; set; }
        
        public string Function { get; set; }

        public string Section { get; set; }

        public string FileName { get; set; }

        public string View { get; set; }

        public bool IsXml { get; set; }
    }
}