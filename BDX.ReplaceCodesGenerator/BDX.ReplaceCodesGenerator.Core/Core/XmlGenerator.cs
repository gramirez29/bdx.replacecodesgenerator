﻿using System;
using System.Diagnostics;
using System.Xml.Linq;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Core.Entities;

namespace BDX.ReplaceCodesGenerator.Core.Core
{
    public static class XmlGenerator
    {
        private static Stopwatch _generateXmlStopwatch;

        public static void GenerateXmlAsync(XDocument xml, string bhiContentPath, string function, string section, string name, Partner partner)
        {
            _generateXmlStopwatch = new Stopwatch();
            _generateXmlStopwatch.Start();

            var htmlPath = string.IsNullOrEmpty(function) ? $@"{bhiContentPath}\GeneratedSeoContent\HTML\{section}\"
                : $@"{bhiContentPath}\GeneratedSeoContent\HTML\{function}\{section}\";
            

            if (xml.Root != null)
            {
                GenerateXml(xml, htmlPath, name);

                _generateXmlStopwatch.Stop();
                BdxWatcher.LogGenerateHtmlWatcher("WriteAllText in GenerateXmlAsync Method finished ", partner.PartnerId, _generateXmlStopwatch.LogTime());

                return;
            }

            _generateXmlStopwatch.Stop();
            Messages.XDocumentIsSavingXml.LogInfo(new object[] { section }, partner.PartnerId);
        }

        #region Private Methods

        private static void GenerateXml(XDocument xml, string outputPath, string name)
        {
            var path = $@"{outputPath}\{name}.xml";

            try
            {
                xml.Save(path);
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        #endregion
    }
}