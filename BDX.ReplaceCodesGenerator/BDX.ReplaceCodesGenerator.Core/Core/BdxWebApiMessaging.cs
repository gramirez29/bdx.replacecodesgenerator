﻿using System;
using System.Collections.Generic;
using System.Text;
using Bdx.Web.Api.Objects.Constants;
using BDX.StaticContentGenerator.Common.Extensions;
using BDX.StaticContentGenerator.Common.Util;
using Nhs.Utility.Web;

namespace BDX.StaticContentGenerator.Core.Core
{
    public class BdxWebApiMessaging
    {
        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

        public BdxWebApiMessaging()
        {
            _parameters.Add("client", "BDXStaticContentGenerator");
            _parameters.Add("sessiontoken", GenerateSessionToken());
            _parameters.Add("algorithm", BhiConfiguration.WebServicesApiHashingAlgorithm);
        }

        public BdxWebApiMessaging(int partnerId) : this()
        {
            if (!_parameters.ContainsKey(ApiUrlConstV2.PartnerId))
                _parameters.Add(ApiUrlConstV2.PartnerId, partnerId);
        }

        public BdxWebApiMessaging(Dictionary<string, object> parameters)
            : this()
        {
            foreach (var p in parameters)
                _parameters.Add(p.Key, p.Value);
        }

        public void AddParameter(string key, object value)
        {
            if (!_parameters.ContainsKey(key))
                _parameters.Add(key, value);
        }

        public T GetData<T>(string page) where T : new()
        {
            var newobj = new T();
            try
            {
                var url = Url(page);

                var jsonResult = HTTP.HttpGet(url);
                var model = jsonResult.ToFromJson<T>();

                if (model != null)
                    newobj = model;
            }
            catch (FormatException ex)
            {
                ex.LogError();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return newobj;
        }

        public string Url(string page)
        {
            var searchUrl = new StringBuilder();

            searchUrl.Append(BhiConfiguration.WebServicesApiUrl);

            searchUrl.AppendFormat("v2/search/{0}?", page);

            foreach (var parameter in _parameters)
                searchUrl.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);

            return searchUrl.ToString().TrimEnd('&');
        }

        private static string GenerateSessionToken()
        {
            return BhiConfiguration.DefaultSessionToken;
        }

        public string PartnerSitePassword { get; set; }
    }
}