﻿using System;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Core.Entities;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Core.Core;
using BDX.ReplaceCodesGenerator.Data.Entities;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Modules
{
    public static class ReplaceCodesContent
    {
        #region Stopwatch Section
        private static Stopwatch _replaceCodesWatcher;
        #endregion

        public static void GenerateReplaceCodes(Partner partner)
        {
            _replaceCodesWatcher = new Stopwatch();
            _replaceCodesWatcher.Start();

            if (!partner.LocationsExtendedInformation.Any())
            {
                partner.ReplaceCodesFileToCreate.CompleteAdding();
                Messages.NoLocationsFound.LogInfo(new object[] {partner.PartnerId.ToString() }, partner.PartnerId);

                _replaceCodesWatcher.Stop();
                BdxWatcher.LogWatcher("Replace Codes Finished ", partner.PartnerId, _replaceCodesWatcher.LogTime());

                return;
            }

            try
            {
                Messages.StartReplaceCodesGeneration.LogInfo(new object[] { partner.PartnerId.ToString() }, partner.PartnerId);

                // When "FilterByLocation" has value we're going to filter "LocationsExtendedInformation" for this filter criteria and thus we'll get generate content regarding define location filter 
                var locationsExtendedInformationFilteredByLocation = !string.IsNullOrEmpty(partner.FilterByLocation)
                    ? partner.LocationsExtendedInformation.FilterLocationsExtendedInformationByLocation(
                        partner.LocationFilterType, partner.LocationFilterValue)
                    : partner.LocationsExtendedInformation;

                var locationsLeft = 0;
                var extendedInformationFilteredByLocation = locationsExtendedInformationFilteredByLocation.ToList();
                var totalLocations = extendedInformationFilteredByLocation.Count;

                Parallel.ForEach(extendedInformationFilteredByLocation, location =>
                {
                    Interlocked.Increment(ref locationsLeft);

                    // No filtered Replace Codes Generation.
                    foreach (var srp in partner.DataProvider.SearchResultPagesForReplaceCodes())
                    {
                        if (location.Key.IsValidReplaceCodeLocation())
                        {
                            CreateReplaceCodes(location, partner, srp);
                        }
                    }

                    // Filtered Replace Codes Generation.
                    foreach (var srp in partner.DataProvider.SearchResultPagesForFilteredReplaceCodes())
                    {
                        if (location.Key.IsValidReplaceCodeLocation())
                        {
                            CreateFilteredReplaceCodes(location, partner, srp);
                        }
                    }

                    if (locationsLeft % 5000 == 0)
                        Messages.LocationsPendingForPartner.LogInfo(new object[] { totalLocations - locationsLeft, partner.PartnerId, "Replace Codes Generation" }, partner.PartnerId);
                });
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            partner.ReplaceCodesFileToCreate.CompleteAdding();
            Messages.FinishReplaceCodesGeneration.LogInfo(new object[] { partner.PartnerId.ToString() }, partner.PartnerId);

            _replaceCodesWatcher.Stop();
            BdxWatcher.LogWatcher("Replace Codes Finished ", partner.PartnerId, _replaceCodesWatcher.LogTime());
        }

        private static void CreateReplaceCodes(KeyValuePair<SearchLocation, Location> locationToGenerate, Partner partner, SearchResultPage srp)
        {
            var fileName = locationToGenerate.Key.ToStaticContentGeneratorReplaceCodeFileName();
            var avgPlanPrice = locationToGenerate.GetAveragePrices();
            var pricePerFoot = locationToGenerate.GetAveragePrices(true);
            var nonBasicBrands = locationToGenerate.GetBrandRanks();

            var resultXml = new ReplaceCode
            {
                AveragePlanPrice = avgPlanPrice?.ToType<int>().ToFormattedPrice(),
                PricePerFoot = pricePerFoot?.ToType<int>().ToFormattedPrice(),
                BrandRank = nonBasicBrands.ToRankedBrands()
            };

            var xml = resultXml.ToStringSerialize<ReplaceCode>();
            var document = XDocument.Parse(xml);

            Messages.SavingXmlFile.LogReplaceCodes(new object[] { fileName, "ReplaceCodes", partner.PartnerId }, partner.PartnerId);
            partner.ReplaceCodesFileToCreate.Add(new FileToCreate(document, srp.Name.GetFunctionNameFromSrpName(partner.PartnerId), "ReplaceCodes", true, fileName));
        }

        /// <summary>   Creates replace codes. </summary>
        /// <param name="locationToGenerate">   The location to generate. </param>
        /// <param name="partner">              The partner. </param>
        /// <param name="srp">                  The srp. </param>
        private static void CreateFilteredReplaceCodes(KeyValuePair<SearchLocation, Location> locationToGenerate, Partner partner, SearchResultPage srp)
        {
            var fileBaseName = locationToGenerate.Key.ToStaticContentGeneratorReplaceCodeFileName();
            var avgPlanPrice = locationToGenerate.GetAveragePrices();
            var pricePerFoot = locationToGenerate.GetAveragePrices(true);
            var nonBasicBrands = locationToGenerate.GetBrandRanks();
            var isCommSrp = srp.Name.IsCommunitySrp();

            Parallel.ForEach(srp.Amenities, amenity =>
            {
                var amenityFileName = $"{fileBaseName}_{amenity.Id}";

                var amenityBrandRank = nonBasicBrands.Where(amenity.AmenityBrandFilterFunction)
                    .OrderByDescending(x => amenity.AmenityBrandCount(x))
                    .Take(3).ToList().ToAmenityRankedBrands();

                var resultXmlAmenity = new ReplaceCode
                {
                    AveragePlanPrice = avgPlanPrice?.ToType<int>().ToFormattedPrice(),
                    PricePerFoot = pricePerFoot?.ToType<int>().ToFormattedPrice(),
                    BrandRank = amenityBrandRank
                };

                // Empty object to avoid File Not Found issues from NHS
                if (amenityBrandRank == null || string.IsNullOrEmpty(amenityBrandRank.BrandRank1))
                {
                    resultXmlAmenity = new ReplaceCode();
                }

                var amenityXml = resultXmlAmenity.ToStringSerialize<ReplaceCode>();
                var doc = XDocument.Parse(amenityXml);

                Messages.SavingXmlFile.LogReplaceCodes(new object[] { amenityFileName, "ReplaceCodes", partner.PartnerId }, partner.PartnerId);
                partner.ReplaceCodesFileToCreate.Add(new FileToCreate(doc, srp.Name.GetFunctionNameFromSrpName(partner.PartnerId), "ReplaceCodes", true, amenityFileName));
            });

            var resultXml = new ReplaceCode
            {
                AveragePlanPrice = avgPlanPrice?.ToType<int>().ToFormattedPrice(),
                PricePerFoot = pricePerFoot?.ToType<int>().ToFormattedPrice(),
                BrandRank = nonBasicBrands.ToRankedBrands(isCommSrp)
            };

            var xml = resultXml.ToStringSerialize<ReplaceCode>();
            var document = XDocument.Parse(xml);

            Messages.SavingXmlFile.LogReplaceCodes(new object[] { fileBaseName, "ReplaceCodes", partner.PartnerId }, partner.PartnerId);
            partner.ReplaceCodesFileToCreate.Add(new FileToCreate(document, srp.Name.GetFunctionNameFromSrpName(partner.PartnerId), "ReplaceCodes", true, fileBaseName));

        }

        /// <summary>
        /// A KeyValuePair SearchLocation,Location extension method that gets average prices.
        /// </summary>
        /// <param name="currentLocation">  The currentLocation to act on. </param>
        /// <param name="isPricePerFoot">   (Optional) True if is price per foot, false if not. </param>
        /// <returns>   The average prices. </returns>
        private static int? GetAveragePrices(this KeyValuePair<SearchLocation, Location> currentLocation, bool isPricePerFoot = false)
        {
            int? averagePrice = null;

            if (currentLocation.Value.LocationInformation != null)
            {
                averagePrice = isPricePerFoot
                    ? currentLocation.Value.LocationInformation.AveragePlanPriceFoot
                    : currentLocation.Value.LocationInformation.AveragePlanPrice;
            }

            return averagePrice;
        }

        /// <summary>
        /// A KeyValuePair SearchLocation,Location extension method that gets brand ranks.
        /// </summary>
        /// <param name="currentLocation">  The currentLocation to act on. </param>
        /// <returns>   The brand ranks. </returns>
        private static List<BrandLocation> GetBrandRanks(this KeyValuePair<SearchLocation, Location> currentLocation)
        {
            var brandRanks = new List<BrandLocation>();

            if (currentLocation.Value.Brands != null)
            {
                brandRanks = currentLocation.Value.Brands.Where(b => !b.Basic_Flag).ToList();
            }

            return brandRanks;
        }
    }
}
