﻿using System;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    /// <summary>   An email template. </summary>
    public class ProcessInfo
    {
        /// <summary>   Gets or sets the partner information. </summary>
        /// <value> The partner. </value>
        public Partner Partner { get; set; }

        /// <summary>   Gets or sets the start date time process. </summary>
        /// <value> The start date time. </value>
        public DateTime StartDateTime { get; set; }

        /// <summary>   Gets or sets the end date time process. </summary>
        /// <value> The end date time. </value>
        public DateTime EndDateTime { get; set; }

        /// <summary>   Gets or sets the arguments. </summary>
        /// <value> The arguments. </value>
        public string Arguments { get; set; }

        /// <summary>   Gets or sets the total number of removed files. </summary>
        /// <value> The total number of removed files. </value>
        public int TotalRemovedFiles { get; set; }

        /// <summary> Gets or sets a value indicating whether this object is removal file process. </summary>
        /// <value> True if this object is removal file process, false if not. </value>
        public bool IsRemovalFileProcess { get; set; }

        /// <summary>   Gets a value indicating whether the generation file process is executed. </summary>
        /// <value> True if generation file process, false if not. </value>
        public bool IsGenerationFileProcess => !IsRemovalFileProcess && TotalRemovedFiles.Equals(0);

    }
}