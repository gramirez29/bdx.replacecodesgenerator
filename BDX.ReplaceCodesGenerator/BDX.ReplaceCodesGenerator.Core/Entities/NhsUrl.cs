﻿using System.Collections.Generic;
using System.Linq;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Core.Entities.Abstract;
using BDX.ReplaceCodesGenerator.Data.Entities;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    public class NhsUrl : BaseUrl
    {
        public NhsUrl(DfuMarkets dfus, string partnerDomainUrl)
            : base(dfus, partnerDomainUrl)
        {
        }

        public override Link ToFooterLinkUrl(FooterLinkLocation location, string function, int partnerId, string text2 = "")
        {
            Link link = IsDfuLocation(location.MarketId)
                ? ToDfuUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName())
                : ToUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName());
            link.Text2 = string.IsNullOrEmpty(text2) ?  location.GetAveragePlanPrice() : text2;
            return link;
        }

        public override Link ToGoogleSnippetCityUrl(FooterLinkLocation location, string function, string count, int partnerId, string[] queryString = null)
        {
            if (location.AveragePlanPrice == 0)
                return new Link();

            var planPrice = location.AveragePlanPrice > 0 ? "$" + location.AveragePlanPrice.ToString("#,##0") : "-";
            Link link = IsDfuLocation(location.MarketId)
                ? ToDfuUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName(), queryString)
                : ToUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName(), queryString);

            link.Text2 = planPrice;
            link.CommunityCount = count;

            return link;
        }

        public override Link ToGoogleSnippetBrandUrl(BrandLocation location, string function, string count, int partnerId)
        {
            if (location.High_Price == 0 && location.Low_Price == 0)
                return null;

            Link link = IsDfuLocation(location.Market_Id)
                ? ToDfuUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name)
                : ToUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name);

            link.CommunityCount = count;
            link.Text2 = "$" + location.Avg_Home_Price.ToString("#,##0");

            return link;
        }

        public override Link ToBuilderDirectoryUrl(BrandLocation location, string function, int partnerId)
        {
            if (location.Community_Count == 0 && location.Home_Count == 0)
                return null;

            var link = IsDfuLocation(location.Market_Id)
                            ? ToDfuUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name)
                            : ToUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name);

            link.CommunityCount = location.Community_Count.ToString();
            link.HomeCount = location.Home_Count.ToString();

            return link;
        }

        public override Link ToSearchLocationUrl(SearchLocation location, string state, string function, string text, string queryString, int partnerId, bool isPopularSearches = false)
        {
            Dictionary<string, string> urlParameters = location.GetReferralSearchLocationParameters(state, partnerId);

            if (isPopularSearches && urlParameters.ContainsKey(UrlParameters.PostalCode))
            {
                urlParameters.Remove(UrlParameters.PostalCode);
            }

            var isMarketDfuLocation = IsDfuLocation(location.MarketId);

            var link = isMarketDfuLocation
                ? ToDfuUrl(urlParameters, function, text)
                : ToUrl(urlParameters, function, text);

            link.Url += queryString.ToLowerCase();
            return link;
        }

        /// <summary>   Converts this object to a dfu URL. </summary>
        /// <param name="urlParameters">        Options for controlling the URL. </param>
        /// <param name="function">             The function. </param>
        /// <param name="text">                 The text. </param>
        /// <param name="queryStringParams">    (Optional) Options for controlling the query string. </param>
        /// <returns>   The given data converted to a Link. </returns>
        public override Link ToDfuUrl(Dictionary<string, string> urlParameters, string function, string text, string[] queryStringParams = null)
        {
            var link = new Link
            {
                Text = text,
                Url = $@"/{function}/{urlParameters[UrlParameters.MarketStateNameOrAbbreviation].Trim().Replace(" ", "-")}/{urlParameters[UrlParameters.MarketName].Trim().Replace(" ", "-")}"
            };

            if (urlParameters.ContainsKey(UrlParameters.CityNameFilter))
                link.Url += "/" + urlParameters[UrlParameters.CityNameFilter].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.PostalCode))
                link.Url += "/" + urlParameters[UrlParameters.PostalCode].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.County))
                link.Url += "/" + urlParameters[UrlParameters.County].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.BrandId))
                link.Url += "/" + UrlParameters.BrandId + "-" + urlParameters[UrlParameters.BrandId].Trim().Replace(" ", "-");

            if (queryStringParams != null && queryStringParams.Any())
            {
                foreach (var parameter in queryStringParams)
                    link.Url += parameter + "&";

                link.Url = link.Url.Substring(0, link.Url.Length - 1);
            }

            link.Url = $"{GetPartnerDomainUrl()}{link.Url.ToLower()}";
            return link;
        }

        protected override void PriceRangeLink(ref Link linkToGenerate, decimal lowPrice, decimal highPrice)
        {
            if (highPrice != 0 && lowPrice != 0)
                linkToGenerate.Text2 = "$" + lowPrice.ToString("#,##0") + " - $" + highPrice.ToString("#,##0");

            if (highPrice == 0 && lowPrice != 0)
                linkToGenerate.Text2 = "From $" + lowPrice.ToString("#,##0");

            if (highPrice != 0 && lowPrice == 0)
                linkToGenerate.Text2 = "$" + highPrice.ToString("#,##0");
        }

        public override Link ToCityUrl(BrandLocation location, int partnerId)
        {
            var parameters = location.GetReferralBrandLocationParameters(partnerId);
            var referalBrandLocationParameters = new Dictionary<string, string>
            {
                {UrlParameters.Market, parameters[UrlParameters.Market]},
                {UrlParameters.CityNameFilter, parameters[UrlParameters.CityNameFilter]}
            };

            return ToUrl(referalBrandLocationParameters, SearchResultPageConsts.Communities, location.City);
        }

        public override Link ToBuilderUrl(Brand brand, string brandId, string marketId, string marketName, string state)
        {
            var parameters = brand.GetReferralBrandParameters(brandId, marketId, marketName, state);
            var referalBrandLocationParameters = new Dictionary<string, string>
            {
                {UrlParameters.Market, parameters[UrlParameters.Market]},
                {UrlParameters.BrandId, parameters[UrlParameters.BrandId]}
            };

            return ToUrl(referalBrandLocationParameters, brand.Function, string.Format(brand.Name, brand.BrandName));
        }

        public override Link ToZipCommUrl(FooterLinkLocation location, int partnerId)
        {
            return ToUrl(location.GetReferralFooterLinkLocationParameters(partnerId), SearchResultPageConsts.Communities, location.PostalCode.Trim());
        }

        public override Link ToAmenityResultUrl(Amenity amenity, Location location)
        {
            var referralAmenityParameters = new Dictionary<string, string>
            {
                {UrlParameters.Market, location.LocationInformation.MarketId.ToString()}
            };

            var queryStringParams = new[] { amenity.Parameter };

            return ToUrl(referralAmenityParameters, SearchResultPageConsts.Communities, amenity.Name, queryStringParams);
        }

        public override Link ToSchoolDistrictUrl(FooterLinkLocation location)
        {
            var referalAmenityParameters = new Dictionary<string, string>
            {
                {UrlParameters.Market, location.MarketId.ToString()}
            };

            var queryStringParams = new[] { string.Format(AmenitiesConsts.SchoolDistrictParameter, location.DistrictId) };
            return ToUrl(referalAmenityParameters, SearchResultPageConsts.Communities, string.Format("{0} {1}", location.DistrictName, AmenitiesConsts.SchoolDistrict), queryStringParams);
        }

        public override Link ToCommDetailUrl(SearchLocation location, int partnerId)
        {
            location.GetReferralSearchLocationParameters(location.State, partnerId);

            var link = new Link
            {
                Url = ToCommDetailUrl(location.Builder_Id.ToString(), location.Id.ToString()),
                Text = $"{location.Location} by {location.Builder_Name}"
            };

            return link;
        }

        public override Link ToCommDetailUrl(NewestCommunities location)
        {
            var link = new Link
            {
                Url = ToCommDetailDfuUrl(location),
                Text = location.CommunityName,
                HomeCount = location.HomeCount.ToString(),
                Text2 = location.LowPrice > 0 ? $"$ {location.LowPrice:#,##0}" : "TBD"
            };

            return link;
        }

        private string ToCommDetailUrl(string builderId, string communityId)
        {
            return $@"/communitydetail/builder-{builderId}/community-{communityId}".ToLowerCase();
        }

        private string ToCommDetailDfuUrl(NewestCommunities location)
        {
            var url = string.Empty;

            if (string.Equals(location.ProjectTypeCode, "MPC"))
                url = $@"/masterplan/{location.CommunityId}";

            else if (!string.IsNullOrEmpty(location.State) && !string.IsNullOrEmpty(location.City) && !string.IsNullOrEmpty(location.CommunityName) && !string.IsNullOrEmpty(location.BrandName))
            {
                url = $@"{GetPartnerDomainUrl()}/community/{location.State}/{location.City.ReplaceSpecialCharacters("-")}/{location.CommunityName.ReplaceSpecialCharacters("-")}-by-{location.BrandName.ReplaceSpecialCharacters("-")}/{location.CommunityId}".Replace(" ", "-");
                url = url.ToLowerCase().Replace("---", "-");
            }

            return url;

        }

        public override Link ToMarketSummaryUrl(MarketSummary market, int partnerId, string[] queryStringParams = null, string function = "", int count = 0)
        {
            function = string.IsNullOrEmpty(function) ? SearchResultPageConsts.Communities : function;
            Link link = IsDfuLocation(market.Market_Id) ? ToDfuUrl(market.GetReferralMarketSummaryParameters(partnerId), function, market.ReferralName(), queryStringParams)
                                                        : ToUrl(market.GetReferralMarketSummaryParameters(partnerId), function, market.ReferralName(), queryStringParams);

            link.CommunityCount = count > 0 ? count.ToString() : market.Community_Count.ToString();
            return link;
        }
    }
}
