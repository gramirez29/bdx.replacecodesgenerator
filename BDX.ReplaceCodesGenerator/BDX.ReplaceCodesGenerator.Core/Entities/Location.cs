﻿using System.Collections.Generic;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    public class Location
    {
        public Location()
        {
            FilteredMarketLinks = new FilteredMarket();
            LocationInformation = new FooterLinkLocation();
            NearByCities = new List<FooterLinkLocation>();
            NearByCounties = new List<FooterLinkLocation>();
            NearByPostalCodes = new List<FooterLinkLocation>();
            NearByNewestCommunies = new List<NewestCommunities>();
            MarketInformation = null;
        }

        public FilteredMarket FilteredMarketLinks { get; set; }

        public bool IsDfu { get; set; }

        public FooterLinkLocation LocationInformation { get; set; }

        public MarketSummary MarketInformation { get; set; }

        public List<FooterLinkLocation> NearByCities { get; set; }

        public List<FooterLinkLocation> NearByCounties { get; set; }

        public List<FooterLinkLocation> NearByPostalCodes { get; set; }

        public List<FooterLinkLocation> NearBySchoolDistricts { get; set; }

        public List<NewestCommunities> NearByNewestCommunies { get; set; }

        public List<BrandLocation> Brands { get; set; }

        public IEnumerable<BrandLocation> BuildersWithoutReviews { get; set; }

        public List<FooterLinkLocation> SyntheticsByMarket { get; set; }

        public IEnumerable<BrandInformation> BrandsInformation { get; set; }
    }
}