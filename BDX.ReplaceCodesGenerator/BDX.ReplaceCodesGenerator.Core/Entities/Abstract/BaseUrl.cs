﻿using System.Collections.Generic;
using System.Linq;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Data.Entities;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities.Abstract
{
    public abstract class BaseUrl : IUrlGenerator
    {
        protected DfuMarkets Dfus { get; set; }

        protected string PartnerDomainUrl { get; set; }

        protected BaseUrl(DfuMarkets dfus, string partnerDomainUrl)
        {
            Dfus = dfus;
            PartnerDomainUrl = partnerDomainUrl;
        }

        public abstract Link ToFooterLinkUrl(FooterLinkLocation location, string function, int partnerId, string text2 = "");

        public abstract Link ToGoogleSnippetCityUrl(FooterLinkLocation location, string function, string count, int partnerId, string[] queryString = null);

        public abstract Link ToGoogleSnippetBrandUrl(BrandLocation location, string function, string count, int partnerId);

        public abstract Link ToBuilderDirectoryUrl(BrandLocation location, string function, int partnerId);

        public abstract Link ToSearchLocationUrl(SearchLocation location, string state, string function, string text, string queryString, int partnerId, bool isPopularSearches = false);

        public abstract Link ToCityUrl(BrandLocation location, int partnerId);

        public abstract Link ToBuilderUrl(Brand brand, string brandId, string marketId, string marketName, string state);

        public abstract Link ToZipCommUrl(FooterLinkLocation location, int partnerId);

        public abstract Link ToAmenityResultUrl(Amenity amenity, Location location);

        public abstract Link ToSchoolDistrictUrl(FooterLinkLocation location);

        public abstract Link ToCommDetailUrl(SearchLocation location, int partnerId);

        public abstract Link ToCommDetailUrl(NewestCommunities location);

        public abstract Link ToMarketSummaryUrl(MarketSummary market, int partnerId, string[] queryStringParams = null, string function = "", int count = 0);

        public virtual Link ToSyntheticUrl(FooterLinkLocation location, string function, string text)
        {
            if (location.AveragePlanPrice == 0 || location.CommunityCount == 0 || location.HomeCount == 0)
                return new Link();

            var planPrice = location.AveragePlanPrice > 0 ? "$" + location.AveragePlanPrice.ToString("#,##0") : "-";

            var link = new Link
            {
                Text = text,
                CommunityCount = location.CommunityCount.ToType<string>(),
                Url = $@"{GetPartnerDomainUrl()}/{function}/{location.Location.Replace(' ', '-')}".ToLowerCase(),
                HomeCount = location.HomeCount.ToType<string>(),
                Text2 = planPrice,
            };

            return link;
        }

        public abstract Link ToDfuUrl(Dictionary<string, string> urlParameters, string function, string text, string[] queryStringParams = null);

        protected Link ToUrl(Dictionary<string, string> urlParameters, string function, string text, string[] queryStringParams = null, bool isSearchResultPageUrl = true, string elementClass = "", string image= "")
        {
            var link = new Link();

            if (isSearchResultPageUrl)
                link = ToSrpUrl(urlParameters, function, text, queryStringParams);
            else
            {
                link.Url = string.IsNullOrEmpty(function) ? string.Empty : $@"/{function}";
                link.Text = text;
                link.ElementClass = elementClass;

                if (!string.IsNullOrEmpty(image))
                {
                    link.Image = image;
                }

                foreach (var parameter in urlParameters.Values)
                {
                    link.Url += $"/{parameter.Trim()}";
                }

                if (queryStringParams != null && queryStringParams.Any())
                {
                    foreach (var parameter in queryStringParams)
                        link.Url += parameter + "&";

                    link.Url += link.Url.Substring(0, link.Url.Length - 1);
                }
            }

            link.Url = $"{GetPartnerDomainUrl()}{link.Url.EndsWithSlashToRemove()}";

            return link;
        }

        /// <summary>   Converts a newLink object to an URL. </summary>
        /// <param name="newLink">  The new link. </param>
        /// <returns>   NewLink as a Link. </returns>
        protected Link ToUrl(LinkInfo newLink)
        {
            var link = new Link();

            if (newLink.IsSearchResultPageUrl)
            {
                link = ToSrpUrl(newLink.UrlParameters, newLink.Function, newLink.LinkText, newLink.QueryStringParams);

                var url = link.Url.EndsWithSlashToRemove();
                link.Url = url;

                return link;
            }

            link.Url = string.IsNullOrEmpty(newLink.Function) ? string.Empty : $@"/{newLink.Function}";
            link.Text = newLink.LinkText;
            link.ElementClass = newLink.ElementClass;

            link.AltText = !string.IsNullOrEmpty(newLink.ImageAltText) ? newLink.ImageAltText : newLink.LinkText;

            if (!string.IsNullOrEmpty(newLink.ImageLink))
            {
                link.Image = newLink.ImageLink;
            }

            foreach (var parameter in newLink.UrlParameters.Values)
            {
                link.Url += $"/{parameter.Trim()}";
            }

            if (newLink.QueryStringParams != null && newLink.QueryStringParams.Any())
            {
                foreach (var parameter in newLink.QueryStringParams)
                {
                    link.Url += parameter + "&";
                }

                link.Url += link.Url.Substring(0, link.Url.Length - 1);
            }

            link.Url = link.Url.EndsWithSlashToRemove();
            return link;
        }

        private static Link ToSrpUrl(IReadOnlyDictionary<string, string> urlParameters, string function, string text, string[] queryStringParams = null)
        {
            var link = new Link
            {
                Text = text,
                Url = $@"/{function}/market-{urlParameters[UrlParameters.Market].Trim().Replace(" ", "-")}"
            };

            if (urlParameters.ContainsKey(UrlParameters.CityNameFilter))
                link.Url += "/" + UrlParameters.CityNameFilter + "-" + urlParameters[UrlParameters.CityNameFilter].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.PostalCode))
                link.Url += "/" + urlParameters[UrlParameters.PostalCode].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.County))
                link.Url += "/" + urlParameters[UrlParameters.County].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.BrandId))
                link.Url += "/" + UrlParameters.BrandId + "-" + urlParameters[UrlParameters.BrandId].Trim().Replace(" ", "-");

            if (queryStringParams == null)
            {
                link.Url = link.Url.ToLowerCase();
                return link;
            }

            foreach (var param in queryStringParams)
                link.Url += param;

            link.Url = link.Url.ToLowerCase();

            return link;
        }

        protected bool IsDfuLocation(int marketId)
        {
            return Dfus.Market.Any(x => x.Id == marketId);
        }

        /// <summary>   Gets partner domain URL. </summary>
        /// <returns>   The partner domain URL. </returns>
        protected string GetPartnerDomainUrl()
        {
            return PartnerDomainUrl;
        }

        protected abstract void PriceRangeLink(ref Link linkToGenerate, decimal lowPrice, decimal highPrice);
    }
}