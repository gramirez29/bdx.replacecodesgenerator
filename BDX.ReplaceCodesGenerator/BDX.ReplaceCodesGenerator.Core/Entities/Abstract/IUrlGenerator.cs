﻿using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities.Abstract
{
    internal interface IUrlGenerator
    {
        Link ToFooterLinkUrl(FooterLinkLocation location, string function, int partnerId, string text2 = "");
    }
}
