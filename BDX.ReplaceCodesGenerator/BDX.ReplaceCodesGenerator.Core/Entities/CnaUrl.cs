﻿using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Core.Entities.Abstract;
using BDX.ReplaceCodesGenerator.Data.Entities;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    public class CnaUrl : BaseUrl
    {
        public CnaUrl(DfuMarkets dfus, string partnerDomainUrl)
            : base(dfus, partnerDomainUrl)
        {
        }

        public override Link ToFooterLinkUrl(FooterLinkLocation location, string function, int partnerId, string text2 = "")
        {
            Link link = ToDfuUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName());
            link.Text2 = string.IsNullOrEmpty(text2) ? location.GetAveragePlanPrice() : text2;
            return link;
        }

        public override Link ToGoogleSnippetCityUrl(FooterLinkLocation location, string function, string count, int partnerId, string[] queryString = null)
        {
            if (location.AveragePlanPrice == 0)
                return new Link();

            var planPrice = location.AveragePlanPrice > 0 ? "$" + location.AveragePlanPrice.ToString("#,##0") : "-";

            Link link = ToDfuUrl(location.GetReferralFooterLinkLocationParameters(partnerId), function, location.ReferralName(), queryString);

            link.Text2 = planPrice;
            link.CommunityCount = count;

            return link;
        }

        public override Link ToGoogleSnippetBrandUrl(BrandLocation location, string function, string count, int partnerId)
        {
            if (location.High_Price == 0 && location.Low_Price == 0)
                return new Link();

            Link link = ToDfuUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name);

            link.CommunityCount = count;
            link.Text2 = "$" + location.Avg_Home_Price.ToString("#,##0");

            return link;
        }

        public override Link ToBuilderDirectoryUrl(BrandLocation location, string function, int partnerId)
        {
            if (location.Community_Count == 0 && location.Home_Count == 0)
                return new Link();

            var link = ToDfuUrl(location.GetReferralBrandLocationParameters(partnerId), function, location.Brand_Name);

            link.CommunityCount = location.Community_Count.ToString();
            link.HomeCount = location.Home_Count.ToString();

            return link;
        }

        public override Link ToSearchLocationUrl(SearchLocation location, string state, string function, string modularTextOrAmenityName, string queryString, int partnerId, bool isPopularSearches = false)
        {
            Dictionary<string, string> urlParamenters = location.GetReferralSearchLocationParameters(state, partnerId);

            if (isPopularSearches && urlParamenters.ContainsKey(UrlParameters.PostalCode))
            {
                urlParamenters.Remove(UrlParameters.PostalCode);
            }

            Link link = ToDfuUrl(urlParamenters, function, modularTextOrAmenityName);
            link.Url += queryString;

            return link;
        }

        public override Link ToDfuUrl(Dictionary<string, string> urlParameters, string function, string text, string[] queryStringParams = null)
        {
            var link = new Link
            {
                Text = text,
                Url = $@"/{function}/{urlParameters[UrlParameters.MarketStateNameOrAbbreviation].Trim().Replace(" ", "-")}/{urlParameters[UrlParameters.MarketName].Trim()
                    .Replace("-" + UrlParameters.Area, "").Replace(" ", "-")}"
            };

            if (urlParameters.ContainsKey(UrlParameters.CityNameFilter))
                link.Url += "/" + UrlParameters.Ciudad + "-" + urlParameters[UrlParameters.CityNameFilter].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.PostalCode))
                link.Url += "/" + urlParameters[UrlParameters.PostalCode].Trim().Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.County))
                link.Url += "/" + UrlParameters.Condado + "-" + urlParameters[UrlParameters.County].Trim().Replace(" County", "").Replace(" ", "-");

            if (urlParameters.ContainsKey(UrlParameters.BrandId))
                link.Url += "/" + UrlParameters.BrandId + "-" + urlParameters[UrlParameters.BrandId].Trim();

            if (queryStringParams != null)
            {
                foreach (var param in queryStringParams)
                    link.Url += param;
            }

            link.Url = $"{GetPartnerDomainUrl()}{link.Url.ToLower()}";
            return link;
        }

        protected override void PriceRangeLink(ref Link linkToGenerate, decimal lowPrice, decimal highPrice)
        {
            if (highPrice != 0 && lowPrice != 0)
                linkToGenerate.Text2 = "$" + lowPrice.ToString("#,##0") + " - $" + highPrice.ToString("#,##0");

            if (highPrice == 0 && lowPrice != 0)
                linkToGenerate.Text2 = "Desde $" + lowPrice.ToString("#,##0");

            if (highPrice != 0 && lowPrice == 0)
                linkToGenerate.Text2 = "$" + highPrice.ToString("#,##0");
        }

        public override Link ToCityUrl(BrandLocation location, int partnerId)
        {
            var parameters = location.GetReferralBrandLocationParameters(partnerId);
            var referalBrandLocationParameters = new Dictionary<string, string>
            {
                {UrlParameters.MarketState, parameters[UrlParameters.MarketState]},
                {UrlParameters.MarketName, parameters[UrlParameters.MarketName]},
                {UrlParameters.CityNameFilter, parameters[UrlParameters.CityNameFilter]}
            };

            return ToDfuUrl(referalBrandLocationParameters, SearchResultPageConsts.Comunidades, location.City);
        }

        public override Link ToBuilderUrl(Brand brand, string brandId, string marketId, string marketName, string state)
        {
            var parameters = brand.GetReferralBrandParameters(brandId, marketId, marketName, state);
            var referalBrandLocationParameters = new Dictionary<string, string>
            {
                {UrlParameters.MarketState, parameters[UrlParameters.State]},
                {UrlParameters.MarketName, parameters[UrlParameters.MarketName]},
                {UrlParameters.BrandId, parameters[UrlParameters.BrandId]}
            };

            return ToDfuUrl(referalBrandLocationParameters, SearchResultPageConsts.Comunidades, string.Format(brand.Name, brand.BrandName));
        }

        public override Link ToZipCommUrl(FooterLinkLocation location, int partnerId)
        {
            return ToDfuUrl(location.GetReferralFooterLinkLocationParameters(partnerId), SearchResultPageConsts.Comunidades, location.PostalCode.Trim());
        }

        public override Link ToAmenityResultUrl(Amenity amenity, Location location)
        {
            var referalAmenityParameters = new Dictionary<string, string>
            {
                {UrlParameters.MarketState, location.LocationInformation.StateName},
                {UrlParameters.MarketName, location.LocationInformation.MarketName}
            };

            var queryStringParams = new[] { amenity.Parameter };
            return ToDfuUrl(referalAmenityParameters, SearchResultPageConsts.Comunidades, amenity.Name, queryStringParams);
        }

        public override Link ToSchoolDistrictUrl(FooterLinkLocation location)
        {
            var referalAmenityParameters = new Dictionary<string, string>
            {
                {UrlParameters.MarketState, location.StateName},
                {UrlParameters.MarketName, location.MarketName}
            };

            var queryStringParams = new[] { string.Format(AmenitiesConsts.SchoolDistrictParameter, location.DistrictId) };
            return ToDfuUrl(referalAmenityParameters, SearchResultPageConsts.Comunidades, $"{location.DistrictName} {AmenitiesConsts.DistritoEscolar}", queryStringParams);
        }

        public override Link ToCommDetailUrl(SearchLocation location, int partnerId)
        {
            location.GetReferralSearchLocationParameters(location.State, partnerId);

            var link = new Link
            {
                Url = ToCommDetailUrl(location.State, location.Market, location.Location, location.Id.ToString()),
                Text = $"{location.Location} por {location.Builder_Name}"
            };

            return link;
        }

        public override Link ToCommDetailUrl(NewestCommunities location)
        {
            var link = new Link
            {
                Url = ToCommDetailDfuUrl(location),
                Text = location.CommunityName,
                Text2 = location.LowPrice > 0 ? $"$ {location.LowPrice:#,##0}" : "TBD",
                HomeCount = location.HomeCount.ToString()
            };

            return link;
        }

        private string ToCommDetailUrl(string state, string market, string location, string id)
        {
            return $@"/comunidad/{state}/{market.Replace(" ", "-")}/{location.Replace(" ", "-")}/{id}".ToLowerCase();
        }

        private string ToCommDetailDfuUrl(NewestCommunities location)
        {
            var url = string.Empty;

            if (string.Equals(location.ProjectTypeCode, "MPC"))
                url = $@"/masterplan/{location.CommunityId}";

            else if (!string.IsNullOrEmpty(location.MarketState))
            {
                url = $@"{GetPartnerDomainUrl()}/comunidad/{location.MarketState}/{location.MarketName}/{location.CommunityName.ReplaceSpecialCharacters("-")}/{location.CommunityId}"
                        .Replace(" ", "-");
                url = url.Replace("---", "-").ToLowerCase();
            }

            return url;
        }

        public override Link ToMarketSummaryUrl(MarketSummary market, int partnerId, string[] queryStringParams = null, string function = "", int count = 0)
        {
            function = string.IsNullOrEmpty(function) ? SearchResultPageConsts.Comunidades : function;
            Link link = IsDfuLocation(market.Market_Id) ? ToDfuUrl(market.GetReferralMarketSummaryParameters(partnerId), function, market.ReferralName(), queryStringParams)
                                                        : ToUrl(market.GetReferralMarketSummaryParameters(partnerId), function, market.ReferralName(), queryStringParams);

            link.CommunityCount = market.Community_Count.ToString();
            return link;
        }
    }
}