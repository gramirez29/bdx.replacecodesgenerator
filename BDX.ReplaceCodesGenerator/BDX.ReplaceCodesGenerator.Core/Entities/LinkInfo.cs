﻿using System.Collections.Generic;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    /// <summary>   A link to generate. </summary>
    public class LinkInfo
    {
        /// <summary>   Gets or sets options for controlling the URL. </summary>
        public Dictionary<string, string> UrlParameters { get; set; }

        /// <summary>   Gets or sets the function. </summary>
        public string Function { get; set; }

        /// <summary>   Gets or sets the link text. </summary>
        public string LinkText { get; set; }

        /// <summary>   Gets or sets options for controlling the query string. </summary>
        public string[] QueryStringParams { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is search result page URL.
        /// </summary>
        public bool IsSearchResultPageUrl { get; set; }

        /// <summary>   Gets or sets the element class. </summary>
        public string ElementClass { get; set; }

        /// <summary>   Gets or sets the image link. </summary>
        public string ImageLink { get; set; }

        /// <summary>   Gets or sets the image alternate text. </summary>
        public string ImageAltText { get; set; }

    }
}
