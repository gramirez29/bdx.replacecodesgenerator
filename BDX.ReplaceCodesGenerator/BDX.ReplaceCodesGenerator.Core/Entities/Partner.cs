﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bdx.Ratings.Client.Api;
using Bdx.Ratings.Models;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Core.Core;
using BDX.ReplaceCodesGenerator.Core.Entities.Abstract;
using BDX.ReplaceCodesGenerator.Data.Abstract;
using BDX.ReplaceCodesGenerator.Data.Concrete;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Core.Entities
{
    public class Partner
    {
        private Stopwatch _informationFromXmlWatcher;
        private Stopwatch _setupLocationWatcher;
        private Stopwatch _overrideSyntheticInfoWatcher;

        private string _filterByLocation;

        public Partner(string id, string filterByLocation)
        {
            PartnerId = id.ToType<int>();

            var partnerStopWatch = new Stopwatch();
            partnerStopWatch.Start();

            FilterByLocation = !string.IsNullOrEmpty(filterByLocation) ? filterByLocation : string.Empty;

            ReplaceCodesFileToCreate = new BlockingCollection<FileToCreate>();

            LocationsExtendedInformation = new ConcurrentDictionary<SearchLocation, Location>();
            SpecialFiltersByMarket = new ConcurrentDictionary<int, FilteredMarket>();

            partnerStopWatch.Stop();
            BdxWatcher.LogWatcher("Partner constructor created. ", PartnerId, partnerStopWatch.LogTime());
        }

        public WebFactory DataProvider { get; private set; }

        public Task[] FileCreationConsumers { get; private set; }

        public DfuMarkets FriendlyNamingMarkets { get; private set; }

        public List<SearchLocation> Locations { get; private set; }

        public List<SearchLocation> LocationsWithoutCommunities { get; private set; }

        public ConcurrentDictionary<SearchLocation, Location> LocationsExtendedInformation { get; private set; }

        public int PartnerId { get; private set; }

        public string PartnerName { get; set; }

        /// <summary>   Gets or sets URL of the partner domain. </summary>
        /// <value> The partner domain URL. </value>
        public string PartnerDomainUrl { get; set; }

        public BaseUrl UrlGenerator { get; private set; }

        public BlockingCollection<FileToCreate> ReplaceCodesFileToCreate { get; set; }

        protected string BhiContentFolder { get; private set; }

        protected int CityRadius { get; private set; }

        protected int PostalCodeRadius { get; private set; }

        protected SyntheticGeoNames SyntheticLocations { get; private set; }

        protected MultiValueDictionary<SearchLocation, BrandLocation> Brands { get; set; }

        protected MultiValueDictionary<SearchLocation, FooterLinkLocation> FooterLinks { get; set; }

        protected MultiValueDictionary<SearchLocation, FooterLinkLocation> SchoolDistricts { get; set; }

        protected HashSet<FooterLinkLocation> FooterLinkLocations { get; set; }

        protected IEnumerable<BrandInformation> BrandsInformation { get; set; }

        protected MultiValueDictionary<SearchLocation, NewestCommunities> NewestCommunities { get; set; }

        protected List<BrandBuilderPerMarket> BrandBuildersPerMarketReviews { get; set; }

        protected Dictionary<int, MarketSummary> MarketsSummary { get; set; }

        private string DfuMarketsFilePath { get; set; }

        private ConcurrentDictionary<int, FilteredMarket> SpecialFiltersByMarket { get; set; }

        private string SpecialFilteredFile { get; set; }

        private SpecialFilteredSrp SpecialFilterLinks { get; set; }

        private string SyntheticFilePath { get; set; }

        public string FilterByLocation
        {
            get => _filterByLocation;
            set
            {
                _filterByLocation = value;
                if (string.IsNullOrEmpty(_filterByLocation)) return;
                LocationFilterType = _filterByLocation.Split('-')[0].Trim();
                LocationFilterValue = _filterByLocation.Split('-')[1].Trim();
            }
        }

        public string LocationFilterValue { get; private set; }

        public string LocationFilterType { get; private set; }

        public int ReplaceCodesCounts { get; set; }

        public void InitializePartner(IRatingsApiClient apiClient, string id)
        {
            CreateDbConnections(apiClient, id);
            InitializeData(id);
            ConfigurePartner(id);
        }

        private void CreateDbConnections(IRatingsApiClient apiClient, string id)
        {
            var connectionStopwatch = new Stopwatch();
            var bhiConfigStopwatch = new Stopwatch();
            connectionStopwatch.Start();

            DataProvider = PartnerId != PartnersConsts.CNA ? new WebDatabase(apiClient) : new EsWebDatabase(apiClient) as WebFactory;

            connectionStopwatch.Stop();
            BdxWatcher.LogWatcher("All db connections have been processed. ", PartnerId, connectionStopwatch.LogTime());

            bhiConfigStopwatch.Start();

            ReadConfigKeys(id);

            bhiConfigStopwatch.Stop();
            BdxWatcher.LogWatcher("BHIConfigs have been read. ", PartnerId, connectionStopwatch.LogTime());
        }

        private void InitializeData(string id)
        {
            var tasksToProcess = new Stopwatch();
            tasksToProcess.Start();

            var getFooterLinks = Task.Factory.StartNew(() => FooterLinks = DataProvider.GetFooterLinkLocationsAsync(PartnerId, CityRadius, PostalCodeRadius).Result);
            // We set a radius 0 to retrieve by Exact Location. Ticket https://builderhomesite.atlassian.net/browse/BDXNHS-6704
            var getBrands = Task.Factory.StartNew(() => Brands = DataProvider.GetBrandsInformationAsync(PartnerId, 0).Result);
            var getLocations = Task.Factory.StartNew(() => Locations = DataProvider.GetLocationsAsync(PartnerId).Result.ToList());
            var getMarketSummary = Task.Factory.StartNew(() => MarketsSummary = DataProvider.GetMarketSummaryAsync(PartnerId, 0).Result);
            var getNewestCommunities = Task.Factory.StartNew(() => NewestCommunities = DataProvider.GetNewestCommunitiesInformationAsync(PartnerId, CityRadius, PostalCodeRadius).Result);
            var getReviews = Task.Factory.StartNew(() => BrandBuildersPerMarketReviews = DataProvider.GetBrandBuilderPerMarketAsync(PartnerId).Result);

            Task.WaitAll(getFooterLinks, getLocations, getBrands, getMarketSummary, getNewestCommunities, getReviews);

            Messages.ConfigurationPartnerDatabaseReady.LogInfo(new object[] { id }, PartnerId);

            tasksToProcess.Stop();
            BdxWatcher.LogWatcher("All DB tasks has been processed. ", PartnerId, tasksToProcess.LogTime());
        }

        private void ConfigurePartner(string id)
        {
            var partnerConfiguration = new Stopwatch();
            partnerConfiguration.Start();

            Messages.ConfigurationPartnerStarted.LogInfo(new object[] { id }, PartnerId);

            ReadInformationFromXml();
            UrlGenerator = PartnerId != PartnersConsts.CNA ? new NhsUrl(FriendlyNamingMarkets, PartnerDomainUrl) : new CnaUrl(FriendlyNamingMarkets, PartnerDomainUrl) as BaseUrl;

            FooterLinkLocations = DataProvider.FooterLinkLocations;
            SchoolDistricts = DataProvider.GetSchoolDistricts();
            BrandsInformation = DataProvider.GetBrandByMarket(PartnerId);

            if (PartnerId != PartnersConsts.CNA)
            {
                Locations.AddRange(SyntheticNamesToLocation(SyntheticLocations));
            }

            LocationsWithoutCommunities = Locations.Where(l => l.Type <= LocationType.PostalCode.ToType<int>() || l.Type == LocationType.SyntheticGeo.ToType<int>()).ToList();
            SetupLocations(id);
            StartFileGenerationConsumers();

            partnerConfiguration.Stop();
            BdxWatcher.LogWatcher("Configuration Partner Finished ", PartnerId, partnerConfiguration.LogTime());

            Messages.ConfigurationPartnerStopped.LogInfo(new object[] { id }, PartnerId);
        }

        private void OverrideSyntheticInformation(KeyValuePair<SearchLocation, Location> currentMarketLocation)
        {
            _overrideSyntheticInfoWatcher = new Stopwatch();
            _overrideSyntheticInfoWatcher.Start();

            var footerLinksToProcess = new List<FooterLinkLocation>();
            var syntheticsByMarket = SyntheticLocations.Synthetics
                .Where(s => s.IsEqualsTo(currentMarketLocation.Key));

            var synthetics = syntheticsByMarket.ToList();
            if (!synthetics.Any()) return;

            foreach (var location in synthetics)
            {
                var apiResultCounts = ApiService.GetGraphQlSyntheticGeoContent(currentMarketLocation, PartnerId, location);

                if (apiResultCounts == null)
                    continue;

                var newFooterLink = new FooterLinkLocation
                {
                    Location = location.Name,
                    LocationType = location.LocationTypeName,
                    MarketId = location.MarketId,
                    MarketName = currentMarketLocation.Key.Market,
                    MarketState = currentMarketLocation.Key.State,
                    CommunityCount = apiResultCounts.CommCount,
                    HomeCount = apiResultCounts.HomeCount,
                    AveragePlanPrice = apiResultCounts.AveragePrice.ToType<int>()
                };

                footerLinksToProcess.Add(newFooterLink);
            }

            if (LocationsExtendedInformation.ContainsKey(currentMarketLocation.Key))
                LocationsExtendedInformation[currentMarketLocation.Key].SyntheticsByMarket = footerLinksToProcess;

            _overrideSyntheticInfoWatcher.Stop();
            BdxWatcher.LogWatcher("Synthetic Information Finished for " + synthetics.Count + " locations", PartnerId, _overrideSyntheticInfoWatcher.LogTime());
        }

        private void ReadConfigKeys(string id)
        {
            BhiContentFolder = BhiConfiguration.BhiContentFolder(id);
            CityRadius = BhiConfiguration.CityRadius(id);
            DfuMarketsFilePath = BhiConfiguration.DfuMarketsFile(id);
            PostalCodeRadius = BhiConfiguration.PostalCodeRadius(id);
            PartnerDomainUrl = BhiConfiguration.PartnerDomainUrl(id);
            SyntheticFilePath = BhiConfiguration.SyntheticsFile(id);
            SpecialFilteredFile = BhiConfiguration.SpecialFilteredFile;
        }

        private void ReadInformationFromXml()
        {
            _informationFromXmlWatcher = new Stopwatch();
            _informationFromXmlWatcher.Start();

            try
            {
                if (PartnerId != PartnersConsts.CNA)
                {
                    SyntheticLocations = XmlDeserialize.Deserialize<SyntheticGeoNames>(BhiContentFolder + SyntheticFilePath);
                }

                FriendlyNamingMarkets = XmlDeserialize.Deserialize<DfuMarkets>(BhiContentFolder + DfuMarketsFilePath);
                SpecialFilterLinks = XmlDeserialize.Deserialize<SpecialFilteredSrp>(BhiContentFolder + SpecialFilteredFile);

                _informationFromXmlWatcher.Stop();
                BdxWatcher.LogWatcher("XML information has been read ", PartnerId, _informationFromXmlWatcher.LogTime());
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }

        private void RegisterLocationInformation(SearchLocation currentLocation, Markets specialFilteredMarkets)
        {
            FilteredMarket filteredByMarket;
            var key = new SearchLocation
            {
                MarketId = currentLocation.MarketId,
                Type = currentLocation.Type == LocationType.SyntheticGeo.ToType<int>() ? LocationType.Market.ToType<int>() : currentLocation.Type,
                Location = currentLocation.Type == LocationType.SyntheticGeo.ToType<int>() || currentLocation.Type == LocationType.Market.ToType<int>() ?currentLocation.MarketId.ToString() : currentLocation.Location.Trim()
            };

            if (SpecialFiltersByMarket.ContainsKey(currentLocation.MarketId))
            {
                filteredByMarket = SpecialFiltersByMarket[currentLocation.MarketId];
            }
            else
            {
                filteredByMarket = specialFilteredMarkets.Market.FirstOrDefault(filterByMarket => filterByMarket.Id == currentLocation.MarketId);
                SpecialFiltersByMarket.TryAdd(currentLocation.MarketId, filteredByMarket);
            }

            FooterLinks.TryGetValue(key, out var nearbyLocations);
            Brands.TryGetValue(key, out var nearbyBrands);
            MarketsSummary.TryGetValue(currentLocation.MarketId, out var marketInformation);
            SchoolDistricts.TryGetValue(key, out var nearbySchoolDistricts);
            NewestCommunities.TryGetValue(key, out var nearbyNewestCommunities);

            LocationsExtendedInformation.TryAdd(currentLocation, new Location
            {
                BrandsInformation = BrandsInformation.Where(x => x.market_id == currentLocation.MarketId),
                Brands = nearbyBrands == null ? new List<BrandLocation>() : nearbyBrands.ToList(),
                NearByCities = nearbyLocations == null ? new List<FooterLinkLocation>() : nearbyLocations.Where(x => !string.IsNullOrEmpty(x.City)).ToList(),
                NearByCounties = nearbyLocations == null ? new List<FooterLinkLocation>() : nearbyLocations.Where(x => !string.IsNullOrEmpty(x.County)).ToList(),
                NearByPostalCodes = nearbyLocations == null ? new List<FooterLinkLocation>() : nearbyLocations.Where(x => !string.IsNullOrEmpty(x.PostalCode)).ToList(),
                LocationInformation = FooterLinkLocations.FirstOrDefault(currentLocation.FindLocationAsFooterLink()),
                MarketInformation = marketInformation,
                FilteredMarketLinks = filteredByMarket,
                NearBySchoolDistricts = nearbySchoolDistricts == null ? new List<FooterLinkLocation>() : nearbySchoolDistricts.Where(x => !string.IsNullOrEmpty(x.DistrictName)).ToList(),
                NearByNewestCommunies = nearbyNewestCommunities == null ? new List<NewestCommunities>() : nearbyNewestCommunities.Where(x => x.MarketId == currentLocation.MarketId).ToList()
            });
        }

        private void SetupLocations(string id)
        {
            _setupLocationWatcher = new Stopwatch();
            _setupLocationWatcher.Start();

            var totalLocations = LocationsWithoutCommunities.Count;
            var degreeOfParallelism = Environment.ProcessorCount;

            Parallel.For(0, degreeOfParallelism, workerId =>
            {
                var max = totalLocations * (workerId + 1) / degreeOfParallelism;
                var index = totalLocations * workerId / degreeOfParallelism;

                for (; index < max; index += 1)
                {
                    RegisterLocationInformation(LocationsWithoutCommunities[index], SpecialFilterLinks.Markets);

                    if (index % 5000 == 0)
                        Messages.ConfigurationPartnerLocationsProcessed.LogInfo(new object[] { id, index, totalLocations }, PartnerId);
                }

            });

            _setupLocationWatcher.Stop();
            BdxWatcher.LogWatcher("Setup Location finished", PartnerId, _setupLocationWatcher.LogTime());
        }

        private void StartFileGenerationConsumers()
        {
            var replaceCodesCounts = 0;

            var fileGenerationConsumersWatcher = new Stopwatch();
            fileGenerationConsumersWatcher.Start();

            FileCreationConsumers = new Task[1];

            FileCreationConsumers[0] = Task.Factory.StartNew(() =>
            {
                foreach (var file in ReplaceCodesFileToCreate.GetConsumingEnumerable())
                {
                    Interlocked.Increment(ref replaceCodesCounts);

                    if (file.IsXml)
                    {
                        XmlGenerator.GenerateXmlAsync(file.Xml, BhiContentFolder, file.Function, file.Section, file.FileName, this);
                    }
                    
                    ReplaceCodesCounts = replaceCodesCounts;
                }
            });
            
            fileGenerationConsumersWatcher.Stop();
            BdxWatcher.LogWatcher("File Generation Consumer ", PartnerId, fileGenerationConsumersWatcher.LogTime());
        }

        private IEnumerable<SearchLocation> SyntheticNamesToLocation(SyntheticGeoNames partnerSynthetic)
        {
            // In Partner location we have a lot of records whose type is "5", that is "communities", in this case the market name corresponds to the City Name,
            // for this reason we need to exclude these record through filter "m.Type != LocationType.Community.ToType<int>()" to avoid build wrong Url in the snippets,
            // like Popular Searches snippets for Synthetic's SRP. Change added for ticket https://builderhomesite.atlassian.net/browse/BDXNHS-4000
            return (from s in partnerSynthetic.Synthetics
                let market = Locations.FirstOrDefault(m => s.MarketId == m.MarketId && !string.IsNullOrEmpty(m.State) && m.Type != LocationType.Community.ToType<int>() && m.Type != LocationType.Developer.ToType<int>())
                where market != null
                select new SearchLocation(s.Name, market.MarketId, market.Market, market.State, LocationType.SyntheticGeo.ToType<int>(), market.MarketStateId)).ToList();
        }
    }
}