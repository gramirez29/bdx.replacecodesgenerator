﻿using CommandLineParser.Arguments;
using CommandLineParser.Validation;

namespace BDX.ReplaceCodesGenerator
{
    public class Params
    {
        [ValueArgument(typeof(string), "filterbylocation", DefaultValue = "", Description = "Filter Location", FullDescription = "Set whether should be generated and filtered by specific criteria (Postal Code, City, Market)")]
        public string FilterByLocation { get; set; }

        [SwitchArgument("replacecodes", false, Description = "ReplaceCodes", FullDescription = "Set whether Replace Codes should be generated")]
        public bool ReplaceCodes { get; set; }
    }
}