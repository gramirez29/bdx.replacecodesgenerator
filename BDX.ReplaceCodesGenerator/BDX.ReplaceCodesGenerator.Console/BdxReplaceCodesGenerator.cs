﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Bdx.Common.StructureMap;
using Bdx.Common.Utility;
using Bdx.Ratings.Client.Api;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Core.Entities;
using BDX.ReplaceCodesGenerator.Core.Modules;
using CommandLineParser.Arguments;
using static BDX.ReplaceCodesGenerator.Common.Constants.PartnersConsts;
using CommandLine = CommandLineParser.CommandLineParser;

namespace BDX.ReplaceCodesGenerator
{
    internal class BdxReplaceCodesGenerator
    {
        private static Stopwatch _fileCreationConsumerWatcher;

        public static void Main(string[] args)
        {
            var dependencyResolver = BooststrapIoc();
            ConfigurePartners(dependencyResolver, args);
            Exit();
        }

        private static void ConfigurePartners(IDependencyResolver dependencyResolver, string[] args)
        {
            var partnerIds = BhiConfiguration.PartnerIds.Split(';').ToList();
            var processInfo = new ProcessInfo();

            if (!partnerIds.Any())
            {
                Exit();
            }

            try
            {
                var @params = new Params();
                var parser = new CommandLine();
                var arguments = string.Empty;

                parser.ExtractArgumentAttributes(@params);
                parser.ParseCommandLine(args);

                arguments = parser.Arguments.Where(s => s.Parsed).Aggregate(arguments,
                    (current, s) =>
                        current + $"--{s.LongName} for {(s.LongName.Equals("filterbylocation") ? ((ValueArgument<string>)parser.Arguments[0]).StringValue : s.Description)}" + Environment.NewLine);

                Parallel.ForEach(partnerIds, new ParallelOptions { MaxDegreeOfParallelism = 3 }, partnerId =>
                {
                    processInfo.StartDateTime = DateTime.Now;

                    var partner = new Partner(partnerId, @params.FilterByLocation);
                    partner.InitializePartner(dependencyResolver.Get<IRatingsApiClient>(), partnerId);

                    if (@params.ReplaceCodes)
                    {
                        ReplaceCodesContent.GenerateReplaceCodes(partner);
                    }
                    else
                    {
                        partner.ReplaceCodesFileToCreate.CompleteAdding();
                    }

                    _fileCreationConsumerWatcher = new Stopwatch();
                    _fileCreationConsumerWatcher.Start();

                    Task.WaitAll(partner.FileCreationConsumers);

                    _fileCreationConsumerWatcher.Stop();
                    BdxWatcher.LogWatcher("File Creation Consumers Process Finished ", partner.PartnerId, _fileCreationConsumerWatcher.LogTime());

                    processInfo.EndDateTime = DateTime.Now;
                    processInfo.Partner = partner;
                    processInfo.Arguments = arguments;

                    GenerateEmail(processInfo);
                    
                });

                SendRequestToHealthMonitor();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }

        private static void Exit()
        {
            Messages.ApplicationHasStopped.LogInfo(NHS);
            Messages.ApplicationHasStopped.LogInfo(CNA);
            Environment.Exit(0);
        }

        private static void GenerateEmail(ProcessInfo emailInfo, string partnerId = "")
        {
            var startDateTime = emailInfo.StartDateTime;
            var endDateTime = emailInfo.EndDateTime;

            var mail = new StringBuilder();
            emailInfo.Partner.PartnerName = emailInfo.Partner.PartnerId == NHS
                ? "NewHomeSource"
                : "CasasNuevasAqui";


            var replaceCodes = emailInfo.Partner.ReplaceCodesCounts;

            mail.AppendLine("  Files Generated for partner " + emailInfo.Partner.PartnerName);
            mail.AppendLine();
            mail.AppendLine("  Replace Codes: " + replaceCodes);
            mail.AppendLine();
            mail.AppendLine("  Total Files Created: " + replaceCodes);

            mail.AppendLine();
            mail.AppendLine("  Start Date: " + startDateTime.ToLocalTime());
            mail.AppendLine("  End Date: " + endDateTime.ToLocalTime());
            mail.AppendLine("  Total time elapsed: " + (endDateTime - startDateTime).ToString(@"hh\:mm\:ss"));
            mail.AppendLine();
            mail.AppendLine("  Console Arguments:");
            mail.AppendLine(emailInfo.Arguments);

            SendEmail(mail.ToString(), emailInfo.Partner.PartnerName);
        }

        private static void SendEmail(string body, string partnerName)
        {
            var mailMessage = new MailMessage(BhiConfiguration.SupportEmail, BhiConfiguration.AudienceEmail)
            {
                Subject = BhiConfiguration.Environment + BhiConfiguration.EmailSubject + partnerName,
                IsBodyHtml = false,
                Body = body
            };

            try
            {
                var smtpClient = new SmtpClient(BhiConfiguration.PrimarySmtpServer);
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                var smtpClient = new SmtpClient(BhiConfiguration.SecondarySmtpServer);

                smtpClient.Send(mailMessage);
                ex.LogError();
            }
        }

        private static void SendRequestToHealthMonitor()
        {
            UtilExtensions.HttpPostCodeStatus(BhiConfiguration.HealthMonitorUrl, HttpStatusCode.Created);
        }

        private static IDependencyResolver BooststrapIoc()
        {
            try
            {
                var container = StructureMapBootstrapper.Bootstrap("Bdx.*");

                return container.GetInstance<IDependencyResolver>();
            }
            catch (Exception ex)
            {
                ex.LogError();
                throw;
            }
            
        }
    }
}