﻿using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public static class XmlSerialize
    {
        private static readonly Regex InsignificantXmlWhitespace = new Regex(@"(?<=>)\s+?(?=<)");

        public static string RemoveInsignificantXmlWhiteSpace(this string xml)
        {
            return InsignificantXmlWhitespace.Replace(xml, string.Empty).Trim();
        }

        public static string ToStringSerialize<T>(this object val) where T : new()
        {
            var w = WriterSerialize<T>(val);
            var xml = w.ToString();

            w.Close();

            return Clean(xml.Trim()).RemoveInsignificantXmlWhiteSpace();
        }

        public static TextWriter WriterSerialize<T>(object objecto) where T : new()
        {
            var w = new StringWriter();
            var s = new XmlSerializer(typeof(T));

            s.Serialize(w, objecto);
            w.Flush();

            return w;
        }

        private static string Clean(string xml)
        {
            var doc = new XmlDocument();

            xml = XElement.Parse(xml).ToString();
            doc.LoadXml(xml);

            var first = doc.FirstChild;

            if (first != null && first.Attributes != null)
            {
                var a = first.Attributes["xmlns:xsd"];

                if (a != null)
                    first.Attributes.Remove(a);

                a = first.Attributes["xmlns:xsi"];

                if (a != null)
                    first.Attributes.Remove(a);
            }

            xml = XElement.Parse(doc.OuterXml).ToString();

            return xml;
        }
    }
}