﻿using System;
using System.Runtime.Caching;
using BDX.ReplaceCodesGenerator.Common.Extensions;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public class BdxStaticContentCache
    {
        private static readonly MemoryCache Cache = new MemoryCache("BDXStaticContentGenerator");

        public static T GetOrAddExisting<T>(string key, Func<T> getItemCallback) where T : class
        {
            var cacheItem = Cache.GetCacheItem(key);

            try
            {
                var oldValue = cacheItem?.Value as T;
                var cachePolicy = new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddDays(1)) };

                if (oldValue != null)
                    return oldValue;

                var item = getItemCallback();

                Cache.Add(key, item, cachePolicy);

                return item;
            }
            catch (Exception ex)
            {
                ex.LogError();
                Cache.Remove(key);
            }

            return default(T);
        }

        public static string GetOrAddExisting(string key, string value)
        {
            var cachePolicy = new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddDays(1)) };
            var oldValue = Cache.AddOrGetExisting(key, value, cachePolicy);

            try
            {
                dynamic item = oldValue ?? value;
                return item;
            }
            catch (Exception ex)
            {
                ex.LogError();
                Cache.Remove(key);
                return string.Empty;
            }
        }

        public static T GetExisting<T>(string key) where T : new()
        {
            dynamic item = Cache.GetCacheItem(key);
            return item;
        }

        public static string GetExisting(string key)
        {
            dynamic item = Cache.GetCacheItem(key);
            return item;
        }
    }
}