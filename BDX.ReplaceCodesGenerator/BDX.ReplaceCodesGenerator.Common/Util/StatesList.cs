﻿using System.Collections.Generic;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public static class StatesList
    {
        public static readonly List<UsState> ListOfUsStates;

        static StatesList()
        {
            ListOfUsStates = new List<UsState>
            {
                new UsState("AL", "ALABAMA"),
                new UsState("AK", "ALASKA"),
                new UsState("AS", "AMERICAN SAMOA"),
                new UsState("AZ", "ARIZONA"),
                new UsState("AR", "ARKANSAS"),
                new UsState("CA", "CALIFORNIA"),
                new UsState("CO", "COLORADO"),
                new UsState("CT", "CONNECTICUT"),
                new UsState("DE", "DELAWARE"),
                new UsState("DC", "DISTRICT OF COLUMBIA"),
                new UsState("FM", "FEDERATED STATES OF MICRONESIA"),
                new UsState("FL", "FLORIDA"),
                new UsState("GA", "GEORGIA"),
                new UsState("GU", "GUAM"),
                new UsState("HI", "HAWAII"),
                new UsState("ID", "IDAHO"),
                new UsState("IL", "ILLINOIS"),
                new UsState("IN", "INDIANA"),
                new UsState("IA", "IOWA"),
                new UsState("KS", "KANSAS"),
                new UsState("KY", "KENTUCKY"),
                new UsState("LA", "LOUISIANA"),
                new UsState("ME", "MAINE"),
                new UsState("MH", "MARSHALL ISLANDS"),
                new UsState("MD", "MARYLAND"),
                new UsState("MA", "MASSACHUSETTS"),
                new UsState("MI", "MICHIGAN"),
                new UsState("MN", "MINNESOTA"),
                new UsState("MS", "MISSISSIPPI"),
                new UsState("MO", "MISSOURI"),
                new UsState("MT", "MONTANA"),
                new UsState("NE", "NEBRASKA"),
                new UsState("NV", "NEVADA"),
                new UsState("NH", "NEW HAMPSHIRE"),
                new UsState("NJ", "NEW JERSEY"),
                new UsState("NM", "NEW MEXICO"),
                new UsState("NY", "NEW YORK"),
                new UsState("NC", "NORTH CAROLINA"),
                new UsState("ND", "NORTH DAKOTA"),
                new UsState("MP", "NORTHERN MARIANA ISLANDS"),
                new UsState("OH", "OHIO"),
                new UsState("OK", "OKLAHOMA"),
                new UsState("OR", "OREGON"),
                new UsState("PW", "PALAU"),
                new UsState("PA", "PENNSYLVANIA"),
                new UsState("PR", "PUERTO RICO"),
                new UsState("RI", "RHODE ISLAND"),
                new UsState("SC", "SOUTH CAROLINA"),
                new UsState("SD", "SOUTH DAKOTA"),
                new UsState("TN", "TENNESSEE"),
                new UsState("TX", "TEXAS"),
                new UsState("UT", "UTAH"),
                new UsState("VT", "VERMONT"),
                new UsState("VI", "VIRGIN ISLANDS"),
                new UsState("VA", "VIRGINIA"),
                new UsState("WA", "WASHINGTON"),
                new UsState("WV", "WEST VIRGINIA"),
                new UsState("WI", "WISCONSIN"),
                new UsState("WY", "WYOMING")
            };
        }

        public class UsState
        {
            public string StateName { get; set; }

            public string StateAbbreviation { get; set; }

            public UsState(string stateAbbreviation, string stateName)
            {
                StateName = stateName;
                StateAbbreviation = stateAbbreviation;
            }
        }
    }
}
