﻿using System;
using System.Configuration;
using System.Threading;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BHI.Configuration;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public static class BhiConfiguration
    {
        private static readonly ReaderWriterLockSlim GetConfigValueLock = new ReaderWriterLockSlim();
        private static readonly string ConnectionStringsConfigGroup = ConfigurationManager.AppSettings["Nhs.ConnectionStrings.ConfigGroup"];
        private static readonly string StaticContent = ConfigurationManager.AppSettings["BDX.StaticContentGenerator"];
        private static readonly string NhsWeb = ConfigurationManager.AppSettings["Nhs.Web"];
        private static readonly string DataBaseName = ConfigurationManager.AppSettings["BDX.StaticContentGenerator.DatabaseName"];

        #region BDX Static Content Generator Keys

        public static string AudienceEmail => GetConfigurationValue("AudienceEmail", StaticContent).ToType<string>();

        public static string EmailSubject => GetConfigurationValue("EmailSubject", StaticContent).ToType<string>();

        public static string Environment => GetConfigurationValue("Environment", StaticContent).ToType<string>();

        public static string HealthMonitorUrl => GetConfigurationValue("HealthMonitorURL", StaticContent).ToType<string>();

        public static string PartnerIds => GetConfigurationValue("PartnerIds", StaticContent).ToType<string>();

        public static string PrimarySmtpServer => GetConfigurationValue("PrimarySMTPServer", StaticContent).ToType<string>();

        public static string SecondarySmtpServer => GetConfigurationValue("SecondarySMTPServer", StaticContent).ToType<string>();

        public static string SpecialFilteredFile => GetConfigurationValue("SpecialFilteredFile", StaticContent).ToType<string>();

        public static string SupportEmail => GetConfigurationValue("SupportEmail", StaticContent).ToType<string>();

        public static string PartnerSitePassword => GetConfigurationValue("PartnerSitePassword", StaticContent).ToType<string>();

        #endregion

        #region NHS Web Group Keys
        public static string DefaultSessionToken => GetConfigurationValue("DefaultSessionToken", NhsWeb).ToType<string>();

        public static bool EnableHttps => GetConfigurationValue("EnableHttps", NhsWeb).ToType<bool>();

        public static string ResourceDomain => GetConfigurationValue("ResourceDomain", NhsWeb);

        public static string WebServicesApiHashingAlgorithm => GetConfigurationValue("WebServicesApiHashingAlgorithm", NhsWeb).ToType<string>();

        public static string WebServicesApiUrl => GetConfigurationValue("WebServicesApiUrl", NhsWeb).ToType<string>();

        #endregion

        #region Partner Specific Keys
        public static string BhiContentFolder(string partnerId)
        {
            return GetConfigurationValue("BhiContentFolder", StaticContent, partnerId).ToType<string>();
        }

        /// <summary>   Partner domain URL. </summary>
        /// <param name="partnerId">    Identifier for the partner. </param>
        /// <returns>   A string. </returns>
        public static string PartnerDomainUrl(string partnerId)
        {
            return GetConfigurationValue("PartnerDomainUrl", StaticContent, partnerId).ToType<string>();
        }

        public static int CityRadius(string partnerId)
        {
            return GetConfigurationValue("CityRadius", StaticContent, partnerId).ToType<int>();
        }

        public static string DfuMarketsFile(string partnerId)
        {
            return GetConfigurationValue("DfuMarketsFile", StaticContent, partnerId).ToType<string>();
        }

        public static int PostalCodeRadius(string partnerId)
        {
            return GetConfigurationValue("PostalCodeRadius", StaticContent, partnerId).ToType<int>();
        }
        
        public static string SyntheticsFile(string partnerId)
        {
            return GetConfigurationValue("SyntheticsFile", StaticContent, partnerId).ToType<string>();
        }

        #endregion

        #region Connection Strings

        public static string ConnectionStringWeb => GetConnectionString("WebDb3");

        #endregion

        #region Methods

        private static string GetConnectionString(string connectionStringName)
        {
            return GetConfigurationValue(connectionStringName, ConnectionStringsConfigGroup).Replace("Application Name=NhsUnknownApp;", "Application Name=BDXStaticContentGenerator;");
        }

        private static string GetConfigurationValue(string key, string configGroup, string partnerId = "")
        {
            var cacheKey = $"ConfigSetting_{key}{partnerId}";
            var partnerCacheKey = $"ConfigSetting_{key}_{partnerId.Trim()}";
            var partnerKey = $"{key}_{partnerId.Trim()}";
            string value = null;

            if (GetConfigValueLock.TryEnterReadLock(new TimeSpan(0, 0, 10)))
            {
                try
                {

                    if (partnerId != "")
                    {
                        value = ConfigurationManager.AppSettings[partnerCacheKey];
                    }

                    if (string.IsNullOrEmpty(value))
                    {
                        value = ConfigurationManager.AppSettings[key];
                    }

                    if (string.IsNullOrEmpty(value))
                    {
                        value = BdxStaticContentCache.GetExisting(cacheKey);
                    }

                    if (value == null)
                    {
                        if (string.IsNullOrEmpty(configGroup))
                        {
                            throw new ArgumentNullException(configGroup);
                        }
                        else
                        {
                            if (partnerId != "")
                            {
                                value = new Config().GetValue(configGroup, partnerKey) as string;
                            }

                            if (string.IsNullOrEmpty(value))
                            {
                                value = new Config().GetValue(configGroup, key) as string;
                            }
                        }
                    }
                }
                finally
                {
                    GetConfigValueLock.ExitReadLock();
                }
            }

            if (!GetConfigValueLock.TryEnterWriteLock(new TimeSpan(0, 0, 10)))
                return value;

            try
            {
                if (value != null)
                {
                    BdxStaticContentCache.GetOrAddExisting(key, value);
                }
            }
            finally
            {
                GetConfigValueLock.ExitWriteLock();
            }
            return value;
        }
    }
        #endregion
}

