﻿using System;
using BDX.ReplaceCodesGenerator.Common.Extensions;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public class BdxWatcher
    {
        public static void LogWatcher(string message, int partnerId, string longTime)
        {
            Console.WriteLine($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with Long Time {longTime} ");
            LoggingExtensions.LogWatcher($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with Long Time {longTime} ", partnerId);
        }

        public static void LogGenerateHtmlWatcher(string message, int partnerId, string longTime)
        {
            Console.WriteLine($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with Long Time {longTime} ");
            LoggingExtensions.LogGenerateHtmlWatcher($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with Long Time {longTime} ", partnerId);
        }

        /// <summary>
        /// Logs delete HTML watcher. 
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="partnerId">Identifier for the partner.</param>
        /// <param name="dataTime">The data time, it has the Log time or the Date time in removal files cases. </param>
        /// <param name="isWatcher">(Optional) True if is watcher, false if not. </param>
        public static void LogDeleteHtmlWatcher(string message, int partnerId, string dataTime, bool isWatcher = false)
        {
            Console.WriteLine($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with {(isWatcher ? "Long Time " : "File Date")} {dataTime} ");
            LoggingExtensions.LogDeleteOldContentWatcher($"{DateTime.Now:MM/dd/yyyy h:mm:ss tt} Message: {message} for Partner {partnerId}, with {(isWatcher ? "Long Time " : "File Date")} {dataTime} ", partnerId);
        }
    }
}
