﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BDX.ReplaceCodesGenerator.Common.Util
{
    public static class XmlDeserialize
    {
        public static T Deserialize<T>(string path) where T : new()
        {
            if (!File.Exists(path)) 
                return new T();

            var file = new XmlDocument();
            file.Load(path);

            return Deserialize<T>(file);
        }

        public static T DeserializeText<T>(string xml) where T : new()
        {
            TextReader reader = new StringReader(xml);
            return Deserialize<T>(reader);
        }

        public static T Deserialize<T>(XmlDocument doc) where T : new()
        {
            TextReader reader = new StringReader(doc.OuterXml);
            return Deserialize<T>(reader);
        }

        public static T Deserialize<T>(TextReader reader) where T : new()
        {
            var ser = new XmlSerializer(typeof(T));
            var newobj = (T)ser.Deserialize(reader);
            reader.Close();
            return newobj;
        }
    }
}