﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class LocationTypes
    {
        public const string Market = "Market";
        public const string PostalCode = "PostalCode";
        public const string City = "City";
        public const string County = "County";
        public const string Community = "Community";
        public const string Developer = "Developer";
        public const string SyntheticGeo = "SyntheticGeo";
        public const string Zip = "Zip";

        // Numeric Location constants
        public const int CityLocation = 2;
        public const int CountyLocation = 3;
        public const int PostalCodeLocation = 4;
    }
}