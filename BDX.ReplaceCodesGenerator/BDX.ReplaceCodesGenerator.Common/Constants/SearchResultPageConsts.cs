﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class SearchResultPageConsts
    {
        public const string Communities = "communities";
        public const string OnYourLot = "on-your-lot";
        public const string CustomHomes = "custom-homes";
        public const string Homes = "homes";
        public const string ManufacturedHomes = "manufactured-homes";
        public const string Manufactured = "manufactured";
        public const string CondosTownHomes = "condo-townhomes";
        public const string CondoDevelopments = "condo-developments";

        public const string CasasPersonalizadas = "casas-personalizadas";
        public const string Casas = "casas";
        public const string Comunidades = "comunidades";
        public const string CasasPrefabricadas = "casasprefabricadas";
        public const string ConstruyaEnSuLote = "construya-en-su-lote";
        public const string Prefabricadas = "prefabricadas";
        public const string Condominios = "condominios";
        public const string Complejos = "complejos";

        public const string SrpCommunityResults = "CommunityResults";
        public const string SrpHomeResults = "HomeResults";
        public const string SrpBoylResults = "BoylResults";
        public const string SrpManufacturedHomes = "Manufactured-Homes";
        public const string SrpCustomHomes = "Custom-Homes";
        public const string SrpManufacturedResults = "ManufacturedResults";
        public const string SrpCondoTownHomes = "CondoTownHomes";
        public const string SrpCondoDevelopments = "CondoDevelopments";
    }
}