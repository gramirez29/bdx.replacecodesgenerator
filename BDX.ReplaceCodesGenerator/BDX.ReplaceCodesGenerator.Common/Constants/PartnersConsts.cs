﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class PartnersConsts
    {
        public const int NHS = 1;
        public const int CNA = 600;

        // Partner String Names
        public const string PartnerNameNHS = "NHS";
        public const string PartnerNameCNA = "CNA";
    }
}
