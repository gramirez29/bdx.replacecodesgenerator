﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class Messages
    {
        public const string ConfigurationPartnerStarted = "Partner {0} has started to load information.";
        public const string ConfigurationPartnerDatabaseReady = "Partner {0} has loaded the information from the database, starting to process the locations.";
        public const string ConfigurationPartnerStopped = "Partner {0} has processed the information, ready to start the generation of files.";
        public const string ConfigurationPartnerLocationsProcessed = "Partner {0} has processed {1} locations from {2} total locations.";
        public const string ApplicationHasStopped = "BDX Static Content Generator has stopped its execution.";
        public const string NoLocationsFound = "Locations for partner {0} were not found.";
        public const string StartReplaceCodesGeneration = "Starting to generate Replace Codes for partner {0}.";
        public const string FinishReplaceCodesGeneration = "Generation of Replace Codes for partner {0} has finished.";

        public const string XDocumentIsNull = "ERROR: XDocument to generated is null for view {0}";
        public const string XDocumentIsSavingXml = "ERROR: XDocument to generated is null for file {0}";
        public const string SavingXmlFile = "Saving XML Filename {0} - For Page {1} - Partner {2}.";

        public const string StartDeletingHtmlFiles = "Starting to delete the oldest for partner {0}.";
        public const string DeletingHtmlFilesProcess = "Deleting HTML Files Process for Partner {0}";
        public const string DeletingRangeDateNotAllowed = "The date defined to delete files should not be in a range between the last month for Partner {0}";
        public const string LocationsPendingForPartner = "Locations {0} pending to process for {2} - Partner {1}.";
    }
}