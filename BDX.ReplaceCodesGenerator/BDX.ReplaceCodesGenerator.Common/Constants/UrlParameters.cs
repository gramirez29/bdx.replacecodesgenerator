﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class UrlParameters
    {
        public const string Market = "market";
        public const string MarketName = "marketname";
        public const string MarketState = "marketstate";
        public const string MarketStateNameOrAbbreviation = "marketstatenameorabbreviation";
        public const string CityNameFilter = "citynamefilter";
        public const string Ciudad = "ciudad";
        public const string Condado = "condado";
        public const string County = "county";
        public const string PostalCode = "postalcode";
        public const string Zip = "zip";
        public const string Area = "area";
        public const string State = "state";
        public const string BrandId = "brandid";

        // Constants for Amenyties parameters in Real State Links
        public const string Function = "Function";
        public const string UrlFunction = "UrlFunction";
        public const string PageFunction = "PageFunction";
    }
}