﻿
namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class AmenitiesConsts
    {
        public const string ActiveAdult = "Active Adult";
        public const string AgeRestricted = "Active AdultAdult";
        public const string ComingSoon = "Coming Soon";
        public const string Condo = "Condo/Townhome";
        public const string CondoTownhome = "Condo-Townhome";
        public const string CondoDevelopments = "Condo-Developments";
        public const string Gated = "Gated";
        public const string Golf = "Golf";
        public const string GreenFeatures = "Green Features";
        public const string HotDeals = "Hot Deals";
        public const string Luxury = "Luxury Homes";
        public const string NatureAreas = "Nature Areas";
        public const string Modular = "Manufactured";
        public const string Parks = "Parks";
        public const string Pool = "Pool";
        public const string Sport = "Sport";
        public const string SchoolDistrict = "School District";
        public const string Views = "Views";
        public const string Waterfront = "Waterfront";
        public const string QuickMoveIn = "Quick move in ";
        public const string Adult = "55+";
        public const string GatedCommunities = "Gated Communities";

        public const string AdultosMayores = "Para adultos mayores";
        public const string Proximamente = "Próximamente";
        public const string Condominio = "Condominio/Complejo";
        public const string Condominios = "Condominios";
        public const string Complejos = "Complejos";
        public const string Cerradas = "Cerradas";
        public const string ConCampoDeGolf = "Con campo de golf";
        public const string Ecologicas = "Características Ecológicas";
        public const string Ofertas = "Ofertas";
        public const string CasasDeLujo = "Casas de Lujo";
        public const string CasasPrefabricadas = "Casas Prefabricadas";
        public const string EspaciosNaturales = "Espacios Naturales";
        public const string Parques = "Parques";
        public const string Piscina = "Piscina";
        public const string DistritoEscolar = "Distrito Escolar";
        public const string Vistas = "Vistas";
        public const string InstalacionesDeportivas = "Instalaciones Deportivas";
        public const string FrenteLago = "Frente al lago";
        public const string MudanzaRapida = "Mudanza Rápida";
        
        public const string AdultParameter = "?adult=true";
        public const string ComingSoonParameter = "?comingsoon=true";
        public const string CondoParameter = "?townhome=true";
        public const string GatedParameter = "?gated=true";
        public const string GolfParameter = "?golfcourse=true";
        public const string GreenParameter = "?greenprogram=true";
        public const string HotDealParameter = "?hotdeals=true";
        public const string LuxuryParameter = "?luxury=true";
        public const string ModularParameter = "?modular=true";
        public const string NatureParameter = "?natureareas=true";
        public const string ParksParameter = "?parks=true";
        public const string PoolParameter = "?pool=true";
        public const string SportParameter = "?sports=true";
        public const string TownhomesParameter = "?townhomes=true";
        public const string ViewsParameter = "?views=true";
        public const string WaterfrontParameter = "?waterfront=true";
        public const string QuickMoveInParameter = "?inventory=true";
        public const string SchoolDistrictParameter = "?schooldistrictid={0}";

        public const string GolfCourseCommunities = "{0} Golf Course Communities";
        public const string WaterLakeAndBeachCommunities = "{0} Waterfront, Lake and Beach Communities";
        public const string CondosAndTownhomes = "{0} Condos and Townhomes";
        public const string ModularHomes = "{0} Modular Homes";
        public const string AdultCommunities = "{0} 55 Plus Communities";
        public const string GatedCommunity = "Gated Communities in {0}";
        public const string AltTextGatedCommunities = "{0} Gated Communities";

        public const string ComunidadesConCampoDeGolf = "Comunidades con Campo de Golf en {0} ";
        public const string ComunidadesFrenteAlAgua = "Comunidades Frente al Agua (Lago/Playa) en {0} ";
        public const string CondominiosCasasDeCuidad = "Comunidades y Casas de Ciudad en {0} ";
        public const string CasasPrebabricadasEn = "Casas Prefabricadas en {0} ";

        public const string GolfFriendlyParameter = "golf-course-homes";
        public const string WaterfrontFriendlyParameter = "waterfront-homes";
        public const string CondoFriendlyParameter = "condos-townhomes";
        public const string ModularFriendlyParameter = "manufactured-homes";
        public const string AdultFriendlyParameter = "active-adult";
        public const string LuxuryFriendlyParameter = "luxury-homes";
        public const string GatedCommunityFriendlyParameter = "gated-communities";

        public const string GolfCourseHomesFunction = "/golf-course-homes";
        public const string WaterfrontHomesFunction = "/waterfront-homes";
        public const string CondoSaleFunction = "/condo-sale";
        public const string CondoTownHomesFunction = "/condos-townhomes";
        public const string ModularHomesFunction = "/modular-homes";
        public const string AdultFunction = "/active-adult";
        public const string LuxuryHomesFunction = "/luxury-homes";
        public const string GatedCommunityFunction = "/gated-communities";

        public const string GreenProgramId = "GreenProgram";
        public const string PoolId = "Pool";
        public const string GolfCourseId = "GolfCourse";
        public const string GatedId = "Gated";
        public const string ParkId = "Parks";
        public const string NatureAreaId = "NatureAreas";
        public const string ViewsId = "Views";
        public const string WaterfrontId = "Waterfront";
        public const string SportFacilitiesId = "Sports";
        public const string FloorPlansId = "FloorPlans";
        public const string ModelHomeId = "Model";
        public const string SingleFamilyId = "SingleFamily";
        public const string AdultId = "Adult";
        public const string AgeRestrictedId = "AgeRestricted";
        public const string TownhomesId = "Townhomes";
        public const string LuxuryId = "Luxury";
        public const string QmiId = "Inventory";
        public const string HotDealsId = "HotDeals";

        // Amenity Consts Configuration values
        public const string AmenityGolfSection = "Golf";
        public const string AmenityGatedCommunitySection = "GatedCommunities";
        public const string AmenityGolfCounterName = "CountGolf";
        public const string AmenityWaterfrontSection = "Waterfront";
        public const string AmenityWaterfrontCounterName = "CountWaterfrontOnly";
        public const string AmenityCondoSection = "CondoTownHome";
        public const string AmenityCondoCounterName = "CountCondo";
        public const string AmenityActiveAdultSection = "ActiveAdult";
        public const string AmenityActiveAdultCounterName = "CountAdult";
        public const string AmenityLuxurySection = "LuxuryHomes";
        public const string AmenityLuxuryCounterName = "CountLuxury";
        public const string AmenityGatedCommunityCounterName = "CountGatedComm";
    }
}