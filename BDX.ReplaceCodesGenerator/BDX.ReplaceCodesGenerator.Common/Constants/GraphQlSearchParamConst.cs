﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class GraphQlSearchParamConst
    {
        public const string PartnerId = "partnerId";
        public const string MarketId = "marketId";
        public const string MarketIds = "marketIds";
        public const string CommunityId = "commId";
        public const string CommunityName = "communityName";
        public const string BuilderId = "builderId";
        public const string PostalCode = "postalCode";
        public const string PostalCodes="postalCodes";
        public const string State = "state";
        public const string City = "city";
        public const string Cities = "cities";
        public const string County = "county";
        public const string Counties = "counties";
        public const string AlphaRestults = "alphaResults";
        public const string NoBoyl = "noBoyl";
    }
}
