﻿namespace BDX.ReplaceCodesGenerator.Common.Constants
{
    public class WebApiMethodsConsts
    {
        public const string Communities = "communities";
        public const string CommunityLocations = "communitylocations";

        // Constants to GraphQl or V3 Api
        public const string SearchAllCommsCounts = "searchAllCommsCounts";
        public const string GraphQl = "graphql";

    }
}
