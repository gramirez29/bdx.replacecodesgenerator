﻿using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class SyntheticExtensions
    {
        public static bool IsEqualsTo(this Synthetic syntheticLocation, SearchLocation otherLocation)
        {
            return syntheticLocation.MarketId.Equals(otherLocation.MarketId);
        }
    }
}
