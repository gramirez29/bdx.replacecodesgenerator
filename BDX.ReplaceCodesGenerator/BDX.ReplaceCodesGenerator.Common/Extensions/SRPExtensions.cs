﻿using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class SrpExtensions
    {
        public static bool IsCommunityOrHomeResults(this string srpName)
        {
            return srpName == SearchResultPageConsts.Communities ||
                   srpName == SearchResultPageConsts.Homes ||
                   srpName == SearchResultPageConsts.Casas ||
                   srpName == SearchResultPageConsts.Comunidades;
        }

        public static string GetModularHomesFunction(this string srpName, int partnerId)
        {
            if (partnerId != PartnersConsts.CNA)
                return srpName == SearchResultPageConsts.Communities ? SearchResultPageConsts.Manufactured : SearchResultPageConsts.ManufacturedHomes;

            return srpName == SearchResultPageConsts.Comunidades ? SearchResultPageConsts.Prefabricadas : SearchResultPageConsts.CasasPrefabricadas;
        }

    }
}
