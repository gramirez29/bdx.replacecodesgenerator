﻿using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class SearchResultPageExtensions
    {
        /// <summary>   Gets a list of names of the functions. </summary>
        /// <value> A list of names of the functions. </value>
        private static Dictionary<string, string> FunctionNames => new Dictionary<string, string>()
        {
            { SearchResultPageConsts.Communities, SearchResultPageConsts.SrpCommunityResults },
            { SearchResultPageConsts.Homes, SearchResultPageConsts.SrpHomeResults },
            { SearchResultPageConsts.Manufactured, SearchResultPageConsts.SrpManufacturedResults },
            { SearchResultPageConsts.OnYourLot, SearchResultPageConsts.SrpBoylResults },
            { SearchResultPageConsts.ManufacturedHomes, SearchResultPageConsts.SrpManufacturedHomes },
            { SearchResultPageConsts.CustomHomes, SearchResultPageConsts.SrpCustomHomes },
            { SearchResultPageConsts.CondoDevelopments,SearchResultPageConsts.SrpCondoDevelopments },
            { SearchResultPageConsts.CondosTownHomes, SearchResultPageConsts.SrpCondoTownHomes },
        };

        /// <summary>   A string extension method that gets function name from srp name. </summary>
        /// <param name="srpName">      The srpName to act on. </param>
        /// <param name="partnerId">    Identifier for the partner. </param>
        /// <returns>   The function name from srp name. </returns>
        public static string GetFunctionNameFromSrpName(this string srpName, int partnerId)
        {
            if (partnerId.Equals(PartnersConsts.CNA))
            {
                return srpName.ToLower();
            }

            var hasValue = FunctionNames.TryGetValue(srpName, out var functionName);

            return hasValue
                ? functionName.ToLower()
                : string.Empty;

        }
    }
}
