﻿using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;
using BDX.ReplaceCodesGenerator.Common.Helpers;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class MarketSummaryExtensions
    {
        public static Dictionary<string, string> GetReferralMarketSummaryParameters(this MarketSummary market, int partnerId)
        {
            var stateNameOrAbbr = partnerId == PartnersConsts.NHS ? market.State_Abbr : market.State_Abbr.GetStateInfo().StateName;

            return new Dictionary<string, string>
            {
                {UrlParameters.Market, market.Market_Id.ToString()},
                {UrlParameters.MarketName, market.Market_Name + "-" + UrlParameters.Area},
                {UrlParameters.MarketStateNameOrAbbreviation, stateNameOrAbbr}
            };
        }

        public static string ReferralName(this MarketSummary market)
        {
            return market.Market_Name + ", " + market.State_Abbr;
        }
    }
}