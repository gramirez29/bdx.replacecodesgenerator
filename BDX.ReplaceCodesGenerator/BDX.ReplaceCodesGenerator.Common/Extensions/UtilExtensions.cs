﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Newtonsoft.Json;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class UtilExtensions
    {
        public static bool HttpPostCodeStatus(string weburl, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            try
            {
                var myHttpReq = (HttpWebRequest)WebRequest.Create(weburl);

                myHttpReq.KeepAlive = false;
                myHttpReq.Timeout = int.MaxValue;
                myHttpReq.Method = "POST";
                myHttpReq.AllowAutoRedirect = true;
                myHttpReq.ContentType = "application/x-www-form-urlencoded";

                var myHttpRes = (HttpWebResponse)myHttpReq.GetResponse();
                return myHttpRes.StatusCode == statusCode;
            }
            catch (Exception e)
            {
                e.LogError();
            }

            return false;
        }

        public static T ToType<T>(this object value)
        {
            //try to convert using modified ChangeType which handles nullables
            try
            {
                if (value == null) return default(T);
                return Convert.IsDBNull(value) ? default(T) : (T)ChangeType(value, typeof(T));
            }

            //if there is an exception then get the default value of parameterized type T
            catch (Exception)
            {
                return default(T);
            }
        }

        public static string ToLowerCase(this string stringToConvert)
        {
            return string.IsNullOrEmpty(stringToConvert) ? string.Empty : stringToConvert.ToLower();
        }

        public static string CutLenghtByWords(this string value, int lenght)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length < lenght)
            {
                return value ?? string.Empty;
            }

            var retValue = value.Substring(0, lenght);
            var lastSpace = retValue.LastIndexOf(' ');

            if (lastSpace > 0)
            {
                retValue = retValue.Substring(0, lastSpace);
            }

            return retValue.Trim();
        }

        public static string ReplaceSpecialCharacters(this string str, string replace)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            var text = Regex.Replace(str, @"[^0-9a-z]", replace, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            text = Regex.Replace(text, @"\" + replace + "+", replace);
            return text.TrimEnd('-');
        }

        public static T ToFromJson<T>(this string jSonString) where T : new()
        {
            return FromJson<T>(jSonString);
        }

        public static T FromJson<T>(string jSonString)
        {
            return JsonConvert.DeserializeObject<T>(jSonString);
        }

        public static string ToJson(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return json;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }

        public static List<List<TSource>> ToColumns<TSource>(this IEnumerable<TSource> source, int columnCount)
        {
            int remainder;
            var finalResults = new List<List<TSource>>();
            var enumerable = source as TSource[] ?? source.ToArray();
            var itemsPerColumn = Math.DivRem(enumerable.Count(), columnCount, out remainder);

            if (remainder != 0)
                itemsPerColumn += 1;

            for (var i = 0; i < columnCount; i++)
            {
                var data = enumerable.Skip(i * itemsPerColumn).Take(itemsPerColumn).ToList();

                if (data.Any())
                    finalResults.Add(data);
            }

            return finalResults;
        }

        /// <summary>
        /// Returns an Object with the specified Type and whose value is equivalent to the specified object.
        /// </summary>
        /// <param name="value">An Object that implements the IConvertible interface.</param>
        /// <param name="conversionType">The Type to which value is to be converted.</param>
        /// <returns>An object whose Type is conversionType (or conversionType's underlying type if conversionType
        /// is Nullable&lt;&gt;) and whose value is equivalent to value. -or- a null reference, if value is a null
        /// reference and conversionType is not a value type.</returns>
        /// <remarks>
        /// This method exists as a workaround to System.Convert.ChangeType(Object, Type) which does not handle
        /// nullables as of version 2.0 (2.0.50727.42) of the .NET Framework. The idea is that this method will
        /// be deleted once Convert.ChangeType is updated in a future version of the .NET Framework to handle
        /// nullable types, so we want this to behave as closely to Convert.ChangeType as possible.
        /// This method was written by Peter Johnson at:
        /// http://aspalliance.com/author.aspx?uId=1026.
        /// </remarks>
        internal static object ChangeType(object value, Type conversionType)
        {
            // Note: This if block was taken from Convert.ChangeType as is, and is needed here since we're
            // checking properties on conversionType below.
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            } // end if

            // If it's not a nullable type, just pass through the parameters to Convert.ChangeType
            if (!conversionType.IsGenericType || conversionType.GetGenericTypeDefinition() != typeof(Nullable<>))
                return Convert.ChangeType(value, conversionType);

            // It's a nullable type, so instead of calling Convert.ChangeType directly which would throw a
            // InvalidCastException (per http://weblogs.asp.net/pjohnson/archive/2006/02/07/437631.aspx),
            // determine what the underlying type is
            // If it's null, it won't convert to the underlying type, but that's fine since nulls don't really
            // have a type--so just return null
            // Note: We only do this check if we're converting to a nullable type, since doing it outside
            // would diverge from Convert.ChangeType's behavior, which throws an InvalidCastException if
            // value is null and conversionType is a value type.
            if (value == null)
                return null;

            // It's a nullable type, and not null, so that means it can be converted to its underlying type,
            // so overwrite the passed-in conversion type with this underlying type
            var nullableConverter = new NullableConverter(conversionType);
            conversionType = nullableConverter.UnderlyingType;

            // Now that we've guaranteed conversionType is something Convert.ChangeType can handle (i.e. not a
            // nullable type), pass the call on to Convert.ChangeType
            return Convert.ChangeType(value, conversionType);
        }

        public static List<string> GetListOfProperties(IEnumerable<PropertyInfo> properties)
        {
            // In both return statements we do the following logic to create correct property name, in this case we use like an example "AveragePrice". With V3 GraphQL
            // the rule is always to send the first letter lower case, you can take a look at in the next case:
            // prop.Name = "AveragePrice"
            // prop.Name[0] = "A"
            // char.ToLower(prop.Name[0]) = "a"
            // prop.Name.Substring(1) = "veragePrice"
            // return has "averagePrice"

            return properties.Select(prop =>
                {
                    if (IsGenericList(prop.PropertyType))
                    {
                        return $"{char.ToLower(prop.Name[0]) + prop.Name.Substring(1)} {{ {string.Join(",",GetListOfProperties(prop.PropertyType.GetGenericArguments()[0].GetProperties()))} }}";
                    }

                    return char.ToLower(prop.Name[0]) + prop.Name.Substring(1);
                }).ToList();
        }


        public static object GetFieldValueFromObject<T>(this T obj, string criteria) where T : new()
        {
            // MarketSummaryInfo we are going to filter by "criteria" -> Condo | Golf | Adult
            var properties = typeof(T).GetProperties();

            var fieldInfo = properties.FirstOrDefault(p => p.Name.Contains(criteria))?.Name ?? string.Empty;

            var fieldValue = obj.GetType().GetProperty(fieldInfo)?.GetValue(obj, null);

            return fieldValue;
        }


        private static bool IsGenericList(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
        }

        public static string LogTime(this Stopwatch stopwatch)
        {
            var ts = stopwatch.Elapsed;
            var info = $"{ts.Hours:00} h :{ts.Minutes:00} m :{ts.Seconds:00} s :{ts.Milliseconds / 10:00} mls";
            return info;
        }

        public static string ToFormattedPrice(this int priceToFormat)
        {
            return priceToFormat > 0 ? $"{priceToFormat:#,##0}" : "0.00";
        }

        public static string EndsWithSlashToRemove(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            var text = value.EndsWith("/") ? value.TrimEnd('/') : value;
            return text.ToLowerCase();
        }

        public static IEnumerable<KeyValuePair<SearchLocation, T>> FilterLocationsExtendedInformationByLocation<T>(this ConcurrentDictionary<SearchLocation, T> locationsExtendedInformation, string locationType, string locationValue) where T : new()
        {
            if (locationType.ToType<int>() == LocationType.Market.ToType<int>())
            {
                return locationsExtendedInformation
                    .Where(x => x.Key.Type == locationType.ToType<int>() && x.Key.MarketId == locationValue.ToType<int>());
            }
            else
            {
                return locationsExtendedInformation
                    .Where(x => x.Key.Type == locationType.ToType<int>() && locationValue.Contains(x.Key.Location));
            }
        }

        public static string ReplaceStateInfoTags(this string value, string stateName, string amenityName)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Replace("[STATENAME]", stateName).Replace("[AMENITYNAME]", amenityName);
        }

        /// <summary>   A DateTime extension method that query if 'value' is an allowed date. </summary>
        /// <param name="value">    An Object that implements the IConvertible interface. </param>
        /// <returns>   True if an allowed date, false if not. </returns>
        public static bool IsAnAllowedDate(this DateTime value)
        {
            var isAllowedDate = value <= DateTime.Now.AddMonths(-1);
            return isAllowedDate;
        }

        /// <summary>   A string extension method that short file name. </summary>
        /// <param name="currentPath">      The currentPath to act on. </param>
        /// <param name="bhiContentPath">   Full pathname of the bhi content file. </param>
        /// <returns>   A string. </returns>
        public static string ToShortFileName(this string currentPath, string bhiContentPath)
        {
            var shortFileName = currentPath.Replace(bhiContentPath, string.Empty);
            return shortFileName;
        }
    }
}
