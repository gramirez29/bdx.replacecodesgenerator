﻿using System;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;
using BDX.ReplaceCodesGenerator.Common.Helpers;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class SearchLocationExtensions
    {
        public static string ToLocationTypeName(this SearchLocation locationType)
        {
            return locationType.Type.ToLocationTypeName();
        }

        public static string ToLocationTypeName(this int locationType)
        {
            switch (locationType)
            {
                case 1:
                    return LocationTypes.Market;
                case 2:
                    return LocationTypes.City;
                case 3:
                    return LocationTypes.County;
                case 4:
                    return LocationTypes.PostalCode;
                case 5:
                    return LocationTypes.Community;
                case 6:
                    return LocationTypes.Developer;
                case 7:
                    return LocationTypes.SyntheticGeo;
                default:
                    return string.Empty;
            }
        }

        public static int ToLocationTypeValue(this string locationType)
        {
            switch (locationType.Trim())
            {
                case LocationTypes.Market:
                    return LocationType.Market.ToType<int>();
                case LocationTypes.City:
                    return LocationType.City.ToType<int>();
                case LocationTypes.County:
                    return LocationType.County.ToType<int>();
                case LocationTypes.PostalCode:
                case LocationTypes.Zip:
                    return LocationType.PostalCode.ToType<int>();
                case LocationTypes.Community:
                    return LocationType.Community.ToType<int>();
                case LocationTypes.Developer:
                    return LocationType.Developer.ToType<int>();
                case LocationTypes.SyntheticGeo:
                    return LocationType.SyntheticGeo.ToType<int>();
                default:
                    return 0;
            }
        }

        public static Func<FooterLinkLocation, bool> FindLocationAsFooterLink(this SearchLocation locationType)
        {
            Func<FooterLinkLocation, bool> filterByLocationType;
            Func<FooterLinkLocation, bool> filterByMarket = footerLinkLocation => footerLinkLocation.MarketId2 == locationType.MarketId;

            // Footer Link Location does not return the name of the Market in the Location field, instead it uses the Market ID.
            // Synthetic Geo are generated using the Market IDs.
            switch (locationType.Type)
            {
                case LocationTypes.CityLocation:
                    filterByLocationType = footerLinkLocation => string.Equals(footerLinkLocation.City, locationType.Location.Trim());
                    break;
                case LocationTypes.CountyLocation:
                    filterByLocationType = footerLinkLocation => string.Equals( $"{footerLinkLocation.County} {footerLinkLocation.LocationType.Trim()}", locationType.Location.Trim());
                    break;
                case LocationTypes.PostalCodeLocation:
                    filterByLocationType = footerLinkLocation => string.Equals(footerLinkLocation.PostalCode?.Trim(), locationType.Location.Trim());
                    break;
                default:
                    return filterByMarket;
            }

            return filterByLocationType.And(filterByMarket);
        }

        public static string ToStaticContentGeneratorFileName(this SearchLocation location)
        {
            if(location.Type == LocationType.Market.ToType<int>())
                return string.Format("{1}_{0}_{1}", location.ToLocationTypeName(), location.MarketId);
            
            return location.Type == LocationType.SyntheticGeo.ToType<int>() ? string.Format("Synthetic_{0}_{1}", location.Location.Trim().Replace(" ", "_"), location.MarketId)
                                      : string.Format("{2}_{0}_{1}", location.ToLocationTypeName(), location.Location.Trim().Replace(" ", "_"), location.MarketId);
        }

        public static string ToStaticContentGeneratorReplaceCodeFileName(this SearchLocation location)
        {
            if (location.Type == LocationType.Market.ToType<int>())
            {
                return $"{location.MarketId}_{location.ToLocationTypeName()}_{location.Location.Trim().Replace(" ", "_")}";
            }

            return location.Type == LocationType.SyntheticGeo.ToType<int>() ? 
                $"{location.MarketId}_Synthetic_{location.Location.Trim().Replace(" ", "_")}" :
                $"{location.MarketId}_{location.ToLocationTypeName()}_{location.Location.Trim().Replace(" ", "_")}";
        }

        /// <summary>
        /// Gets the URL components from a Search Location
        /// </summary>
        /// <param name="location">Search Location that will be trasnformed to an URL</param>
        /// <param name="state">Full name of the state, as Search Locations only provides the state abbreviation</param>
        /// <param name="isPopularSearches">Indicates that is Popular Searches snippet</param>
        /// <param name="modularTextOrAmenityName">Contains Amenity name or Modular Link text</param>
        /// <returns>Dictionary with the key and value for URL parameters</returns>
        public static Dictionary<string, string> GetReferralSearchLocationParameters(this SearchLocation location, string state, int partnerId)
        {
            var referralLocation = new KeyValuePair<string, string>(string.Empty, string.Empty);

            // Regarding ticket https://builderhomesite.atlassian.net/browse/BDXNHS-2589
            // When files are generated for NHS Brand partner we need to user StateCode to build the URL, but when
            // we are generating URLs for CNA we must use the StateName, so this validation is the responsible to make that,
            // through PartnerId we are able to know which type of parameter we are going to use
            var stateNameOrAbbr = partnerId == PartnersConsts.NHS ? state : state.GetStateInfo().StateName;

            if (location.Type == LocationType.City.ToType<int>())
                referralLocation = new KeyValuePair<string, string>(UrlParameters.CityNameFilter, location.Location);

            if (location.Type == LocationType.County.ToType<int>())
                referralLocation = new KeyValuePair<string, string>(UrlParameters.County, location.Location);

            if (location.Type == LocationType.PostalCode.ToType<int>())
            {
                referralLocation = new KeyValuePair<string, string>(UrlParameters.PostalCode, location.Location);
            }

            return new Dictionary<string, string>
            {
                {UrlParameters.Market, location.MarketId.ToString()},
                {UrlParameters.MarketName, location.Market + "-" + UrlParameters.Area},
                {UrlParameters.State, state},
                {UrlParameters.MarketStateNameOrAbbreviation, stateNameOrAbbr},
                {referralLocation.Key, referralLocation.Value}
            };
        }

        public static bool IsMarketOrCounty(this SearchLocation location)
        {
            return location.Type == LocationType.Market.ToType<int>() || location.Type == LocationType.County.ToType<int>();
        }

        public static bool IsMarket(this SearchLocation location)
        {
            return location.Type == LocationType.Market.ToType<int>();
        }

        public static bool IsValidReplaceCodeLocation(this SearchLocation location)
        {
            return location.Type == LocationType.Market.ToType<int>() ||
                   location.Type == LocationType.City.ToType<int>() ||
                   location.Type == LocationType.County.ToType<int>() ||
                   location.Type == LocationType.PostalCode.ToType<int>() ||
                   location.Type == LocationType.SyntheticGeo.ToType<int>();
        }

        public static bool IsCounty(this SearchLocation location)
        {
            return location.Type == LocationType.County.ToType<int>();
        }
        
        public static bool IsCityOrZipCode(this SearchLocation location)
        {
            return location.Type == LocationType.City.ToType<int>() || location.Type == LocationType.PostalCode.ToType<int>();
        }

        public static bool IsMarketOrIsCityOrIsCounty(this SearchLocation location)
        {
            return location.Type == LocationType.Market.ToType<int>() ||
                   location.Type == LocationType.City.ToType<int>() ||
                   location.Type == LocationType.County.ToType<int>();
        }

        public static bool IsCity(this SearchLocation location)
        {
            return location.Type != LocationType.City.ToType<int>();
        }

        public static bool IsSynthetic(this SearchLocation location)
        {
            return location.Type == LocationType.SyntheticGeo.ToType<int>();
        }

        public static bool IsPoolAmenity(this string amenityName)
        {
            if (amenityName == string.Empty)
            {
                return false;
            }

            return amenityName == AmenitiesConsts.Pool || amenityName == AmenitiesConsts.Piscina;
        }

        public static bool IsGreenAmenity(this string amenityName)
        {
            if (amenityName == string.Empty)
            {
                return false;
            }

            return amenityName == AmenitiesConsts.GreenFeatures || amenityName == AmenitiesConsts.Ecologicas;
        }

        public static bool IsSportAmenity(this string amenityName)
        {
            if (amenityName == string.Empty)
            {
                return false;
            }

            return amenityName == AmenitiesConsts.Sport || amenityName == AmenitiesConsts.InstalacionesDeportivas;
        }

        /// <summary>   A string extension method that query if 'srpName' is community srp. </summary>
        /// <param name="srpName">  The srpName to act on. </param>
        /// <returns>   True if community srp, false if not. </returns>
        public static bool IsCommunitySrp(this string srpName)
        {
            var isCommunitySrp = srpName.Equals(SearchResultPageConsts.Communities) ||
                                 srpName.Equals(SearchResultPageConsts.Comunidades);

            return isCommunitySrp;
        }
    }
}