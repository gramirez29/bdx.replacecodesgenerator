﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDX.ReplaceCodesGenerator.Common.Enums;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class LocationTypeExtensions
    {
        public static bool IsMarketLocation<T>(this KeyValuePair<SearchLocation, T> location) where T : new() => location.Key.Type == LocationType.Market.ToType<int>();

        public static bool IsCityLocation<T>(this KeyValuePair<SearchLocation, T> location) where T : new() => location.Key.Type == LocationType.City.ToType<int>();

        public static bool IsSyntheticLocation<T>(this KeyValuePair<SearchLocation, T> location) where T : new() => location.Key.Type == LocationType.SyntheticGeo.ToType<int>();

        public static bool IsCountyLocation<T>(this KeyValuePair<SearchLocation, T> location) where T : new() => location.Key.Type == LocationType.County.ToType<int>();

        public static bool IsPostalCodeLocation<T>(this KeyValuePair<SearchLocation, T> location) where T : new() => location.Key.Type == LocationType.PostalCode.ToType<int>();
    }
}
