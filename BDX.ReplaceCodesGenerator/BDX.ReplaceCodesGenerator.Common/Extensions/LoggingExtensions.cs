﻿using System;
using System.Threading.Tasks;
using BDX.ReplaceCodesGenerator.Common.Constants;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Web.ApiV3;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;
using log4net;
using log4net.Config;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class LoggingExtensions
    {
        private static readonly object LoggingLocker = new object();
        private static readonly ILog RollingLog = LogManager.GetLogger("RollingLogFileAppender");
        private static readonly ILog ErrorLog = LogManager.GetLogger("ErrorLogFileAppender");
        private static readonly ILog LocationLog = LogManager.GetLogger("LocationLogFileApppender");
        private static readonly ILog FooterLinksLog = LogManager.GetLogger("FooterLinksFileApppender");
        private static readonly ILog PopularSearchesLog = LogManager.GetLogger("PopularSearchesFileApppender");
        private static readonly ILog GoogleSnippetCitiesLog = LogManager.GetLogger("GoogleSnippetCitiesFileApppender");
        private static readonly ILog GoogleSnippetBrandsLog = LogManager.GetLogger("GoogleSnippetBrandsFileApppender");
        private static readonly ILog GoogleSnippetAmenityLog = LogManager.GetLogger("GoogleSnippetAmenityFileApppender");
        private static readonly ILog GoogleSnippetCommunityLog = LogManager.GetLogger("GoogleSnippetCommunityFileApppender");
        private static readonly ILog GoogleSnippetSyntheticLog = LogManager.GetLogger("GoogleSnippetSyntheticFileApppender");
        private static readonly ILog HousingMarketLog = LogManager.GetLogger("HousingMarketFileApppender");
        private static readonly ILog RealEstateLog = LogManager.GetLogger("RealEstateFileApppender");
        private static readonly ILog MarketIndexLog = LogManager.GetLogger("MarketIndexFileApppender");
        private static readonly ILog CityIndexLog = LogManager.GetLogger("CityIndexFileApppender");
        private static readonly ILog ZipIndexLog = LogManager.GetLogger("ZipIndexFileApppender");
        private static readonly ILog AdultLog = LogManager.GetLogger("AdultFileApppender");
        private static readonly ILog LogHomeBuyingTools = LogManager.GetLogger("HomeBuyingToolsApppender");
        private static readonly ILog LogLandingReviews = LogManager.GetLogger("LandingReviewsFileApppender");
        private static readonly ILog SyntheticCountsLog = LogManager.GetLogger("SyntheticCountsLogFileApppender");
        private static readonly ILog LogLearningCenter = LogManager.GetLogger("LearningCenterFileApppender");
        private static readonly ILog LogLearningCenterCustomHomes = LogManager.GetLogger("LearningCenterCustomHomesFileApppender");
        private static readonly ILog LogStopWatcher = LogManager.GetLogger("LogStopWatcherFileApppender");
        private static readonly ILog LogGenerateHtml = LogManager.GetLogger("LogGenerateHtmlFileApppender");
        private static readonly ILog ReplaceCodesLogs = LogManager.GetLogger("ReplaceCodesFileApppender");
        private static readonly ILog DeleteHtmlFileLogs = LogManager.GetLogger("DeleteHtmlOldFilesAppender");

        private static void SetupLog4Net(int partnerId)
        {
            var partnerName = PartnersConsts.PartnerNameNHS;

            if (partnerId == PartnersConsts.CNA)
                partnerName = PartnersConsts.PartnerNameCNA;

            GlobalContext.Properties["Partner"] = partnerName;
            XmlConfigurator.Configure();

        }
        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="partnerId">Partner to log the message</param>
        public static void LogInfo(this string message, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                MDC.Set("message", $"INFO: {message}");
                RollingLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId">Partner to log the message</param>
        public static void LogInfo(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);

                MDC.Set("message", $"INFO: {message}");
                RollingLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the Invalid Location logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="location">Footer Link Location to log</param>
        /// <param name="partnerId"></param>
        public static void LogInvalidLocation(this FooterLinkLocation location, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                MDC.Set("message", $"INFO: {location}");
                LocationLog.Info($"INFO: {location}");
            }
        }

        /// <summary>
        /// Logs a informative message to the Invalid Location logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="location">Brand Location to log</param>
        /// <param name="partnerId"></param>
        public static void LogInvalidLocation(this BrandLocation location, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                MDC.Set("message", $"INFO: {location}");
                LocationLog.Info($"INFO: {location}");
            }
        }

        /// <summary>
        /// Logs a informative message to the Invalid Location logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="location">Footer Link Location to log</param>
        /// <param name="partnerId"></param>
        public static void LogInvalidLocation(this SearchLocation location, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: {location}");
                    LocationLog.Info($"INFO: {location}");
                }
            });
        }

        /// <summary>
        /// Logs a informative message to the Invalid Location logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="location">Footer Link Location to log</param>
        /// <param name="partnerId"></param>
        public static void LogInvalidLocation(this MarketSummary location, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: {location}");
                    LocationLog.Info($"INFO: {location}");
                }
            });
        }

        /// <summary>
        /// Logs a informative message to the Invalid Location logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="location">Footer Link Location to log</param>
        /// <param name="partnerId"></param>
        public static void LogInvalidLocation(this NewestCommunities location, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: MarketId: {location.MarketId} CommunityId: {location.CommunityId} BrandId: {location.BrandId}");                    LocationLog.Info($"INFO: MarketId: {location.MarketId} CommunityId: {location.CommunityId} BrandId: {location.BrandId}");
                    LocationLog.Info($"INFO: MarketId: {location.MarketId} CommunityId: {location.CommunityId} BrandId: {location.BrandId}");
                }
            });
        }

        /// <summary>
        /// Logs a error message to the logs. File Error_{yyyymmdd}.log
        /// </summary>
        /// <param name="ex">Exception to log</param>
        public static void LogError(this Exception ex)
        {
            lock (LoggingLocker)
            {
                MDC.Set("error", $"ERROR: {ex}");
                ErrorLog.Error($"ERROR: {ex}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogFooterLink(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                FooterLinksLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogReplaceCodes(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                ReplaceCodesLogs.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogPopularSearches(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                PopularSearchesLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogModularHomes(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                PopularSearchesLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LandingReviewsLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                LogLandingReviews.Info($"INFO: {message}");
            }
        }
        
        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogGoogleSnippetCities(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                GoogleSnippetCitiesLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogGoogleSnippetBrands(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                GoogleSnippetBrandsLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogGoogleSnippetCommunity(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                GoogleSnippetCommunityLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogGoogleSnippetSynthetic(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                GoogleSnippetSyntheticLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogGoogleSnippetAmenity(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                GoogleSnippetAmenityLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogHousingMarketLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                HousingMarketLog.Info($"INFO: {message}");

            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogRealEstateLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                RealEstateLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogMarketIndexLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                MarketIndexLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogCityIndexLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                CityIndexLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogZipIndexLog(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                ZipIndexLog.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogDeleteOldContent(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                DeleteHtmlFileLogs.Info($"INFO: {message}");
            }
        }

        /// <summary>
        /// Logs a informative message to the logs. File Log_{yyyymmdd}.log
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="args">Information to replace in the message to log</param>
        /// <param name="partnerId"></param>
        public static void LogLearningCenterContent(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                LogLearningCenter.Info($"INFO: {message}");
            }
        }

        public static void LogLearningCenterCustomHomeContent(this string message, object[] args, int partnerId)
        {
            lock (LoggingLocker)
            {
                SetupLog4Net(partnerId);
                message = string.Format(message, args);
                MDC.Set("message", $"INFO: {message}");
                LogLearningCenterCustomHomes.Info($"INFO: {message}");
            }
        }

        public static void LogInvalidsCounters(this SearchAllCommsCounts results, int partnerId, Synthetic syntheticInfo)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: MarketId: {syntheticInfo.MarketId} Synthetic Name: {syntheticInfo.Name} CommunityCounts: {results.CommCount} HomeCounts: {results.HomeCount} AveragePrice: {results.AveragePrice}");
                    SyntheticCountsLog.Info($"INFO: MarketId: {syntheticInfo.MarketId} Synthetic Name: {syntheticInfo.Name} CommunityCounts: {results.CommCount} HomeCounts: {results.HomeCount} AveragePrice: {results.AveragePrice}");
                }
            });
        }

        public static void LogWatcher(string message, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: Message: {message} ");
                    LogStopWatcher.Info($"INFO: Message: {message} ");
                }
            });
        }

        public static void LogGenerateHtmlWatcher(string message, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: Message: {message} ");
                    LogGenerateHtml.Info($"INFO: Message: {message} ");
                }
            });
        }

        /// <summary>Logs delete old content watcher. </summary>
        /// <param name="message">Message to log. </param>
        /// <param name="partnerId">Partner to log the message. </param>
        public static void LogDeleteOldContentWatcher(string message, int partnerId)
        {
            Task.Run(() =>
            {
                lock (LoggingLocker)
                {
                    SetupLog4Net(partnerId);
                    MDC.Set("message", $"INFO: Message: {message} ");
                    DeleteHtmlFileLogs.Info($"INFO: Message: {message} ");
                }
            });
        }
    }
}