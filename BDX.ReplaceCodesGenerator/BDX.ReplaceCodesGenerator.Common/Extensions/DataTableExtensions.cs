﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class DataTableExtensions
    {
        public static IList<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            return (from object row in table.Rows
                    select CreateItemFromRow<T>((DataRow)row, properties)).ToList();
        }


        private static T CreateItemFromRow<T>(DataRow row, IEnumerable<PropertyInfo> properties) where T : new()
        {
            var item = new T();

            foreach (var property in properties)
            {
                if (!row.Table.Columns.Contains(property.Name))
                    continue;

                var value = row[property.Name] != DBNull.Value ? row[property.Name] : null;
                property.SetValue(item, value, null);
            }

            return item;
        }
    }
}