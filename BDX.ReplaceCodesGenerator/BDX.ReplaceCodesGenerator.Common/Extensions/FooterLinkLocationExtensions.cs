﻿using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Helpers;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class FooterLinkLocationExtensions
    {
        public static Dictionary<string, string> GetReferralFooterLinkLocationParameters(this FooterLinkLocation location, int partnerId)
        {
            var referralLocation = new KeyValuePair<string, string>(string.Empty, string.Empty);
            var stateNameOrAbbr = partnerId == PartnersConsts.NHS ? location.MarketState.GetStateInfo().StateAbbreviation : location.MarketState;

            if (!string.IsNullOrEmpty(location.City))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.CityNameFilter, location.City);

            if (!string.IsNullOrEmpty(location.County))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.County, location.County + $" {LocationTypes.County}");

            if (!string.IsNullOrEmpty(location.PostalCode))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.PostalCode, location.PostalCode);

            return new Dictionary<string, string>
            {
                {UrlParameters.Market, location.MarketId2.ToString()},
                {UrlParameters.MarketName, location.MarketName + "-" + UrlParameters.Area},
                {UrlParameters.State, location.StateName},
                {UrlParameters.MarketStateNameOrAbbreviation, stateNameOrAbbr},
                {referralLocation.Key, referralLocation.Value}
            };
        }

        public static string ReferralName(this FooterLinkLocation location)
        {
            if (!string.IsNullOrEmpty(location.City))
                return location.City;

            if (!string.IsNullOrEmpty(location.County))
                return location.County;

            return !string.IsNullOrEmpty(location.PostalCode) ? location.PostalCode : location.MarketName;
        }

        public static string GetAveragePlanPrice(this FooterLinkLocation location)
        {
            return location.AveragePlanPrice > 0 ? "$" + location.AveragePlanPrice.ToString("#,##0") : "-";
        }

        /// <summary>   A FooterLinkLocation extension method that gets market state. </summary>
        /// <param name="location"> The location to act on. </param>
        /// <returns>   The market state. </returns>
        public static string GetMarketState(this FooterLinkLocation location)
        {
            // For the Urgency sense I have added this code here to fix the issue reported in the ticket https://builderhomesite.atlassian.net/browse/BDXNHS-6943
            // Today April 14, 2021 the FooterLinks Stored procedure is returned 1 record with wrong state for some counties in Baltimore (Id. 142). This is a hack to fix them.
            // I will create a new ticket to validate it with Database team as soon as I can. For now, when we need to retrieve the State for a Baltimore location
            // we are going to set the State instead of MarketState value.  
            if (location == null)
            {
                return string.Empty;
            }

            return location.MarketId2.Equals(142) && !location.MarketState.GetStateInfo().StateAbbreviation.Equals(location.State) 
                ? location.State.GetStateInfo().StateName
                : location.MarketState;
        }

        public static int SetCountForHotDeals(this FooterLinkLocation linkLocation, bool isCommunity = false)
        {
            var hotdealCounter = 0;

            if (isCommunity)
            {
                if (linkLocation.CommunityRegularCount >= linkLocation.HotCommCount)
                {
                    if (linkLocation.GreenMfrHomesCount <= 0)
                    {
                        hotdealCounter = linkLocation.HotCommCount;
                    }
                }

                return hotdealCounter;
            }

            if (linkLocation.CommunityRegularCount >= linkLocation.HotHomeCount)
            {
                if (linkLocation.GreenMfrHomesCount <= 0)
                {
                    hotdealCounter = linkLocation.HotHomeCount;
                }
            }

            return hotdealCounter;
        }

        public static int SetCountForGreenFeatures(this FooterLinkLocation linkLocation, bool isCommunity = false)
        {
            var greenCounter = 0;

            if (isCommunity)
            {
                if (linkLocation.CommunityRegularCount >= linkLocation.GreenCommunityCount)
                {
                    if (linkLocation.GreenMfrHomesCount <= 0)
                    {
                        greenCounter = linkLocation.GreenCommunityCount;
                    }
                }

                return greenCounter;
            }

            if (linkLocation.CommunityRegularCount >= linkLocation.GreenHomeCount)
            {
                if (linkLocation.GreenMfrHomesCount <= 0)
                {
                    greenCounter = linkLocation.GreenHomeCount;
                }
            }

            return greenCounter;
        }
    }
}