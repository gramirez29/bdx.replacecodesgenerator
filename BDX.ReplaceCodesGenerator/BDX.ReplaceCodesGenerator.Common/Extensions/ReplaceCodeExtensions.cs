﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class ReplaceCodeExtensions
    {
        /// <summary>
        /// A List BrandLocation extension method that converts this object to a ranked brands.
        /// </summary>
        /// <param name="brands">           The brands to act on. </param>
        /// <param name="isCommunityCount"> (Optional) True if is community count, false if not. </param>
        /// <returns>   The given data converted to a BrandRank. </returns>
        public static BrandRank ToRankedBrands(this List<BrandLocation> brands, bool isCommunityCount = false)
        {
            var brandRank = new BrandRank();
            var propertyCount = 1;

            var topBrands = brands
                .OrderByCommunityOrHomeCount(isCommunityCount).Take(3)
                .Select((b, index) => new BrandRank
                {
                    BrandName = b.Brand_Name,
                }).ToList();

            Type objBrand = brandRank.GetType();

            foreach (var topBrand in topBrands)
            {
                var brandRankProperty = objBrand.GetProperty($"BrandRank{propertyCount}");

                if (brandRankProperty != null)
                {
                    brandRankProperty.SetValue(brandRank, topBrand.BrandName, null);
                }

                propertyCount++;
            }

            return brandRank;
        }

        /// <summary>
        /// A List BrandLocation extension method that converts the brands to an amenity ranked
        /// brands.
        /// </summary>
        /// <param name="brands">   The brands to act on. </param>
        /// <returns>   Brands as a BrandRank. </returns>
        public static BrandRank ToAmenityRankedBrands(this List<BrandLocation> brands)
        {
            var brandRank = new BrandRank();
            var propertyCount = 1;

            Type objBrand = brandRank.GetType();

            foreach (var topBrand in brands)
            {
                var brandRankProperty = objBrand.GetProperty($"BrandRank{propertyCount}");

                if (brandRankProperty != null)
                {
                    brandRankProperty.SetValue(brandRank, topBrand.Brand_Name, null);
                }

                propertyCount++;
            }

            return brandRank;
        }

        /// <summary>
        /// A List BrandLocation extension method that order by community or home count.
        /// </summary>
        /// <param name="brands">       The brands to act on. </param>
        /// <param name="isCommunity">  True if is community, false if not. </param>
        /// <returns>   A List BrandLocation </returns>
        private static List<BrandLocation> OrderByCommunityOrHomeCount(this List<BrandLocation> brands, bool isCommunity)
        {
            var orderedList = isCommunity
                ? brands.OrderByDescending(b => b.Community_Count)
                : brands.OrderByDescending(b => b.Home_Count);

            return orderedList.ToList();
        }
    }
}
