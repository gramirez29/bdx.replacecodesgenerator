﻿using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Helpers;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class BrandLocationExtensions
    {
        public static string GetLocationName(this BrandLocation brand)
        {
            switch (brand.Location.Trim())
            {
                case LocationTypes.City:
                    return brand.City;
                case LocationTypes.PostalCode:
                    return !string.IsNullOrEmpty(brand.Postal_Code) ? brand.Postal_Code : string.Empty;
                case LocationTypes.County:
                    return brand.County;
                case LocationTypes.Market:
                    return brand.Market_Name;
                default:
                    return brand.Market_Name;
            }
        }

        public static string GetLocationNameAsFooterLink(this BrandLocation brand)
        {
            switch (brand.Location.Trim())
            {
                case LocationTypes.City:
                    return brand.City;
                case LocationTypes.PostalCode:
                    return !string.IsNullOrEmpty(brand.Postal_Code) ? brand.Postal_Code : string.Empty;
                case LocationTypes.County:
                    return brand.County + " County";
                case LocationTypes.Market:
                    return brand.Market_Id.ToString();
                default:
                    return brand.Market_Id.ToString();
            }
        }

        public static Dictionary<string, string> GetReferralBrandLocationParameters(this BrandLocation location, int partnerId)
        {
            var referralLocation = new KeyValuePair<string, string>(string.Empty, string.Empty);
            var stateNameOrAbbr = partnerId == PartnersConsts.NHS ? location.Market_State.GetStateInfo().StateAbbreviation : location.Market_State;

            if (!string.IsNullOrEmpty(location.City))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.CityNameFilter, location.City);

            if (!string.IsNullOrEmpty(location.County))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.County, location.County + $" {LocationTypes.County}");

            if (!string.IsNullOrEmpty(location.Postal_Code))
                referralLocation = new KeyValuePair<string, string>(UrlParameters.PostalCode, location.Postal_Code.ToString());

            return new Dictionary<string, string>
            {
                {UrlParameters.Market, location.Market_Id.ToString()},
                {UrlParameters.MarketName, location.Market_Name + "-" + UrlParameters.Area},
                {UrlParameters.MarketStateNameOrAbbreviation, stateNameOrAbbr },
                {UrlParameters.BrandId, location.Brand_Id.ToString()},
                {referralLocation.Key, referralLocation.Value}
            };
        }

        public static bool IsModularBuilder(this BrandLocation location)
        {
            return location.Modular_Count > 0;
        }

        public static bool IsCustomBuilder(this BrandLocation location)
        {
            return location.Custom_Count > 0;
        }

        public static string GetBuilderFunction(this BrandLocation location, int partnerId)
        {
            var isCna = partnerId == PartnersConsts.CNA;

            if (location.IsCustomBuilder())
                return isCna ? SearchResultPageConsts.ConstruyaEnSuLote : SearchResultPageConsts.OnYourLot;

            if (location.IsModularBuilder())
                return isCna ? SearchResultPageConsts.Prefabricadas : SearchResultPageConsts.Manufactured;

            return isCna ? SearchResultPageConsts.Comunidades : SearchResultPageConsts.Communities;
        }
    }
}