﻿using System;

namespace BDX.ReplaceCodesGenerator.Common.Extensions
{
    public static class LambdaExtensions
    {
        public static Func<T, bool> And<T>(this Func<T, bool> left, Func<T, bool> right)
        {
            return a => left(a) && right(a);
        }
    }
}