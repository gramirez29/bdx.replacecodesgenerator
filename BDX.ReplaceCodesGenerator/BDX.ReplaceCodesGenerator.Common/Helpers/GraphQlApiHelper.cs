﻿using BDX.ReplaceCodesGenerator.Common.Constants;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using Bdx.ReplaceCodesGenerator.Model.Web.ApiV3;

namespace BDX.ReplaceCodesGenerator.Common.Helpers
{
    public static class GraphQlApiHelper
    {
        /// <summary>
        /// This method receive a dictionary parameters which has a string like key and object like query parameter,
        /// String value is EndPoint name, in addition we be able send multiple endpoints at the same time
        /// </summary>
        /// <param name="apiResquestParam"></param>
        /// <returns></returns>
        public static string CreateGraphQlQuery(Dictionary<string, WebApiV3SearchParams> apiResquestParam)
        {
            var queryList = new List<string>();
            var requestParamDictionary = apiResquestParam;

            foreach (var param in requestParamDictionary)
            {
                var webApiEndpoint = param.Key;
                var searchParams = param.Value;
                var query = $"{webApiEndpoint}({CreateGraphQlRequest(searchParams)}){{{CreateGraphQlResponse(webApiEndpoint)}}}";

                queryList.Add(query);
            }

            var separetedQueries = string.Join(",", queryList);
            var queryResult = $"{{\"query\": \"query {{ {separetedQueries} }}\"}}";

            return queryResult;
        }

        /// <summary>
        /// This method will create dinamically the query parameters needed to get data from GraphQL, we can add more parameters if is needed
        /// </summary>
        /// <param name="searchParams"></param>
        /// <returns></returns>
        private static string CreateGraphQlRequest(WebApiV3SearchParams searchParams)
        {
            var @params = new List<string>();

            if(searchParams.PartnerId > 0)
                AddParameter(@params, GraphQlSearchParamConst.PartnerId, searchParams.PartnerId);
            if(!string.IsNullOrEmpty(searchParams.State))
                AddParameter(@params, GraphQlSearchParamConst.State, searchParams.State);
            if(!string.IsNullOrEmpty(searchParams.PostalCodes))
                AddParameter(@params, GraphQlSearchParamConst.PostalCodes, searchParams.PostalCodes);
            if (!string.IsNullOrEmpty(searchParams.Cities))
                AddParameter(@params, GraphQlSearchParamConst.Cities, searchParams.Cities);
            if (!string.IsNullOrEmpty(searchParams.MarketIds))
                AddParameter(@params, GraphQlSearchParamConst.MarketIds, searchParams.MarketIds);
            if (searchParams.AlphaResults)
                AddParameter(@params, GraphQlSearchParamConst.AlphaRestults, searchParams.AlphaResults);
            if (searchParams.NoBoyl)
                AddParameter(@params, GraphQlSearchParamConst.NoBoyl, searchParams.NoBoyl);

            return string.Join(",", @params);
        }

        private static void AddParameter(ICollection<string> @params, string paramName, bool paramValue)
        {
            if (paramValue) @params.Add($"{paramName}:{true.ToString().ToLower()}");
        }

        private static void AddParameter(ICollection<string> @params, string paramName, string paramValue)
        {
            if (!string.IsNullOrEmpty(paramValue)) @params.Add($"{paramName}:\\\"{paramValue.ToLower()}\\\"");
        }

        private static void AddParameter(ICollection<string> @params, string paramName, int paramValue)
        {
            if (paramValue > 0) @params.Add($"{paramName}:{paramValue}");
        }

        /// <summary>
        /// This method will build a response regarding to search type and return a string separated by comma, you can add more WebApiMethodsConst is needed
        /// </summary>
        /// <param name="apiMethodName"></param>
        /// <returns></returns>
        private static string CreateGraphQlResponse(string apiMethodName)
        {
            var list = new List<string>();
            switch (apiMethodName)
            {
                case WebApiMethodsConsts.SearchAllCommsCounts:
                    list = UtilExtensions.GetListOfProperties(typeof(SearchAllCommsCounts).GetProperties());
                    break;
            }
            return string.Join(",", list);
        }
    }
}
