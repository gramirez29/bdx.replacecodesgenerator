﻿using BDX.ReplaceCodesGenerator.Common.Util;
using System;
using System.Linq;

namespace BDX.ReplaceCodesGenerator.Common.Helpers
{
    public static class StateInfoHelper
    {
        public static StatesList.UsState GetStateInfo(this string stateAbbreviationOrName)
        {
            stateAbbreviationOrName = stateAbbreviationOrName.ToUpper();

            var state = StatesList.ListOfUsStates.FirstOrDefault(x => x.StateAbbreviation == stateAbbreviationOrName);
            if (state != null)
            {
                return state;
            }

            state = StatesList.ListOfUsStates.FirstOrDefault(x => x.StateName == stateAbbreviationOrName);

            if (state != null)
            {
                return state;
            }

            throw new ArgumentException(
                $"The provided value for state (abbreviation or name) was: {stateAbbreviationOrName} and it was not found in the states static list");
        }
    }
}
