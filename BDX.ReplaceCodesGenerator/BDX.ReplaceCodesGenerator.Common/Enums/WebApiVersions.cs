﻿namespace BDX.ReplaceCodesGenerator.Common.Enums
{
    public enum WebApiVersions
    {
        V2,
        V3
    }
}