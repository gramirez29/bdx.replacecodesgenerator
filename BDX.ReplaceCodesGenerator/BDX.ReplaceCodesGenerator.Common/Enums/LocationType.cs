﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDX.ReplaceCodesGenerator.Common.Enums
{
    public enum LocationType
    {
        Market = 1,
        City = 2,
        County = 3,
        PostalCode = 4,
        Community = 5,
        Developer = 6,
        SyntheticGeo = 7
    }
}
