﻿using Bdx.ReplaceCodesGenerator.Model.Web.ApiV3;

namespace BDX.ReplaceCodesGenerator.Data.Entities.GraphQL
{
    public class GraphQlApiResult
    {
        public SearchAllCommsCounts SearchAllCommsCounts { get; set; }
    }
}
