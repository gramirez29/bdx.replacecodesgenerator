﻿using System;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Data.Entities
{
    public class Amenity
    {
        public Amenity(string name, string parameter, Func<FooterLinkLocation, int> count, string urlFunction = "", string amenityClass = "", string pageFunction = "", string amenityId = "", string image = "")
        {
            Name = name;
            Parameter = parameter;
            Count = count;
            UrlFunction = urlFunction;
            AmenityClass = amenityClass;
            PageFunction = pageFunction;
            Id = amenityId;
            Image = image;
        }

        public Amenity(string amenityId, Func<BrandLocation, int> amenityBrandCount)
        {
            Id = amenityId;
            AmenityBrandCount = amenityBrandCount;
        }

        public string Name { get; private set; }
        
        public string Parameter { get; private set; }
        
        public string UrlFunction { get; private set; }

        public string AmenityClass { get; private set; }

        public string Id { get; private set; }

        public string PageFunction { get; private set; }

        public Func<FooterLinkLocation, int> Count { get; private set; }

        public Func<FooterLinkLocation, bool> FilterFunction 
        {
            get { return location => Count(location) > 0; }
        }

        public Func<BrandLocation, bool> AmenityBrandFilterFunction
        {
            get { return location => AmenityBrandCount(location) > 0; }
        }

        public Func<BrandLocation, int> AmenityBrandCount { get; private set; }

        public Dictionary<string, string> GetReferralAmenityParameters(Amenity amenity, string stateName)
        {
            return new Dictionary<string, string>
            {
                { UrlParameters.Function, "state" },
                { UrlParameters.State, stateName.Replace(" ","-") },
                { UrlParameters.UrlFunction, amenity.UrlFunction },
            };
        }

        public string GetPage(SearchResultPage srp, Amenity amenity)
        {
            return !string.IsNullOrEmpty(amenity.PageFunction) ? amenity.PageFunction : srp.Name;
        }

        public string Image { get; private set; }
    }
}