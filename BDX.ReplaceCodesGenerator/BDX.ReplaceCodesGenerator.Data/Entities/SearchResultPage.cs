﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Data.Entities
{
    public class SearchResultPage
    {
        public SearchResultPage(string pageName, Func<FooterLinkLocation, int> count, Func<BrandLocation, int> brandCount, IEnumerable<Amenity> amenities)
        {
            Name = pageName;
            Count = count;
            Amenities = amenities.ToList();
            BrandCount = brandCount;
        }
        
        public SearchResultPage(string pageName, Func<FooterLinkLocation, int> count, Func<BrandLocation, int> brandCount, Amenity amenity)
        {
            Name = pageName;
            Count = count;
            Amenity = amenity;
            BrandCount = brandCount;
        }

        public SearchResultPage(string pageName, Func<FooterLinkLocation, int> count, Func<BrandLocation, int> brandCount)
        {
            Name = pageName;
            Count = count;
            BrandCount = brandCount;
        }

        public Func<FooterLinkLocation, bool> FilterFunction
        {
            get { return location => Count(location) > 0; } 
        }

        public Func<BrandLocation, bool> BrandFilterFunction
        {
            get { return location => BrandCount(location) > 0; }
        }

        public Func<FooterLinkLocation, int> Count { get; private set; }

        public Func<BrandLocation, int> BrandCount { get; private set; }

        public List<Amenity> Amenities { get; private set; }
        
        public Amenity Amenity { get; private set; }

        public string Name { get; private set; }
    }
}
