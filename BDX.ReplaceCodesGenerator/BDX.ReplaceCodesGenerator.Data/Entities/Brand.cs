﻿using System;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using Bdx.ReplaceCodesGenerator.Model.Web;

namespace BDX.ReplaceCodesGenerator.Data.Entities
{
    public class Brand
    {
        public Brand(string name, Func<BrandLocation, int> count, string function)
        {
            Name = name;
            Count = count;
            Function = function;
        }

        public string Name { get; private set; }

        public string BrandId { get; set; }

        public string BrandName { get; set; }

        public string Function { get; set; }

        public Func<BrandLocation, int> Count { get; private set; }

        public Func<BrandLocation, bool> FilterFunction
        {
            get { return location => Count(location) > 0; }
        }

        public Dictionary<string, string> GetReferralBrandParameters(string brandId, string marketId, string marketName, string state)
        {
            return new Dictionary<string, string>
            {
                {UrlParameters.BrandId, brandId},
                {UrlParameters.Market, marketId},
                {UrlParameters.MarketName, marketName},
                {UrlParameters.State, state}
            };
        }
    }
}
