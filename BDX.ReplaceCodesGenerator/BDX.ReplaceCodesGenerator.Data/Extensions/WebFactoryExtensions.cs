﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Data.Entities;

namespace BDX.ReplaceCodesGenerator.Data.Extensions
{
    /// <summary>   A web factory extensions.
    ///             The code added in this class is shared between WebDatabase and EsWebDatabase to avoid duplicating code. 
    /// </summary>
    public static class WebFactoryExtensions
    {
        static WebFactoryExtensions()
        {
            LoadAmenityConfiguration();
        }

        /// <summary>   The amenity configuration. </summary>
        private static readonly ConcurrentDictionary<string, IEnumerable<Amenity>> AmenityConfiguration = new ConcurrentDictionary<string, IEnumerable<Amenity>>();

        // <summary>   Loads amenity configuration NHS. </summary>
        private static void LoadAmenityConfiguration()
        {
            AmenityConfiguration.TryAdd("AmenityReplaceCodesComms", new List<Amenity>
            {
                new Amenity(AmenitiesConsts.AdultId, linkLocation => linkLocation.AdultCommCount),
                new Amenity(AmenitiesConsts.AgeRestrictedId, linkLocation => linkLocation.AgeRestrictedCommCount),
                new Amenity(AmenitiesConsts.GatedId, linkLocation => linkLocation.GatedCommCount),
                new Amenity(AmenitiesConsts.GolfCourseId, linkLocation => linkLocation.GolfCommCount),
                new Amenity(AmenitiesConsts.GreenProgramId, linkLocation => linkLocation.GreenCommCount),
                new Amenity(AmenitiesConsts.HotDealsId, linkLocation => linkLocation.HotDealsCommCount),
                new Amenity(AmenitiesConsts.LuxuryId, linkLocation => linkLocation.LuxuryCommCount),
                new Amenity(AmenitiesConsts.NatureAreaId, linkLocation => linkLocation.NatureCommCount),
                new Amenity(AmenitiesConsts.ParkId, linkLocation => linkLocation.ParksCommCount),
                new Amenity(AmenitiesConsts.PoolId, linkLocation => linkLocation.PoolCommCount),
                new Amenity(AmenitiesConsts.QmiId, linkLocation => linkLocation.QmiCommCount),
                new Amenity(AmenitiesConsts.SingleFamilyId, linkLocation => linkLocation.SingleFamilyCommCount),
                new Amenity(AmenitiesConsts.SportFacilitiesId, linkLocation => linkLocation.SportCommCount),
                new Amenity(AmenitiesConsts.ViewsId, linkLocation => linkLocation.ViewsCommCount),
                new Amenity(AmenitiesConsts.WaterfrontId, linkLocation => linkLocation.WaterfrontCommCount),
            });

            AmenityConfiguration.TryAdd("AmenityReplaceCodesHomes", new List<Amenity>
            {
                new Amenity(AmenitiesConsts.AdultId, linkLocation => linkLocation.AdultHomeCount),
                new Amenity(AmenitiesConsts.AgeRestrictedId, linkLocation => linkLocation.AgeRestrictedHomeCount),
                new Amenity(AmenitiesConsts.FloorPlansId, linkLocation => linkLocation.FloorPlansHomeCount),
                new Amenity(AmenitiesConsts.GatedId, linkLocation => linkLocation.GatedHomeCount),
                new Amenity(AmenitiesConsts.GolfCourseId, linkLocation => linkLocation.GolfHomeCount),
                new Amenity(AmenitiesConsts.GreenProgramId, linkLocation => linkLocation.GreenHomeCount),
                new Amenity(AmenitiesConsts.HotDealsId, linkLocation => linkLocation.HotDealsHomeCount),
                new Amenity(AmenitiesConsts.LuxuryId, linkLocation => linkLocation.LuxuryHomeCount),
                new Amenity(AmenitiesConsts.ModelHomeId, linkLocation => linkLocation.ModelHomeCount),
                new Amenity(AmenitiesConsts.NatureAreaId, linkLocation => linkLocation.NatureHomeCount),
                new Amenity(AmenitiesConsts.ParkId, linkLocation => linkLocation.ParksHomeCount),
                new Amenity(AmenitiesConsts.PoolId, linkLocation => linkLocation.PoolHomeCount),
                new Amenity(AmenitiesConsts.QmiId, linkLocation => linkLocation.QmiHomeCount),
                new Amenity(AmenitiesConsts.SingleFamilyId, linkLocation => linkLocation.SingleFamilyHomeCount),
                new Amenity(AmenitiesConsts.SportFacilitiesId, linkLocation => linkLocation.SportHomeCount),
                new Amenity(AmenitiesConsts.ViewsId, linkLocation => linkLocation.ViewsHomeCount),
                new Amenity(AmenitiesConsts.WaterfrontId, linkLocation => linkLocation.WaterfrontHomeCount),
            });

            AmenityConfiguration.TryAdd("AmenityReplaceCodesDevelop", new List<Amenity>
            {
                new Amenity(AmenitiesConsts.AdultId, linkLocation => linkLocation.AdultCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.AgeRestrictedId, linkLocation => linkLocation.AgeRestrictedCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.GatedId, linkLocation => linkLocation.GatedCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.GolfCourseId, linkLocation => linkLocation.GolfCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.GreenProgramId, linkLocation => linkLocation.GreenCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.HotDealsId, linkLocation => linkLocation.HotDealsCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.LuxuryId, linkLocation => linkLocation.LuxuryCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.NatureAreaId, linkLocation => linkLocation.NatureCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.ParkId, linkLocation => linkLocation.ParksCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.PoolId, linkLocation => linkLocation.PoolCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.QmiId, linkLocation => linkLocation.QmiCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.SportFacilitiesId, linkLocation => linkLocation.SportCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.ViewsId, linkLocation => linkLocation.ViewsCondoDevelopmentCount),
                new Amenity(AmenitiesConsts.WaterfrontId, linkLocation => linkLocation.WaterfrontCondoDevelopmentCount),
            });

            AmenityConfiguration.TryAdd("AmenityReplaceCodesTownHomes", new List<Amenity>
            {
                new Amenity(AmenitiesConsts.AdultId, linkLocation => linkLocation.AdultCondoTownHomeCount),
                new Amenity(AmenitiesConsts.AgeRestrictedId, linkLocation => linkLocation.AgeRestrictedCondoTownHomeCount),
                new Amenity(AmenitiesConsts.FloorPlansId, linkLocation => linkLocation.FloorPlansCondoTownHomeCount),
                new Amenity(AmenitiesConsts.GatedId, linkLocation => linkLocation.GatedCondoTownHomeCount),
                new Amenity(AmenitiesConsts.GolfCourseId, linkLocation => linkLocation.GolfCondoTownHomeCount),
                new Amenity(AmenitiesConsts.GreenProgramId, linkLocation => linkLocation.GreenCondoTownHomeCount),
                new Amenity(AmenitiesConsts.HotDealsId, linkLocation => linkLocation.HotDealsCondoTownHomeCount),
                new Amenity(AmenitiesConsts.LuxuryId, linkLocation => linkLocation.LuxuryCondoTownHomeCount),
                new Amenity(AmenitiesConsts.ModelHomeId, linkLocation => linkLocation.ModelCondoTownHomeCount),
                new Amenity(AmenitiesConsts.NatureAreaId, linkLocation => linkLocation.NatureCondoTownHomeCount),
                new Amenity(AmenitiesConsts.ParkId, linkLocation => linkLocation.ParksCondoTownHomeCount),
                new Amenity(AmenitiesConsts.PoolId, linkLocation => linkLocation.PoolCondoTownHomeCount),
                new Amenity(AmenitiesConsts.QmiId, linkLocation => linkLocation.QmiCondoTownHomeCount),
                new Amenity(AmenitiesConsts.SportFacilitiesId, linkLocation => linkLocation.SportCondoTownHomeCount),
                new Amenity(AmenitiesConsts.ViewsId, linkLocation => linkLocation.ViewsCondoTownHomeCount),
                new Amenity(AmenitiesConsts.WaterfrontId, linkLocation => linkLocation.WaterfrontCondoTownHomeCount),
            });
        }

        /// <summary>   Gets the amenity lists in this collection. </summary>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the amenity lists in this collection.
        /// </returns>
        public static IEnumerable<Amenity> GetAmenityList(string key)
        {
            var hasValue = AmenityConfiguration.TryGetValue(key, out var amenityList);

            return hasValue
                ? amenityList
                : new List<Amenity>();
        }
    }
}
