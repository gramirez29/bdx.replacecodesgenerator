﻿using System;
using System.Collections.Generic;
using System.Text;
using Bdx.Common.Web;
using Bdx.Common.Web.Common;
using Bdx.Web.Api.Objects.Constants;
using BDX.ReplaceCodesGenerator.Common.Enums;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;

namespace BDX.ReplaceCodesGenerator.Data.Service
{
    public class BdxWebApiMessaging
    {
        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

        public BdxWebApiMessaging()
        {
            _parameters.Add("client", "BDXStaticContentGenerator");
            _parameters.Add("sessiontoken", GenerateSessionTokenV2());
            _parameters.Add("algorithm", BhiConfiguration.WebServicesApiHashingAlgorithm);
        }

        public BdxWebApiMessaging(int partnerId) : this()
        {
            if (!_parameters.ContainsKey(ApiUrlConstV2.PartnerId))
                _parameters.Add(ApiUrlConstV2.PartnerId, partnerId);
        }

        public BdxWebApiMessaging(Dictionary<string, object> parameters)
            : this()
        {
            foreach (var p in parameters)
                _parameters.Add(p.Key, p.Value);
        }

        public void AddParameter(string key, object value)
        {
            if (!_parameters.ContainsKey(key))
                _parameters.Add(key, value);
        }

        public T GetDataFromGraphQl<T>(string page, string query, int partnerId) where T : new()
        {
            var newObject = new T();

            try
            {
                var url = GetGraphQlUrl(page, partnerId);
                var jsonResult = HTTP.HttpJsonPost(url, query);
                var model = jsonResult.ToFromJson<T>();

                if (model != null)
                    newObject = model;
            }
            catch (FormatException ex)
            {
                ex.LogError();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return newObject;
        }

        private string GetGraphQlUrl(string page, int partnerId)
        {
            var searchUrl = new StringBuilder();
            searchUrl.Append(BhiConfiguration.WebServicesApiUrl);
            searchUrl.AppendFormat($"{WebApiVersions.V3}/{page}?partnerId={partnerId}&sessionToken={GenerateSessionTokenV3()}");
            return searchUrl.ToString();
        }

        public T GetData<T>(string page) where T : new()
        {
            var newObj = new T();
            try
            {
                var url = Url(page);

                var jsonResult = HTTP.HttpGet(url);
                var model = jsonResult.ToFromJson<T>();

                if (model != null)
                    newObj = model;
            }
            catch (FormatException ex)
            {
                ex.LogError();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return newObj;
        }

        public string Url(string page)
        {
            var searchUrl = new StringBuilder();

            searchUrl.Append(BhiConfiguration.WebServicesApiUrl);

            searchUrl.AppendFormat(WebApiVersions.V2 + "/search/{0}?", page);

            foreach (var parameter in _parameters)
                searchUrl.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);

            return searchUrl.ToString().TrimEnd('&');
        }

        private static string GenerateSessionTokenV3()
        {
            return WebApiSecurity.CreateToken(DateTime.UtcNow, BhiConfiguration.WebServicesApiHashingAlgorithm, BhiConfiguration.PartnerSitePassword); ; ;
        }

        private static string GenerateSessionTokenV2()
        {
            return BhiConfiguration.DefaultSessionToken;
        }
        
    }
}
