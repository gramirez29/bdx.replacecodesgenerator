﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Bdx.Ratings.Client.Api;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Data.Abstract;
using BDX.ReplaceCodesGenerator.Data.Extensions;
using SearchResultPage = BDX.ReplaceCodesGenerator.Data.Entities.SearchResultPage;

namespace BDX.ReplaceCodesGenerator.Data.Concrete
{
    public class EsWebDatabase : WebFactory
    {
        public EsWebDatabase(IRatingsApiClient ratingsApi) : base(ratingsApi)
        {
            DatabaseConnection = new SqlConnection(BhiConfiguration.ConnectionStringWeb);
        }

        public override List<SearchResultPage> SearchResultPagesForReplaceCodes()
        {
            return new List<SearchResultPage>
            {
                new SearchResultPage(SearchResultPageConsts.Comunidades, linkLocation => linkLocation.CommunityCount, brandLocation => brandLocation.Community_Count),
                new SearchResultPage(SearchResultPageConsts.Casas, linkLocation => linkLocation.HomeCount, brandLocation => brandLocation.Home_Count),
                new SearchResultPage(SearchResultPageConsts.Condominios, linkLocation => linkLocation.CondoCommunityCount, brandLocation => brandLocation.Home_Count),
                new SearchResultPage(SearchResultPageConsts.Complejos, linkLocation => linkLocation.CondoHomeCount, brandLocation => brandLocation.Home_Count),
            };
        }

        public override List<SearchResultPage> SearchResultPagesForFilteredReplaceCodes()
        {
            return new List<SearchResultPage>
            {
                new SearchResultPage(SearchResultPageConsts.Comunidades, linkLocation => linkLocation.CommunityCount, brandLocation => brandLocation.Community_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesComms")),
                new SearchResultPage(SearchResultPageConsts.Casas, linkLocation => linkLocation.HomeCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesHomes")),
                new SearchResultPage(SearchResultPageConsts.Condominios, linkLocation => linkLocation.CondoCommunityCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesTownHomes")),
                new SearchResultPage(SearchResultPageConsts.Complejos, linkLocation => linkLocation.CondoHomeCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesDevelop")),
            };
        }

    }
}