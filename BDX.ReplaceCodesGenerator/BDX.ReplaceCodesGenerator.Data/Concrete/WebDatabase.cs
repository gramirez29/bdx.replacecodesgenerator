﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Bdx.Ratings.Client.Api;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Data.Abstract;
using BDX.ReplaceCodesGenerator.Data.Entities;
using BDX.ReplaceCodesGenerator.Data.Extensions;

namespace BDX.ReplaceCodesGenerator.Data.Concrete
{
    public class WebDatabase : WebFactory
    {
        public WebDatabase(IRatingsApiClient ratingsApi) : base(ratingsApi)
        {
            DatabaseConnection = new SqlConnection(BhiConfiguration.ConnectionStringWeb);
        }

        public override List<SearchResultPage> SearchResultPagesForReplaceCodes()
        {
            return new List<SearchResultPage>
            {
                new SearchResultPage(SearchResultPageConsts.Communities, linkLocation => linkLocation.CommunityCount, brandLocation => brandLocation.Community_Count),
                new SearchResultPage(SearchResultPageConsts.Homes, linkLocation => linkLocation.HomeCount, brandLocation => brandLocation.Home_Count),
                new SearchResultPage(SearchResultPageConsts.CondosTownHomes, linkLocation => linkLocation.CondoCommunityCount, brandLocation => brandLocation.Home_Count),
                new SearchResultPage(SearchResultPageConsts.CondoDevelopments, linkLocation => linkLocation.CondoHomeCount, brandLocation => brandLocation.Home_Count),
            };
        }

        public override List<SearchResultPage> SearchResultPagesForFilteredReplaceCodes()
        {
            return new List<SearchResultPage>
            {
                new SearchResultPage(SearchResultPageConsts.Communities, linkLocation => linkLocation.CommunityCount, brandLocation => brandLocation.Community_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesComms")),
                new SearchResultPage(SearchResultPageConsts.Homes, linkLocation => linkLocation.HomeCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesHomes")),
                new SearchResultPage(SearchResultPageConsts.CondosTownHomes, linkLocation => linkLocation.CondoCommunityCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesTownHomes")),
                new SearchResultPage(SearchResultPageConsts.CondoDevelopments, linkLocation => linkLocation.CondoHomeCount, brandLocation => brandLocation.Home_Count, WebFactoryExtensions.GetAmenityList("AmenityReplaceCodesDevelop")),
            };
        }
    }
}
