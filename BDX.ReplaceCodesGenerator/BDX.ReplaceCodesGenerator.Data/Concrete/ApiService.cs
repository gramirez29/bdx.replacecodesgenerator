﻿using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Helpers;
using BDX.ReplaceCodesGenerator.Data.Entities.GraphQL;
using BDX.ReplaceCodesGenerator.Data.Service;
using System.Collections.Generic;
using System.Linq;
using Bdx.ReplaceCodesGenerator.Model.Web;
using Bdx.ReplaceCodesGenerator.Model.Web.ApiV3;
using Bdx.ReplaceCodesGenerator.Model.Xml.Concrete;

namespace BDX.ReplaceCodesGenerator.Data.Concrete
{
    public class ApiService
    {
        public static Dictionary<string, WebApiV3SearchParams> SyntheticCounts { get; private set; }

        public static SearchAllCommsCounts GetGraphQlSyntheticGeoContent<T>(KeyValuePair<SearchLocation, T> currentMarket, int partnerId, Synthetic currentSynthetic) where T : new()
        {
            if (currentMarket.Key == null) return new SearchAllCommsCounts();

            SyntheticCounts = new Dictionary<string, WebApiV3SearchParams>
            {
                {
                    WebApiMethodsConsts.SearchAllCommsCounts,
                    new WebApiV3SearchParams
                    {
                        PartnerId = partnerId,
                        State = currentMarket.Key.State,
                        AlphaResults = true,
                        NoBoyl = true
                    }
                }
            };

            if (SyntheticCounts.ContainsKey(WebApiMethodsConsts.SearchAllCommsCounts))
            {
                SyntheticCounts[WebApiMethodsConsts.SearchAllCommsCounts] = GraphQlApiUrlLocationType(currentSynthetic, SyntheticCounts.Values.FirstOrDefault());
            }

            var syntheticCountsProcessed = SyntheticCounts;
            var query = GraphQlApiHelper.CreateGraphQlQuery(syntheticCountsProcessed);
            var api = new BdxWebApiMessaging();
            var webApiResultModel = api.GetDataFromGraphQl<ApiResultModel<GraphQlApiResult>>(WebApiMethodsConsts.GraphQl, query, partnerId);

            return webApiResultModel.Result == null ? null : IsValidSyntheticCount(webApiResultModel.Result.SearchAllCommsCounts, partnerId, currentSynthetic);
        }

        private static WebApiV3SearchParams GraphQlApiUrlLocationType(Synthetic currentSyntheticLocation, WebApiV3SearchParams v3SearchParams)
        {
            var locationTypeLowerCase = char.ToUpper(currentSyntheticLocation.LocationTypeName[0]) + currentSyntheticLocation.LocationTypeName.Substring(1);

            switch (locationTypeLowerCase.ToLocationTypeValue())
            {
                // market
                case 1:
                    v3SearchParams.MarketIds = currentSyntheticLocation.Locations;
                    break;
                // City
                case 2:
                    v3SearchParams.Cities = currentSyntheticLocation.Locations;
                    break;
                // Postal Codes
                case 4:
                    v3SearchParams.PostalCodes = currentSyntheticLocation.Locations;
                    break;
            }

            return v3SearchParams;
        }

        private static string ToApiUrlLocationType(Synthetic currentSynthetic)
        {
            switch (currentSynthetic.LocationTypeName.ToLocationTypeValue())
            {
                case 1:
                    // Market
                    return ApiUrlConstV2.Markets;
                case 2:
                    // City
                    return ApiUrlConstV2.Cities;
                case 3:
                    // County
                    return ApiUrlConstV2.Counties;
                case 4:
                    // Postal Codes/Zip
                    return ApiUrlConstV2.Postalcodes;
                default:
                    return ApiUrlConstV2.Markets;
            }
        }

        private static SearchAllCommsCounts IsValidSyntheticCount(SearchAllCommsCounts searchResult, int partnerId, Synthetic synthetic)
        {
            var isValid = (searchResult.CommCount != 0 || searchResult.HomeCount != 0) && searchResult.AveragePrice != 0;

            if (isValid)
                return searchResult;

            searchResult.LogInvalidsCounters(partnerId, synthetic);

            return null;
        }
    }
}

