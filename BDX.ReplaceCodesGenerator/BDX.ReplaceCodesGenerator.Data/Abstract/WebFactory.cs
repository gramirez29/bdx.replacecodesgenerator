﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;
using Bdx.Ratings.Client.Api;
using Bdx.Ratings.Models;
using BDX.ReplaceCodesGenerator.Common.Constants;
using BDX.ReplaceCodesGenerator.Common.Extensions;
using BDX.ReplaceCodesGenerator.Common.Util;
using BDX.ReplaceCodesGenerator.Data.Entities;
using Bdx.ReplaceCodesGenerator.Model.Web;
using SearchResultPage = BDX.ReplaceCodesGenerator.Data.Entities.SearchResultPage;

namespace BDX.ReplaceCodesGenerator.Data.Abstract
{
    public abstract class WebFactory : SqlFactory
    {
        private Stopwatch _locationWatcher;
        private Stopwatch _brandByMarketWatcher;
        private Stopwatch _footerLinkWatcher;
        private Stopwatch _brandsWatcher;
        private Stopwatch _newestCommWatcher;
        private Stopwatch _brandBuilderWatcher;
        private Stopwatch _marketSummaryWatcher;

        private readonly IRatingsApiClient _ratingsAndReviewsApi;
        private MultiValueDictionary<SearchLocation, FooterLinkLocation> _footerLinks;
        private MultiValueDictionary<SearchLocation, FooterLinkLocation> _schoolDistricts;
        private MultiValueDictionary<SearchLocation, BrandLocation> _brandLocations;
        private MultiValueDictionary<SearchLocation, NewestCommunities> _newestCommunities;

        protected WebFactory(IRatingsApiClient ratingsAndReviewsApi)
        {
            _ratingsAndReviewsApi = ratingsAndReviewsApi;
        }

        public HashSet<FooterLinkLocation> FooterLinkLocations { get; private set; }

        public HashSet<BrandLocation> BrandLocations { get; private set; }

        public HashSet<FooterLinkLocation> SchoolDistricts { get; private set; }

        public Dictionary<int, MarketSummary> Markets { get; private set; }

        public HashSet<NewestCommunities> NewestCommunities { get; private set; }

        public abstract List<SearchResultPage> SearchResultPagesForReplaceCodes();

        public abstract List<SearchResultPage> SearchResultPagesForFilteredReplaceCodes();

        public async Task<IEnumerable<SearchLocation>> GetLocationsAsync(int partnerId)
        {
            _locationWatcher = new Stopwatch();
            _locationWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            await connection.OpenAsync();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetPartnerLocationsIndex", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("PartnerId", partnerId, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();

                _locationWatcher.Stop();
                BdxWatcher.LogWatcher($"Finished Method: GetLocationsAsync with {objDataSet.Tables[0].Rows.Count} processed rows. ", partnerId, _locationWatcher.LogTime());

                var partnerLocations = objDataSet.Tables[0] != null
                    ? objDataSet.Tables[0].ToList<SearchLocation>()
                    : new List<SearchLocation>();

                return partnerLocations;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new List<SearchLocation>();
        }

        public IEnumerable<BrandInformation> GetBrandByMarket(int partnerId)
        {
            _brandByMarketWatcher = new Stopwatch();
            _brandByMarketWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            connection.Open();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetBrandByMarket", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("PartnerId", partnerId, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();

                _brandByMarketWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetBrandByMarket {objDataSet.Tables[0].Rows.Count} processed rows. ", partnerId, _brandByMarketWatcher.LogTime());

                return objDataSet.Tables[0] != null ? objDataSet.Tables[0].ToList<BrandInformation>() : new List<BrandInformation>();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new List<BrandInformation>();
        }

        public async Task<MultiValueDictionary<SearchLocation, FooterLinkLocation>> GetFooterLinkLocationsAsync(int partnerId, int cityRadius, int postalCodeRadius)
        {
            _footerLinkWatcher = new Stopwatch();
            _footerLinkWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            await connection.OpenAsync();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetFooterLinkLocations", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PartnerID", partnerId, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@CityRadius", cityRadius, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PostalCodeRadius", postalCodeRadius, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();


                IEnumerable<FooterLinkLocation> data = objDataSet.Tables[0].ToList<FooterLinkLocation>();
                SchoolDistricts = new HashSet<FooterLinkLocation>();
                FooterLinkLocations = new HashSet<FooterLinkLocation>();
                _footerLinks = new MultiValueDictionary<SearchLocation, FooterLinkLocation>();
                _schoolDistricts = new MultiValueDictionary<SearchLocation, FooterLinkLocation>();

                foreach (var item in data)
                {
                    IsValidLocation(item, partnerId);
                }

                _footerLinkWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetFooterLinkLocationsAsync {_footerLinks.Count} processed rows. ", partnerId, _footerLinkWatcher.LogTime());

                return _footerLinks;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new MultiValueDictionary<SearchLocation, FooterLinkLocation>();
        }

        public async Task<MultiValueDictionary<SearchLocation, BrandLocation>> GetBrandsInformationAsync(int partnerId, int radius)
        {
            _brandsWatcher = new Stopwatch();
            _brandsWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            await connection.OpenAsync();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetBrandByMarketCountyCityZip", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PartnerID", partnerId, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@Radius", radius, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();


                IEnumerable<BrandLocation> data = objDataSet.Tables[0].ToList<BrandLocation>();
                BrandLocations = new HashSet<BrandLocation>();
                _brandLocations = new MultiValueDictionary<SearchLocation, BrandLocation>();

                foreach (var item in data)
                {
                    IsValidBrand(item, partnerId);
                }

                _brandsWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetBrandsInformationAsync {_brandLocations.Count} processed rows. ", partnerId, _brandsWatcher.LogTime());

                return _brandLocations;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new MultiValueDictionary<SearchLocation, BrandLocation>();
        }

        public async Task<MultiValueDictionary<SearchLocation, NewestCommunities>> GetNewestCommunitiesInformationAsync(int partnerId, int cityRadius, int postalCodeRadius)
        {
            _newestCommWatcher = new Stopwatch();
            _newestCommWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            await connection.OpenAsync();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetNewestCommbyLocations", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PartnerID", partnerId, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@CityRadius", cityRadius, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PostalCodeRadius", postalCodeRadius, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();


                IEnumerable<NewestCommunities> data = objDataSet.Tables[0].ToList<NewestCommunities>();
                NewestCommunities = new HashSet<NewestCommunities>();
                _newestCommunities = new MultiValueDictionary<SearchLocation, NewestCommunities>();

                foreach (var item in data)
                {
                    IsValidCommunity(item, partnerId);
                }

                _newestCommWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetNewestCommunitiesInformationAsync {_newestCommunities.Count} processed rows. ", partnerId, _newestCommWatcher.LogTime());

                return _newestCommunities;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new MultiValueDictionary<SearchLocation, NewestCommunities>();
        }

        public async Task<List<BrandBuilderPerMarket>> GetBrandBuilderPerMarketAsync(int partnerId, int minCountOfReviews = 1)
        {
            _brandBuilderWatcher = new Stopwatch();
            _brandBuilderWatcher.Start();

            try
            {
                var markets = _ratingsAndReviewsApi.GetBrandBuildersPerMarketAsync(minCountOfReviews);

                await markets;

                _brandBuilderWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetBrandBuilderPerMarketAsync {markets.Result.Count} processed rows. ", partnerId, _brandBuilderWatcher.LogTime());

                return markets.Result;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new List<BrandBuilderPerMarket>();
        }

        public async Task<Dictionary<int, MarketSummary>> GetMarketSummaryAsync(int partnerId, int radius)
        {
            _marketSummaryWatcher = new Stopwatch();
            _marketSummaryWatcher.Start();

            var connection = new SqlConnection(DatabaseConnection.ConnectionString);
            await connection.OpenAsync();

            try
            {
                var sqlDataAdapter = new SqlDataAdapter("pr_NHS_GetMarketSummary", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure, CommandTimeout = int.MaxValue }
                };

                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@PartnerID", partnerId, SqlDbType.VarChar));
                sqlDataAdapter.SelectCommand.Parameters.Add(AdapterParameter("@Radius", radius, SqlDbType.VarChar));

                var objDataSet = ExecuteDataset(ref sqlDataAdapter) ?? new DataSet();
                connection.Close();

                IEnumerable<MarketSummary> data = objDataSet.Tables[0].ToList<MarketSummary>();
                Markets = new Dictionary<int, MarketSummary>();

                foreach (var item in data)
                    IsValidMarket(item, partnerId);

                _marketSummaryWatcher.Stop();
                BdxWatcher.LogWatcher($"Method: GetMarketSummaryAsync {Markets.Count} processed rows. ", partnerId, _marketSummaryWatcher.LogTime());

                return Markets;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return new Dictionary<int, MarketSummary>();
        }

        public MultiValueDictionary<SearchLocation, FooterLinkLocation> GetSchoolDistricts()
        {
            return _schoolDistricts;
        }

        private void IsValidLocation(FooterLinkLocation location, int partnerId)
        {
            var key = new SearchLocation
            {
                MarketId = location.MarketId,
                Type = location.LocationType.ToLocationTypeValue(),
                Location = string.Equals(location.LocationType, LocationTypes.Market) ? location.MarketId.ToString()
                    : string.Equals(location.LocationType, LocationTypes.County) ? location.Location.Trim() + $" {LocationTypes.County}"
                        : location.Location.Trim()
            };

            if (location.DistrictId <= 0)
            {
                var isValid = location.BoylCount > 0 || location.CustomCount > 0 || location.CommunityCount > 0 ||
                              location.HomeCount > 0;

                if (!isValid)
                {
                    location.LogInvalidLocation(partnerId);
                    return;
                }

                FooterLinkLocations.Add(location);
                _footerLinks.Add(key, location);
            }
            else
            {
                SchoolDistricts.Add(location);
                _schoolDistricts.Add(key, location);
            }
        }

        private void IsValidBrand(BrandLocation brand, int partnerId)
        {
            var isValid = brand.Community_Count > 0 || brand.Boyl_Count > 0 || brand.Home_Count > 0 || brand.Custom_Builder_Flag_Count > 0 || brand.MfrHomes_Count > 0;

            if (!isValid)
            {
                brand.LogInvalidLocation(partnerId);
                return;
            }

            var key = new SearchLocation
                {
                    MarketId = brand.Market_Id,
                    Type = brand.Location.ToLocationTypeValue(),
                    Location = brand.GetLocationNameAsFooterLink()
                };

            BrandLocations.Add(brand);
            _brandLocations.Add(key, brand);
        }

        private void IsValidMarket(MarketSummary market, int partnerId)
        {
            var isValid = market.Community_Count > 0 || market.Builder_Count > 0;

            if (!isValid)
            {
                market.LogInvalidLocation(partnerId);
                return;
            }

            Markets.Add(market.Market_Id, market);
        }

        private void IsValidCommunity(NewestCommunities community, int partnerId)
        {
            var isValid = community.HomeCount > 0;

            if (!isValid)
            {
               community.LogInvalidLocation(partnerId);
               return;
            }

            var key = new SearchLocation
            {
                MarketId = community.MarketId,
                Type = community.LocationType.ToLocationTypeValue(),
                Location = community.Location
            };

            NewestCommunities.Add(community);
            _newestCommunities.Add(key, community);
        }
    }
}