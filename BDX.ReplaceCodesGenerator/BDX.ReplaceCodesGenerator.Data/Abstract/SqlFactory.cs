﻿using System;
using System.Data;
using System.Data.SqlClient;
using BDX.ReplaceCodesGenerator.Common.Extensions;

namespace BDX.ReplaceCodesGenerator.Data.Abstract
{
    public abstract class SqlFactory : IDisposable
    {
        public SqlConnection DatabaseConnection { get; internal set; }

        public SqlParameter AdapterParameter(string parameterName, object value, SqlDbType sqlValue)
        {
            value = value ?? DBNull.Value;
        
            var param = new SqlParameter
            {
                ParameterName = parameterName,
                Value = value,
                SqlValue = value,
                SqlDbType = sqlValue
            };

            return param;
        }

        public void Open()
        {
            try
            {
                if (DatabaseConnection.State != ConnectionState.Open)
                    DatabaseConnection.Open();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }

        public void Close()
        {
            if (DatabaseConnection.State != ConnectionState.Closed)
                DatabaseConnection.Close();
        }

        public DataSet ExecuteDataset(ref SqlDataAdapter sqlDataAdapter)
        {
            var dataset = new DataSet();
            sqlDataAdapter.Fill(dataset);
            return dataset;
        }

        public void Dispose()
        {
            if (DatabaseConnection != null)
                Close();
        }
    }
}
