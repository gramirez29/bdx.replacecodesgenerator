﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public class BrandLocation
    {
        public string Location { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Market_Id { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Postal_Code { get; set; }

        public string County { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Market_Name { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Market_State { get; set; }

        public string StateId { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Brand_Id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Brand_Name { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public decimal Low_Price { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public decimal High_Price { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Community_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Home_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Boyl_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Custom_Builder_Flag_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int MfrHomes_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Custom_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Modular_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public long Avg_Home_Price { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public bool Basic_Flag { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public bool ShowCase_Flag { get; set; }

        #region Amenity Counts

        /// <summary>   Gets or sets the number of age restricted communications. </summary>
        /// <value> The number of age restricted communications. </value>
        public int AgeRestrictedCommCount { get; set; }

        /// <summary>   Gets or sets the number of green communities. </summary>
        /// <value> The number of green communities. </value>
        public int GreenCommCount { get; set; }

        /// <summary>   Gets or sets the number of pool communities. </summary>
        /// <value> The number of pool communities. </value>
        public int PoolCommCount { get; set; }

        /// <summary>   Gets or sets the number of golf communities. </summary>
        /// <value> The number of golf communities. </value>
        public int GolfCommCount { get; set; }

        /// <summary>   Gets or sets the number of gated communities. </summary>
        /// <value> The number of gated communities. </value>
        public int GatedCommCount { get; set; }

        /// <summary>   Gets or sets the number of parks communities. </summary>
        /// <value> The number of parks communities. </value>
        public int ParksCommCount { get; set; }

        /// <summary>   Gets or sets the number of nature communications. </summary>
        /// <value> The number of nature communications. </value>
        public int NatureCommCount { get; set; }

        /// <summary>   Gets or sets the number of views communities. </summary>
        /// <value> The number of views communities. </value>
        public int ViewsCommCount { get; set; }

        /// <summary>   Gets or sets the number of waterfront communities. </summary>
        /// <value> The number of waterfront communities. </value>
        public int WaterfrontCommCount { get; set; }

        /// <summary>   Gets or sets the number of sport communities. </summary>
        /// <value> The number of sport communities. </value>
        public int SportCommCount { get; set; }

        /// <summary>   Gets or sets the number of adult communications. </summary>
        /// <value> The number of adult communications. </value>
        public int AdultCommCount { get; set; }

        /// <summary>   Gets or sets the number of luxury communities. </summary>
        /// <value> The number of luxury communities. </value>
        public int LuxuryCommCount { get; set; }

        /// <summary>   Gets or sets the number of qmi communities. </summary>
        /// <value> The number of qmi communities. </value>
        public int QmiCommCount { get; set; }

        /// <summary>   Gets or sets the number of hot deals communities. </summary>
        /// <value> The number of hot deals communities. </value>
        public int HotDealsCommCount { get; set; }

        /// <summary>   Gets or sets the number of single family communications. </summary>
        /// <value> The number of single family communications. </value>
        public int SingleFamilyCommCount { get; set; }

        /// <summary>   Gets or sets the number of age restricted homes. </summary>
        /// <value> The number of age restricted homes. </value>
        public int AgeRestrictedHomeCount { get; set; }

        /// <summary>   Gets or sets the number of green homes. </summary>
        /// <value> The number of green homes. </value>
        public int GreenHomeCount { get; set; }

        /// <summary>   Gets or sets the number of pool homes. </summary>
        /// <value> The number of pool homes. </value>
        public int PoolHomeCount { get; set; }

        /// <summary>   Gets or sets the number of golf homes. </summary>
        /// <value> The number of golf homes. </value>
        public int GolfHomeCount { get; set; }

        /// <summary>   Gets or sets the number of gated homes. </summary>
        /// <value> The number of gated homes. </value>
        public int GatedHomeCount { get; set; }

        /// <summary>   Gets or sets the number of parks homes. </summary>
        /// <value> The number of parks homes. </value>
        public int ParksHomeCount { get; set; }

        /// <summary>   Gets or sets the number of nature homes. </summary>
        /// <value> The number of nature homes. </value>
        public int NatureHomeCount { get; set; }

        /// <summary>   Gets or sets the number of views homes. </summary>
        /// <value> The number of views homes. </value>
        public int ViewsHomeCount { get; set; }

        /// <summary>   Gets or sets the number of waterfront homes. </summary>
        /// <value> The number of waterfront homes. </value>
        public int WaterfrontHomeCount { get; set; }

        /// <summary>   Gets or sets the number of sport homes. </summary>
        /// <value> The number of sport homes. </value>
        public int SportHomeCount { get; set; }

        /// <summary>   Gets or sets the number of adult homes. </summary>
        /// <value> The number of adult homes. </value>
        public int AdultHomeCount { get; set; }

        /// <summary>   Gets or sets the number of luxury homes. </summary>
        /// <value> The number of luxury homes. </value>
        public int LuxuryHomeCount { get; set; }

        /// <summary>   Gets or sets the number of qmi homes. </summary>
        /// <value> The number of qmi homes. </value>
        public int QmiHomeCount { get; set; }

        /// <summary>   Gets or sets the number of single family homes. </summary>
        /// <value> The number of single family homes. </value>
        public int SingleFamilyHomeCount { get; set; }

        /// <summary>   Gets or sets the number of model homes. </summary>
        /// <value> The number of model homes. </value>
        public int ModelHomeCount { get; set; }

        /// <summary>   Gets or sets the number of floorplans homes. </summary>
        /// <value> The number of floorplans homes. </value>
        public int FloorPlansHomeCount { get; set; }

        /// <summary>   Gets or sets the number of hot deals homes. </summary>
        /// <value> The number of hot deals homes. </value>
        public int HotDealsHomeCount { get; set; }

        /// <summary>   Gets or sets the number of age restricted condo town homes. </summary>
        /// <value> The number of age restricted condo town homes. </value>
        public int AgeRestrictedCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of green condo town homes. </summary>
        /// <value> The number of green condo town homes. </value>
        public int GreenCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of pool condo town homes. </summary>
        /// <value> The number of pool condo town homes. </value>
        public int PoolCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of golf condo town homes. </summary>
        /// <value> The number of golf condo town homes. </value>
        public int GolfCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of gated condo town homes. </summary>
        /// <value> The number of gated condo town homes. </value>
        public int GatedCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of parks condo town homes. </summary>
        /// <value> The number of parks condo town homes. </value>
        public int ParksCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of nature condo town homes. </summary>
        /// <value> The number of nature condo town homes. </value>
        public int NatureCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of views condo town homes. </summary>
        /// <value> The number of views condo town homes. </value>
        public int ViewsCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of waterfront condo town homes. </summary>
        /// <value> The number of waterfront condo town homes. </value>
        public int WaterfrontCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of sport condo town homes. </summary>
        /// <value> The number of sport condo town homes. </value>
        public int SportCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of adult condo town homes. </summary>
        /// <value> The number of adult condo town homes. </value>
        public int AdultCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of luxury condo town homes. </summary>
        /// <value> The number of luxury condo town homes. </value>
        public int LuxuryCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of qmi condo town homes. </summary>
        /// <value> The number of qmi condo town homes. </value>
        public int QmiCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of hot deals condo town homes. </summary>
        /// <value> The number of hot deals condo town homes. </value>
        public int HotDealsCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of floor plans condo town homes. </summary>
        /// <value> The number of floor plans condo town homes. </value>
        public int FloorPlansCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of model condo town homes. </summary>
        /// <value> The number of model condo town homes. </value>
        public int ModelCondoTownHomeCount { get; set; }

        /// <summary>   Gets or sets the number of age restricted condo developments. </summary>
        /// <value> The number of age restricted condo developments. </value>
        public int AgeRestrictedCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the green condo development counts. </summary>
        /// <value> The green condo development counts. </value>
        public int GreenCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the pool condo development counts. </summary>
        /// <value> The pool condo development counts. </value>
        public int PoolCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the golf condo development counts. </summary>
        /// <value> The golf condo development counts. </value>
        public int GolfCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the gated condo development counts. </summary>
        /// <value> The gated condo development counts. </value>
        public int GatedCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the parks condo development counts. </summary>
        /// <value> The parks condo development counts. </value>
        public int ParksCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the nature condo development counts. </summary>
        /// <value> The nature condo development counts. </value>
        public int NatureCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the views condo development counts. </summary>
        /// <value> The views condo development counts. </value>
        public int ViewsCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the waterfront condo development counts. </summary>
        /// <value> The waterfront condo development counts. </value>
        public int WaterfrontCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the sport condo development counts. </summary>
        /// <value> The sport condo development counts. </value>
        public int SportCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the adult condo development counts. </summary>
        /// <value> The adult condo development counts. </value>
        public int AdultCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the luxury condo development counts. </summary>
        /// <value> The luxury condo development counts. </value>
        public int LuxuryCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the qmi condo development counts. </summary>
        /// <value> The qmi condo development counts. </value>
        public int QmiCondoDevelopmentCount { get; set; }

        /// <summary>   Gets or sets the hot deals condo development counts. </summary>
        /// <value> The hot deals condo development counts. </value>
        public int HotDealsCondoDevelopmentCount { get; set; }

        #endregion

        public override string ToString()
        {
            string locationName;

            switch (Location.Trim())
            {
                case "City":
                    locationName = City;
                    break;
                case "PostalCode":
                    locationName = !string.IsNullOrEmpty(Postal_Code) ? Postal_Code : string.Empty;
                    break;
                case "County":
                    locationName = County;
                    break;
                case "Market":
                    locationName = Market_Name;
                    break;
                default:
                    locationName = string.Empty;
                    break;
            }

            return string.Format("Location {0}; LocationType {1}; MarketId {2}; Brand {3}", locationName, Location, Market_Id, Brand_Name);
        }
    }
}