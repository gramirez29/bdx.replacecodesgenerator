﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public sealed class SearchLocation
    {
        public SearchLocation()
        {
            Location = string.Empty;
        }

        public SearchLocation(string location, int marketId, string marketName, string stateAbbreviation, int type, string marketStateId)
        {
            MarketId = marketId;
            Market = marketName;
            State = stateAbbreviation;
            Type = type;
            Location = location;
            MarketStateId = marketStateId;
        }

        public int MarketId { get; set; }

        public string Market { get; set; }

        public string Location { get; set; }

        public string State { get; set; }

        /// <summary>Gets or sets the identifier of the market state.</summary>
        public string MarketStateId { get; set; }

        public int Id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Builder_Id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Builder_Name { get; set; }

        // Type of the location as described below
        // 1 : Market
        // 2 : City
        // 3 : County
        // 4 : Zip
        // 5 : Comm name
        // 6 : Developer Name 
        public int Type { get; set; }

        public override string ToString()
        {
            return "Location: " + Location + " - Type: " + Type + " - Market Id: " + MarketId;
        }

        public override bool Equals(object obj)
        {
            var location = obj as SearchLocation;

            if (location == null)
                return false;

            return Type == 1 ? Type == 1 && MarketId == location.MarketId
                                          : string.Equals(Location.Trim(), location.Location.Trim()) && Type == location.Type && MarketId == location.MarketId;
        }

        public override int GetHashCode()
        {
            Location = Location != null ? Location.Trim() : string.Empty;
            return (Location + Type + MarketId).GetHashCode();
        }
    }
}