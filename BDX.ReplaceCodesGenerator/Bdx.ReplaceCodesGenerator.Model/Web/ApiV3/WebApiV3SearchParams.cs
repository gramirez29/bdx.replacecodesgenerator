﻿using System;

namespace Bdx.ReplaceCodesGenerator.Model.Web.ApiV3
{
    [Serializable]
    public class WebApiV3SearchParams
    {
        public int PartnerId { get; set; }
        public string MarketIds { get; set; }
        public string PostalCodes { get; set; }
        public string State { get; set; }
        public string Cities { get; set; }
        public bool AlphaResults { get; set; }
        public bool NoBoyl { get; set; }
    }
}
