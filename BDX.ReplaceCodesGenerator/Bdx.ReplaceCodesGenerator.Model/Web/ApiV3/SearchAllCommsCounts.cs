﻿using System;

namespace Bdx.ReplaceCodesGenerator.Model.Web.ApiV3
{
    [Serializable]
    public class SearchAllCommsCounts
    {
        public decimal AveragePrice { get; set; }
        public int CommCount { get; set; }
        public int HomeCount { get; set; }
    }
}