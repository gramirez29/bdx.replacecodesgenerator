﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public class NewestCommunities
    {
        public string Location { get; set; }

        public string LocationType { get; set; }

        public int MarketId { get; set; }

        public string MarketName { get; set; }

        public string StateId { get; set; }

        public string MarketState { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public int CommunityId { get; set; }

        public string CommunityName { get; set; }

        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public string ProjectTypeCode { get; set; }

        public decimal LowPrice { get; set; }

        public int HomeCount { get; set; }

        public int ActiveDays { get; set; }

        public bool IsComingSoonCommunity { get; set; }
    }
}