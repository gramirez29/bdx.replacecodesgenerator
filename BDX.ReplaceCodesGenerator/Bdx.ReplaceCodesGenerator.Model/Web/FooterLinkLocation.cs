﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public class FooterLinkLocation
    {
        public FooterLinkLocation() { }

        public FooterLinkLocation(string location, string locationType, int marketId, string marketName, string marketState)
        {
            Location = location;
            LocationType = locationType;
            MarketId = marketId;
            MarketName = marketName;
            MarketState = marketState;
        }

        public string Location { get; set; }

        public string LocationType { get; set; }

        public int MarketId { get; set; }

        public int MarketId2 { get; set; }

        public string MarketName { get; set; }

        public string MarketState { get; set; }

        public string StateName { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string PostalCode { get; set; }

        public int DistrictId { get; set; }

        public string DistrictName { get; set; }

        public int CommunityCount { get; set; }

        public int BoylCount { get; set; }

        public int CustomCount { get; set; }

        public int HomeCount { get; set; }

        public int AdultCommunityCount { get; set; }

        public int AdultBoylCount { get; set; }

        public int AdultCustomCount { get; set; }

        public int AdultHomeCount { get; set; }

        public int AdultMfrHomesCount { get; set; }

        public int AgeRestrictedCommCount { get; set; }

        public int AgeRestrictedBoylCount { get; set; }

        public int AgeRestrictedMfrCount { get; set; }

        public int GatedCommunityCount { get; set; }

        public int GatedBoylCount { get; set; }

        public int GatedCustomCount { get; set; }

        public int GatedHomeCount { get; set; }

        public int GatedMfrHomesCount { get; set; }

        public int GolfCommunityCount { get; set; }

        public int GolfBoylCount { get; set; }

        public int GolfCustomCount { get; set; }

        public int GolfHomeCount { get; set; }

        public int GolfMfrHomesCount { get; set; }

        public int WaterCommunityCount { get; set; }

        public int WaterBoylCount { get; set; }

        public int WaterCustomCount { get; set; }

        public int WaterHomeCount { get; set; }

        public int WaterMfrHomesCount { get; set; }

        public int ComingCommunityCount { get; set; }

        public int ComingBoylCount { get; set; }

        public int ComingCustomCount { get; set; }

        public int ComingHomeCount { get; set; }

        public int ComingMfrHomesCount { get; set; }

        public int CondoCommunityCount { get; set; }

        public int CondoBoylCount { get; set; }

        public int CondoCustomCount { get; set; }

        public int CondoHomeCount { get; set; }

        public int CondoMfrHomesCount { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int CSLCustomCount { get; set; }
        public int PoolCommunityCount { get; set; }

        public int PoolBoylCount { get; set; }

        public int PoolCustomCount { get; set; }

        public int PoolHomeCount { get; set; }

        public int PoolMfrHomesCount { get; set; }

        public int GreenNatureCommunityCount { get; set; }

        public int GreenNatureBoylCount { get; set; }

        public int GreenNatureCustomCount { get; set; }

        public int GreenNatureHomeCount { get; set; }

        public int GreenNatureMfrHomesCount { get; set; }

        public int ViewCommunityCount { get; set; }

        public int ViewBoylCount { get; set; }

        public int ViewCustomCount { get; set; }

        public int ViewHomeCount { get; set; }

        public int ViewMfrHomesCount { get; set; }

        public int NatureCommCount { get; set; }

        public int NatureHomeCount { get; set; }

        public int NatureCustomHomesCount { get; set; }

        public int NatureBoylCount { get; set; }

        public int NatureMfrCount { get; set; }

        public int NatureMfrHomesCount { get; set; }

        public int ParkCommunityCount { get; set; }

        public int ParkBoylCount { get; set; }

        public int ParkCustomCount { get; set; }

        public int ParkHomeCount { get; set; }

        public int ParkMfrHomesCount { get; set; }

        public int SportCommunityCount { get; set; }

        public int SportBoylCount { get; set; }

        public int SportCustomCount { get; set; }

        public int SportHomeCount { get; set; }

        public int SportMfrHomesCount { get; set; }

        public int GreenCommunityCount { get; set; }

        public int GreenBoylCount { get; set; }

        public int GreenCustomCount { get; set; }

        public int GreenHomeCount { get; set; }

        public int GreenMfrHomesCount { get; set; }

        public int MfrHomesCommunityCount { get; set; }

        public int MfrHomesBoylCount { get; set; }

        public int MfrHomesCustomCount { get; set; }

        public int MfrHomesHomeCount { get; set; }

        public int LuxuryCommunityCount { get; set; }

        public int LuxuryBoylCount { get; set; }

        public int LuxuryCustomCount { get; set; }

        public int LuxuryHomeCount { get; set; }

        public int LuxuryMfrHomesCount { get; set; }

        public int AveragePlanPrice { get; set; }

        public int AveragePlanPriceFoot { get; set; }

        public int DistanceFromMarket { get; set; }

        public int QmiCommCount { get; set; }

        public int QmiHomeCount { get; set; }

        public int QmiBoylCount { get; set; }

        public int QmiMfrHomeCount { get; set; }

        public int QmiMfrCount { get; set; }

        public int QmiCustomCount { get; set; }

        public int HotCommCount { get; set; }

        public int HotHomeCount { get; set; }

        public int HotBoylCount { get; set; }

        public int HotMfrHomesCount { get; set; }

        public int HotMfrCount { get; set; }

        public int HotCustomCount { get; set; }

        public int CommunityRegularCount { get; set; }

        public override string ToString()
        {
            return string.Format("Location {0}; LocationType {1}; MarketId {2}; MarketName {7}; State {3}; City {4}; County {5}; PostalCode {6}", Location, LocationType, MarketId, MarketState, City, County, PostalCode, MarketName);
        }
    }
}
