﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public class MarketSummary
    {
        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Market_Id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Market_Name { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string Market_State { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public string State_Abbr { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Builder_Count { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int Community_Count { get; set; }

        /// <summary>   Gets or sets the total number of condo. </summary>
        /// <value> The total number of condo. </value>
        public int CountCondo { get; set; }

        /// <summary>   Gets or sets the total number of waterfront only. </summary>
        /// <value> The total number of waterfront only. </value>
        public int CountWaterfrontOnly { get; set; }

        /// <summary>   Gets or sets the total number of golf. </summary>
        /// <value> The total number of golf. </value>
        public int CountGolf { get; set; }

        /// <summary>   Gets or sets the total number of adult. </summary>
        /// <value> The total number of adult. </value>
        public int CountAdult { get; set; }

        /// <summary>   Gets or sets the total number of luxury. </summary>
        /// <value> The total number of luxury homes. </value>
        public int CountLuxury { get; set; }

        /// <summary>   Gets or sets the total number of gated communications. </summary>
        /// <value> The total number of gated communities. </value>
        public int CountGatedComm { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public decimal Low_Price { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public decimal High_Price { get; set; }

        public override string ToString()
        {
            return $"MarketId {Market_Id}; MarketName {Market_Name}; State {State_Abbr};";
        }
    }
}