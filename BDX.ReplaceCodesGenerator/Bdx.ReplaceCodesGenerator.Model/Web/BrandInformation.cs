﻿namespace Bdx.ReplaceCodesGenerator.Model.Web
{
    public class BrandInformation
    {
        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int market_id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int brand_id { get; set; }

        // ReSharper disable once InconsistentNaming
        // Cannot rename as this is the name returned from the stored procedure, we need to match the stored procedure name to be able to use reflection when parsing the results
        public int home_count { get; set; }
    }
}
