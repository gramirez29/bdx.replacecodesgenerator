﻿using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Abstract
{
    public interface IFileProperties
    {
        [XmlElement(ElementName = "LocationName")]
        string LocationName { get; set; }

        [XmlElement(ElementName = "LocationType")]
        string LocationType { get; set; }

        [XmlElement(ElementName = "NamePath")]
        string NamePath { get; set; }

        [XmlElement(ElementName = "FileName")]
        string FileName { get; set; }

        [XmlElement(ElementName = "IsVisible")]
        bool IsVisible { get; set; }

        [XmlElement(ElementName = "FileTitle")]
        string FileTitle { get; set; }
    }
}
