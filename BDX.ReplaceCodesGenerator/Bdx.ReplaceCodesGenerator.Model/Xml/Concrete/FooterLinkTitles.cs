﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlType(AnonymousType = true)]
    [XmlRoot("sections")]
    public class FooterLinkTitles
    {
        [XmlElement("section")]
        public List<Section> Sections { get; set; }
    }

    [XmlRoot("section")]
    public class Section
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("location")]
        public List<LocationP> Locations { get; set; }
    }

    [XmlRoot("location")]
    public class LocationP
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}
