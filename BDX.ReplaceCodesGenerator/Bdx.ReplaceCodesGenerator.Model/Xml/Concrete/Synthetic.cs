﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlType(AnonymousType = true)]
    [XmlRoot("synthetics")]
    public class SyntheticGeoNames
    {
        public SyntheticGeoNames()
        {
            Synthetics = new List<Synthetic>();
        }

        [XmlElement("synthetic")]
        public List<Synthetic> Synthetics { get; set; }
    }

    [XmlRoot(ElementName = "Synthetic")]
    public class Synthetic : FileProperties
    {
        /// <summary>
        /// Synthetic name, it can have spaces, and should be unique.
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// Primary market name
        /// </summary>
        [XmlAttribute("market")]
        public int MarketId { get; set; }

        /// <summary>
        /// Locations IDs or names separated by semicolon to avoid conflict with Name, State Names (eg: "17;256;286;344" or "cedar park, tx; round rock, tx; miami, fl")
        /// </summary>
        [XmlAttribute("locations")]
        public string Locations { get; set; }

        /// <summary>
        /// Locations type: could be: "market", "city", "county" or "zip"
        /// </summary>
        [XmlAttribute("locationtype")]
        public string LocationTypeName { get; set; }

        /// <summary>
        /// Synthetic Link: Contains the list of Links
        /// </summary>
        [XmlElement(ElementName = "SyntheticLink")]
        public List<Link> SyntheticLink { get; set; }
    }
}