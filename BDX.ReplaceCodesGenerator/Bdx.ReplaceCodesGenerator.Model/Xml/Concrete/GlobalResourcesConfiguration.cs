﻿using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "globalResourcesConfiguration")]
    public class GlobalResourcesConfiguration
    {
        public GlobalResourcesConfiguration()
        {
            AmenityResources = new AmenityResources();
        }

        [XmlElement(ElementName = "amenityResources")]
        public AmenityResources AmenityResources { get; set; }
    }

    [XmlRoot(ElementName = "amenityImages")]
    public class AmenityImages
    {
        [XmlElement(ElementName = "condoImageUrl")]
        public string CondoImageUrl { get; set; }

        [XmlElement(ElementName = "waterfrontImageUrl")]
        public string WaterFrontImageUrl { get; set; }

        [XmlElement(ElementName = "golfImageUrl")]
        public string GolfImageUrl { get; set; }

        [XmlElement(ElementName = "adultImageUrl")]
        public string AdultImageUrl { get; set; }

        [XmlElement(ElementName = "luxuryImageUrl")]
        public string LuxuryImageUrl { get; set; }
    }


    [XmlRoot(ElementName = "amenityResources")]
    public class AmenityResources
    {
        public AmenityResources()
        {
            AmenityImages = new AmenityImages();
        }

        [XmlElement(ElementName = "amenityImages")]
        public AmenityImages AmenityImages { get; set; }
    }
}