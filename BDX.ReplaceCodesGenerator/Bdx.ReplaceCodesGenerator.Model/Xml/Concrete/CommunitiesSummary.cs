﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "Communities")]
    public class CommunitiesSummary : FileProperties
    {
        [XmlElement(ElementName = "Column")]
        public List<Column> Communities { get; set; }
    }
}
