﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "Column")]
    public class Column
    {
        public Column()
        {
            Link = new List<Link>();
        }

        public Column(List<Link> links)
        {
            if (links == null)
                links = new List<Link>();

            Link = links;
        }

        [XmlElement(ElementName = "Link")]
        public List<Link> Link { get; set; }
    }
}