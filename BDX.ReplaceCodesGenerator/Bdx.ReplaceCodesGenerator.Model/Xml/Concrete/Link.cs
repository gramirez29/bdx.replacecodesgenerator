﻿using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "Link")]
    public class Link
    {
        public Link()
        {
        }

        public Link(string url, string text)
        {
            Url = url;
            Text = text;
        }

        [XmlElement(ElementName = "Url")]
        public string Url { get; set; }

        [XmlElement(ElementName = "Text")]
        public string Text { get; set; }

        [XmlElement(ElementName = "Text2")]
        public string Text2 { get; set; }

        [XmlElement(ElementName = "CommunityCount")]
        public string CommunityCount { get; set; }

        [XmlElement(ElementName = "HomeCount")]
        public string HomeCount { get; set; }

        [XmlElement(ElementName = "BuilderCount")]
        public string BuilderCount { get; set; }

        [XmlElement(ElementName = "ElementClass")]
        public string ElementClass { get; set; }

        [XmlElement(ElementName = "Image")]
        public string Image { get; set; }

        [XmlElement(ElementName = "AltText")]
        public string AltText { get; set; }
    }
}