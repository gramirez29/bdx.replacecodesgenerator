﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "Builder")]
    public class Builders : FileProperties
    {
        [XmlElement(ElementName = "Column")]
        public List<Column> Builder { get; set; }
    }
}