﻿using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "ModularHomes")]
    public class ModularHomes : FileProperties
    {
        [XmlElement(ElementName = "ModularLink")]
        public Link ModularLink { get; set; }
    }
}
