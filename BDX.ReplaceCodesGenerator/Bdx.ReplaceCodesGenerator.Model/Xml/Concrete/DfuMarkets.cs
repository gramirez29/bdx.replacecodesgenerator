﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "market")]
    public class Market
    {
        [XmlAttribute(AttributeName = "id")]
        public int Id { get; set; }
    }

    [XmlRoot(ElementName = "markets")]
    public class DfuMarkets
    {
        public DfuMarkets()
        {
            Market = new List<Market>();
        }

        [XmlElement(ElementName = "market")]
        public List<Market> Market { get; set; }
    }

}