﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot("specialFilterSrp")]
    public class SpecialFilteredSrp
    {
        public SpecialFilteredSrp()
        {
            Markets = new Markets();
        }

        [XmlElement("markets")]
        public Markets Markets { get; set; }
    }

    public class Markets
    {
        public Markets()
        {
            Market = new List<FilteredMarket>();
        }

        [XmlElement("market")]
        public List<FilteredMarket> Market { get; set; }

    }

    public class FilteredMarket
    {
        public FilteredMarket()
        {
            PopularLinks = new PopularFilteredLinks();
        }

        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlElement("popularLinks")]
        public PopularFilteredLinks PopularLinks { get; set; }
    }

    public class PopularFilteredLinks
    {
        public PopularFilteredLinks()
        {
            Links = new List<FilteredLink>();
        }

        [XmlElement("link")]
        public List<FilteredLink> Links { get; set; }
    }

    public class FilteredLink
    {
        [XmlAttribute("url")]
        public string Url { get; set; }


        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}