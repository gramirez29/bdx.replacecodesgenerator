﻿using System.Xml.Serialization;
using Bdx.ReplaceCodesGenerator.Model.Xml.Abstract;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    public class FileProperties : IFileProperties
    {
        [XmlElement(ElementName = "LocationName")]
        public string LocationName { get; set; }

        [XmlElement(ElementName = "MarketName")]
        public string MarketName { get; set; }

        [XmlElement(ElementName = "LocationType")]
        public string LocationType { get; set; }

        [XmlElement(ElementName = "NamePath")]
        public string NamePath { get; set; }

        [XmlElement(ElementName = "FileName")]
        public string FileName { get; set; }

        [XmlElement(ElementName = "IsVisible")]
        public bool IsVisible { get; set; }

        [XmlElement(ElementName = "FileTitle")]
        public string FileTitle { get; set; }
    }
}
