﻿using System.Xml.Serialization;

namespace Bdx.ReplaceCodesGenerator.Model.Xml.Concrete
{
    [XmlRoot(ElementName = "ReplaceCode")]
    public class ReplaceCode
    {
        public ReplaceCode()
        {
            BrandRank = new BrandRank();
        }

        [XmlElement(ElementName = "BrandRank")]
        public BrandRank BrandRank { get; set; }

        [XmlElement(ElementName = "AveragePlanPrice")]
        public string AveragePlanPrice { get; set; }

        [XmlElement(ElementName = "PricePerFoot")]
        public string PricePerFoot { get; set; }

        [XmlElement(ElementName = "BrandRank1")]
        public string BrandRank1 { get; set; }

        [XmlElement(ElementName = "BrandRank2")]
        public string BrandRank2 { get; set; }

        [XmlElement(ElementName = "BrandRank3")]
        public string BrandRank3 { get; set; }
    }

    [XmlRoot(ElementName = "BrandRank")]
    public class BrandRank
    {
        [XmlElement(ElementName = "BrandRank")]
        public string BrandName { get; set; }

        [XmlElement(ElementName = "BrandRank1")]
        public string BrandRank1 { get; set; }

        [XmlElement(ElementName = "BrandRank2")]
        public string BrandRank2 { get; set; }

        [XmlElement(ElementName = "BrandRank3")]
        public string BrandRank3 { get; set; }

    }
}
